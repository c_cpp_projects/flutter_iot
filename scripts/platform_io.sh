#! /bin/bash

######################## Constants ########################

# See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
readonly TEXT_STYLE_END="\033[m"
readonly TEXT_STYLE_ERROR="\033[1;31m"
readonly TEXT_STYLE_INFO="\033[1;4;33m"

######################## Function definitions ########################

# Log an error message(=$2) and exit with the specified code(=$1).
function error() {
  echo -e "${TEXT_STYLE_ERROR}ERROR!${TEXT_STYLE_END}"
  echo -e "$2"
  echo

  exit "$1"
}

# Log an info message(=$1).
function info() {
  echo -e "${TEXT_STYLE_INFO}INFO${TEXT_STYLE_END}"
  echo -e "$1"
  echo
}

# Create a directory at the specified path(=$1).
function createDirectory() {
  mkdir -p "$1"
  echo "Created directory '$1'."
}

# Delete the directory at the specified path(=$1) and
# then create it again.
function recreateDirectory() {
  if [ -d "$1" ]; then
    rm -rf "$1"
    echo "Deleting directory '$1'."
  fi

  createDirectory "$1"
}

# Copy all files from srcDir(=$1) into dstDir(=$2).
function copyDirectory() {
  # Move into srcDir.
  cd "$1" || error 4 "There is no directory called '$1'."

  # Copy all files from srcDir into dstDir.
  eval "cp -r --target-directory $2 $1/*"
}

######################## Script ########################

readonly WORKSPACE_FOLDER=$1

read -rp "Input path to PIO lib folder: " pioLib

# Check if pioLib exists.
if ! [ -d "$pioLib" ]; then
  error 1 "The directory '$pioLib' does not exist."
fi

read -rp "Input the current flutter_iot version (eg.: 1.2.3): " flutterIotVersion

if [ -z "$flutterIotVersion" ]; then
  error 2 "The flutter_iot version string cannot be empty."
fi

IFS="." read -ra versionCodes <<<"$flutterIotVersion"

versionCodesLength=${#versionCodes[@]}
if [ 3 != "$versionCodesLength" ]; then
  error 3 "The flutter_iot version has to be a string with three numbers seperated by two dots. (eg.: 1.2.3)"
fi

# Retrieve all versions
readonly MAJOR_VERSION=${versionCodes[0]}
readonly MINOR_VERSION=${versionCodes[1]}
readonly PATCH_VERSION=${versionCodes[2]}

readonly FLUTTER_IOT_LIB_PATH="$pioLib/flutter_iot_$MAJOR_VERSION""_""$MINOR_VERSION""_""$PATCH_VERSION""/flutter_iot"

recreateDirectory "$FLUTTER_IOT_LIB_PATH"

# Copy sources and include files
copyDirectory "$WORKSPACE_FOLDER/src" "$FLUTTER_IOT_LIB_PATH"
copyDirectory "$WORKSPACE_FOLDER/include" "$FLUTTER_IOT_LIB_PATH"

# Delete all CMakeLists.txt files
find "$FLUTTER_IOT_LIB_PATH" -type f -name "CMakeLists.txt" | while read -r file; do
  rm -f "$file"
done

info "Finished creating '$FLUTTER_IOT_LIB_PATH' !"

exit 0
