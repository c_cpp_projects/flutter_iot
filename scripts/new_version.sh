#! /bin/bash

echo -n "Enter the new version name: "
read -r versionName

git add . >/dev/null
git commit -m"$versionName" >/dev/null

echo -n "Push everything? [y/n]: "
read -r shouldPush

if [ "y" = "$shouldPush" ]; then
  git push >/dev/null
fi

echo "Finished !"

exit 0
