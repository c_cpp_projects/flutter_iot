#include "memory.hpp"

namespace flutter_iot {

  void*
  flutter_malloc(std::size_t size) noexcept(false) {
    if (0 == size)
      return nullptr;

    void* ptr = std::malloc(size);
    if (nullptr == ptr)
      throw BadAllocationException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                                   "Failed to allocate " + toString(size) +
                                     " Bytes using flutter_services::flutter_malloc.");

    return ptr;
  }

  void*
  flutter_calloc(std::size_t num, std::size_t size) noexcept(false) {
    if (0 == size)
      return nullptr;

    void* ptr = std::calloc(num, size);
    if (nullptr == ptr)
      throw BadAllocationException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                                   "Failed to allocate " + toString(num) +
                                     " members of size " + toString(size) +
                                     " Bytes using flutter_services::flutter_calloc.");

    return ptr;
  }

  void*
  flutter_realloc(void* ptr, std::size_t new_size) noexcept(false) {
    if (nullptr == ptr)
      return nullptr;

    if (0 == new_size)
      return nullptr;

    void* new_ptr = std::realloc(ptr, new_size);
    if (nullptr == new_ptr)
      throw BadAllocationException(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        "Failed to reallocate " + toString((uintptr_t) ptr) +
          " to a pointer pointing to a memory block of " + toString(new_size) +
          " Bytes using flutter_services::flutter_realloc.");

    return new_ptr;
  }

  void
  flutter_free(void* ptr) noexcept {
    if (nullptr == ptr)
      return;

    std::free(ptr);
  }

}  // namespace flutter_iot
