#include "exceptions/bad_allocation_exception.hpp"
#include "exceptions/flutter_exception.hpp"
#include "exceptions/format_exception.hpp"
#include "exceptions/missing_plugin_exception.hpp"
#include "exceptions/platform_exception.hpp"

namespace flutter_iot {

  BadAllocationException::BadAllocationException() noexcept : FlutterException() {}

  BadAllocationException::BadAllocationException(const std::string& defaultErrorMessage,
                                                 const std::string& what) noexcept :
      FlutterException(defaultErrorMessage, what) {}

  BadAllocationException::BadAllocationException(const char* defaultErrorMessage,
                                                 const char* what) noexcept :
      FlutterException(defaultErrorMessage, what) {}



  void
  FlutterException::m_setWhat(const std::string& what) noexcept {
    m_what = m_defaultErrorMessage + what;
  }

  const std::string&
  FlutterException::m_getWhat() const noexcept {
    return m_what;
  }

  FlutterException::FlutterException() noexcept : m_defaultErrorMessage(""), m_what("") {}

  FlutterException::FlutterException(const std::string& defaultErrorMessage,
                                     const std::string& what) noexcept :
      m_defaultErrorMessage(defaultErrorMessage), m_what(defaultErrorMessage + what) {}

  FlutterException::FlutterException(const char* defaultErrorMessage,
                                     const char* what) noexcept :
      m_defaultErrorMessage(defaultErrorMessage) {
    m_what = m_defaultErrorMessage + what;
  }

  const char*
  FlutterException::what() const noexcept {
    return m_what.c_str();
  }



  FormatException::FormatException() noexcept : FlutterException() {}

  FormatException::FormatException(const std::string& defaultErrorMessage,
                                   const std::string& what) noexcept :
      FlutterException(defaultErrorMessage, what) {}

  FormatException::FormatException(const char* defaultErrorMessage,
                                   const char* what) noexcept :
      FlutterException(defaultErrorMessage, what) {}



  MissingPluginException::MissingPluginException() noexcept : FlutterException() {}

  MissingPluginException::MissingPluginException(const std::string& defaultErrorMessage,
                                                 const std::string& what) noexcept :
      FlutterException(defaultErrorMessage, what) {}

  MissingPluginException::MissingPluginException(const char* defaultErrorMessage,
                                                 const char* what) noexcept :
      FlutterException(defaultErrorMessage, what) {}



  PlatformException::PlatformException(const std::string& defaultErrorMessage,
                                       const std::string& code,
                                       const ErrorDetails& message,
                                       const ErrorDetails& details,
                                       const ErrorDetails& stacktrace) noexcept :
      FlutterException(defaultErrorMessage),
      m_code(code),
      m_message(message),
      m_details(details),
      m_stacktrace(stacktrace) {
    m_setWhat();
  }

  PlatformException::PlatformException(const PlatformException& source) noexcept :
      FlutterException(),
      m_code(source.m_code),
      m_message(source.m_message),
      m_details(source.m_details),
      m_stacktrace(source.m_stacktrace) {
    m_setWhat();
  }

  PlatformException&
  PlatformException::operator=(const PlatformException& source) noexcept {
    m_code = source.m_code;
    m_message = source.m_message;
    m_details = source.m_details;
    m_stacktrace = source.m_stacktrace;
    m_setWhat();
    return *this;
  }

  void
  PlatformException::m_setWhat() noexcept {
    std::stringstream stream;

    stream << "\nCode: " << m_code << "\n";
    stream << "Message: " << (m_message.has_value() ? m_message.value() : "null") << "\n";
    stream << "Details: " << (m_details.has_value() ? m_details.value() : "null") << "\n";
    stream << "StackTrace: "
           << (m_stacktrace.has_value() ? m_stacktrace.value() : "null");

    FlutterException::m_setWhat(stream.str());
  }

  const std::string&
  PlatformException::getCode() const noexcept {
    return m_code;
  }

  const ErrorDetails&
  PlatformException::getMessage() const noexcept {
    return m_message;
  }

  const ErrorDetails&
  PlatformException::getDetails() const noexcept {
    return m_details;
  }

  const ErrorDetails&
  PlatformException::getStackTrace() const noexcept {
    return m_stacktrace;
  }

}  // namespace flutter_iot