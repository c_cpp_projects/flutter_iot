#include "utils.hpp"

namespace flutter_iot {

  std::string
  removeControlCharacters(std::string s) noexcept {
    for (char i = 1; i < 32; ++i) {
      s.erase(std::remove(s.begin(), s.end(), i), s.end());
    }
    return s;
  }

  const std::string&
  m_saveDefaultErrorMessage(const std::string& message) {
    static std::string s_message;
    s_message = message;
    return s_message;
  }

}  // namespace flutter_iot