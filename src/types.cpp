#include "types/typed_data.hpp"
#include "types/types_utils.hpp"

namespace flutter_iot {

  template<typename T>
  std::string
  m_vectorToString(const std::vector<T>& vector) noexcept {
    if (vector.empty())
      return "[]";

    std::stringstream stream;
    stream << '[';

    size_t i;
    for (i = 0; i < vector.size() - 1; ++i) {
      stream << vector[i] << ", ";
    }
    stream << vector[i];

    stream << ']';

    return stream.str();
  }



  std::string
  toString(const Uint8List& list) noexcept {
    if (list.empty())
      return "[]";

    std::stringstream stream;
    stream << '[';

    size_t i;
    for (i = 0; i < list.size() - 1; ++i) {
      stream << static_cast<int>(list[i]) << ", ";
    }
    stream << static_cast<int>(list[i]);

    stream << ']';

    return stream.str();
  }

  std::string
  toString(const Uint16List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Uint32List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Uint64List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Int8List& list) noexcept {
    if (list.empty())
      return "[]";

    std::stringstream stream;
    stream << '[';

    size_t i;
    for (i = 0; i < list.size() - 1; ++i) {
      stream << static_cast<int>(list[i]) << ", ";
    }
    stream << static_cast<int>(list[i]);

    stream << ']';

    return stream.str();
  }

  std::string
  toString(const Int16List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Int32List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Int64List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Float32List& list) noexcept {
    return m_vectorToString(list);
  }

  std::string
  toString(const Float64List& list) noexcept {
    return m_vectorToString(list);
  }

}  // namespace flutter_iot
