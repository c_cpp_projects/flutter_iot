#include "buffer.hpp"

namespace flutter_iot {

  template<typename T,
           boost::enable_if_t<              ///
             boost::is_integral<T>::value,  //
             bool                           //
             > = true,                      ///
                                            //
           boost::enable_if_t<              ///
             2 == sizeof(T),                //
             bool                           //
             > = true                       ///
           >
  inline static T
  m_getIntegralFromBytes(uint8_t b0, uint8_t b1, Endian endian = Endian::HOST) noexcept {
    T value;
    if (Endian::LITTLE == endian) {
      value = (T) ((((T) (b1)) << (8 * 1)) |  //
                   (((T) (b0)) << (8 * 0)));
    } else {
      value = (T) ((((T) (b0)) << (8 * 1)) |  //
                   (((T) (b1)) << (8 * 0)));
    }

    return value;
  }

  template<typename T,
           boost::enable_if_t<              ///
             boost::is_integral<T>::value,  //
             bool                           //
             > = true,                      ///
                                            //
           boost::enable_if_t<              ///
             4 == sizeof(T),                //
             bool                           //
             > = true                       ///
           >
  inline static T
  m_getIntegralFromBytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3,
                         Endian endian = Endian::HOST) noexcept {
    T value;
    if (Endian::LITTLE == endian) {
      value = (((T) (b3)) << (8 * 3)) |  //
              (((T) (b2)) << (8 * 2)) |  //
              (((T) (b1)) << (8 * 1)) |  //
              (((T) (b0)) << (8 * 0));
    } else {
      value = (((T) (b0)) << (8 * 3)) |  //
              (((T) (b1)) << (8 * 2)) |  //
              (((T) (b2)) << (8 * 1)) |  //
              (((T) (b3)) << (8 * 0));
    }

    return value;
  }

  template<typename T,
           boost::enable_if_t<              ///
             boost::is_integral<T>::value,  //
             bool                           //
             > = true,                      ///
                                            //
           boost::enable_if_t<              ///
             8 == sizeof(T),                //
             bool                           //
             > = true                       ///
           >
  inline static T
  m_getIntegralFromBytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4,
                         uint8_t b5, uint8_t b6, uint8_t b7,
                         Endian endian = Endian::HOST) noexcept {
    T value;
    if (Endian::LITTLE == endian) {
      value = (((T) (b7)) << (8 * 7)) |  //
              (((T) (b6)) << (8 * 6)) |  //
              (((T) (b5)) << (8 * 5)) |  //
              (((T) (b4)) << (8 * 4)) |  //
              (((T) (b3)) << (8 * 3)) |  //
              (((T) (b2)) << (8 * 2)) |  //
              (((T) (b1)) << (8 * 1)) |  //
              (((T) (b0)) << (8 * 0));
    } else {
      value = (((T) (b0)) << (8 * 7)) |  //
              (((T) (b1)) << (8 * 6)) |  //
              (((T) (b2)) << (8 * 5)) |  //
              (((T) (b3)) << (8 * 4)) |  //
              (((T) (b4)) << (8 * 3)) |  //
              (((T) (b5)) << (8 * 2)) |  //
              (((T) (b6)) << (8 * 1)) |  //
              (((T) (b7)) << (8 * 0));
    }

    return value;
  }

  inline static float32_t
  m_getFloatFromBytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3,
                      Endian endian = Endian::HOST) noexcept {
    auto value = m_getIntegralFromBytes<uint32_t>(b0, b1, b2, b3, endian);
    float32_t f32;
    std::memcpy(&f32, &value, 4);
    return f32;
  }

  inline static float64_t
  m_getFloatFromBytes(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4,
                      uint8_t b5, uint8_t b6, uint8_t b7,
                      Endian endian = Endian::HOST) noexcept {
    auto value = m_getIntegralFromBytes<uint64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian);
    float64_t f64;
    std::memcpy(&f64, &value, 8);
    return f64;
  }



  /**
   * @brief Add the bytes of @c value to @c buffer .
   * 
   * @param endian Determines the order in which the bytes of @c value are
   *               appended to @c buffer .
   * 
   * @tparam SequenceContainer A SequenceContainer that holds elements of
   *                           type uint8_t.
   * @tparam T An arithmentic type.
   */
  template<typename SequenceContainer, typename T,
           boost::enable_if_t<                                      ///
             IsContainerOfType<SequenceContainer, uint8_t>::value,  //
             bool                                                   //
             > = true,                                              ///
                                                                    //
           boost::enable_if_t<                                      ///
             boost::is_arithmetic<T>::value,                        //
             bool                                                   //
             > = true                                               ///
           >
  inline static void
  m_addBytes(SequenceContainer& buffer, T value, Endian endian) {
    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, sizeof(T), value);

    (Endian::LITTLE == endian)
      ? buffer.insert(buffer.end(), boost::begin(bytes), boost::end(bytes))
      : buffer.insert(buffer.end(), boost::rbegin(bytes), boost::rend(bytes));
  }

  /**
   * @brief Add all bytes from @c inBuffer to @c outBuffer .
   * 
   * @tparam OutSequenceContainer A SequenceContainer that holds elements of
   *                              type uint8_t.
   * @tparam InSequenceContainer A SequenceContainer that holds elements of
   *                             an arithmetic type.
   */
  template<typename OutSequenceContainer, typename InSequenceContainer,
           boost::enable_if_t<                                         ///
             IsContainerOfType<OutSequenceContainer, uint8_t>::value,  //
             bool                                                      //
             > = true,                                                 ///
                                                                       //
           boost::enable_if_t<                                         ///
             IsContainerOfArithmeticType<InSequenceContainer>::value,  //
             bool                                                      //
             > = true                                                  ///
           >
  inline static void
  m_addBytesFromBuffer(OutSequenceContainer& outBuffer,
                       const InSequenceContainer& inBuffer) {
    for (size_t i = 0; i < inBuffer.size(); ++i) {
      FLUTTER_IOT_AS_CARRAY(
        uint8_t, bytes, sizeof(typename InSequenceContainer::value_type), inBuffer[i]);
      outBuffer.insert(outBuffer.end(), boost::begin(bytes), boost::end(bytes));
    }
  }

  /**
   * 
   * @tparam SequenceContainer A SequenceContainer that holds elements of
   *                           type uint8_t.
   */
  template<typename SequenceContainer,
           boost::enable_if_t<                                      ///
             IsContainerOfType<SequenceContainer, uint8_t>::value,  //
             bool                                                   //
             > = true                                               ///
           >
  inline static void
  m_alignBufferTo(SequenceContainer& buffer, uint8_t alignment) noexcept {
    FLUTTER_IOT_ASSERT(alignment <= 8);

    static const std::array<uint8_t, 8> zeroBuffer{0, 0, 0, 0, 0, 0, 0, 0};

    uint8_t mod = static_cast<uint8_t>(buffer.size() % alignment);
    if (0 == mod)
      return;

    buffer.insert(
      buffer.end(), zeroBuffer.begin(), zeroBuffer.begin() + (alignment - mod));
  }

  inline static void
  m_alignPositionTo(uint64_t& position, uint8_t alignment) noexcept {
    FLUTTER_IOT_ASSERT(alignment <= 8);

    uint8_t mod = static_cast<uint8_t>(position % alignment);
    if (0 == mod)
      return;

    position += alignment - mod;
  }



  void
  ByteData::m_checkValidOffset(int64_t offset, size_t typeSize,
                               const std::string& message) const noexcept(false) {
    size_t size = m_buffer.size();

    if (size < typeSize) {
      throw RangeError::index(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        offset,
        size,
        std::string("ByteData.m_buffer"),
        "The size of the type is greater than the length of this ByteData.\nlength: " +
          toString(size) + "\ntypeSize: " + toString(typeSize) + message);
    } else {
      RangeError::checkValueInInterval(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                                       offset,
                                       0,
                                       size - typeSize,
                                       std::string("ByteData.m_buffer"),
                                       message);
    }
  }

  ByteData::ByteData(ByteBuffer&& buffer) noexcept : m_buffer(std::move(buffer)) {}

  ByteData::ByteData(size_t lengthInBytes) noexcept(false) :
      m_buffer(ByteBuffer(lengthInBytes, 0)) {
    if (0 == lengthInBytes)
      throw ArgumentError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                                 "0",
                                 std::string("ByteData.length"),
                                 std::string("The length of a ByteData cannot be zero"));
  }

  ByteData::ByteData(const uint8_t* data, size_t size) noexcept(false) {
    if (0 == size)
      throw ArgumentError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                                 "0",
                                 std::string("ByteData.length"),
                                 std::string("The length of a ByteData cannot be zero"));

    m_buffer = ByteBuffer(data, data + size);
  }


  ByteData::ByteData(const ByteData& source) noexcept : m_buffer(source.m_buffer) {}

  ByteData::ByteData(ByteData&& source) noexcept : m_buffer(std::move(source.m_buffer)) {}

  ByteData&
  ByteData::operator=(const ByteData& source) noexcept {
    m_buffer = source.m_buffer;
    return *this;
  }

  ByteData&
  ByteData::operator=(ByteData&& source) noexcept {
    m_buffer = std::move(source.m_buffer);
    return *this;
  }

  ByteData
  ByteData::sublistCopy(const Uint8List& data, size_t start, size_t end) noexcept(false) {
    FLUTTER_IOT_FASSERT(start <= end,
                        "start and end must satisfy: 0 ≤ start ≤ end\n"
                        "start is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        start,
                        end);
    FLUTTER_IOT_FASSERT(start < data.size() && end < data.size(),
                        "start and end must be less than data.size().\n"
                        "data.size() is %" FLUTTER_IOT_PRIsize_t
                        "\nstart is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        data.size(),
                        start,
                        end);

    ByteBuffer buffer(data.begin() + start, data.begin() + end + 1);
    return ByteData(std::move(buffer));
  }

  ByteData
  ByteData::sublistCopy(const Int32List& data, size_t start, size_t end,
                        Endian endian) noexcept(false) {
    FLUTTER_IOT_FASSERT(start <= end,
                        "start and end must satisfy: 0 ≤ start ≤ end\n"
                        "start is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        start,
                        end);
    FLUTTER_IOT_FASSERT(start < data.size() && end < data.size(),
                        "start and end must be less than data.size().\n"
                        "data.size() is %" FLUTTER_IOT_PRIsize_t
                        "\nstart is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        data.size(),
                        start,
                        end);

    ByteBuffer buffer;
    buffer.reserve((end - start + 1) * 4);
    for (size_t i = start; i <= end; ++i) {
      m_addBytes(buffer, data[i], endian);
    }

    return ByteData(std::move(buffer));
  }

  ByteData
  ByteData::sublistCopy(const Int64List& data, size_t start, size_t end,
                        Endian endian) noexcept(false) {
    FLUTTER_IOT_FASSERT(start <= end,
                        "start and end must satisfy: 0 ≤ start ≤ end\n"
                        "start is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        start,
                        end);
    FLUTTER_IOT_FASSERT(start < data.size() && end < data.size(),
                        "start and end must be less than data.size().\n"
                        "data.size() is %" FLUTTER_IOT_PRIsize_t
                        "\nstart is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        data.size(),
                        start,
                        end);

    ByteBuffer buffer;
    buffer.reserve((end - start + 1) * 8);
    for (size_t i = start; i <= end; ++i) {
      m_addBytes(buffer, data[i], endian);
    }

    return ByteData(std::move(buffer));
  }

  ByteData
  ByteData::sublistCopy(const Float32List& data, size_t start, size_t end,
                        Endian endian) noexcept(false) {
    FLUTTER_IOT_FASSERT(start <= end,
                        "start and end must satisfy: 0 ≤ start ≤ end\n"
                        "start is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        start,
                        end);
    FLUTTER_IOT_FASSERT(start < data.size() && end < data.size(),
                        "start and end must be less than data.size().\n"
                        "data.size() is %" FLUTTER_IOT_PRIsize_t
                        "\nstart is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        data.size(),
                        start,
                        end);

    ByteBuffer buffer;
    buffer.reserve((end - start + 1) * 4);
    for (size_t i = start; i <= end; ++i) {
      m_addBytes(buffer, data[i], endian);
    }

    return ByteData(std::move(buffer));
  }

  ByteData
  ByteData::sublistCopy(const Float64List& data, size_t start, size_t end,
                        Endian endian) noexcept(false) {
    FLUTTER_IOT_FASSERT(start <= end,
                        "start and end must satisfy: 0 ≤ start ≤ end\n"
                        "start is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        start,
                        end);
    FLUTTER_IOT_FASSERT(start < data.size() && end < data.size(),
                        "start and end must be less than data.size().\n"
                        "data.size() is %" FLUTTER_IOT_PRIsize_t
                        "\nstart is %" FLUTTER_IOT_PRIsize_t
                        "\nend is %" FLUTTER_IOT_PRIsize_t,
                        data.size(),
                        start,
                        end);

    ByteBuffer buffer;
    buffer.reserve((end - start + 1) * 8);
    for (size_t i = start; i <= end; ++i) {
      m_addBytes(buffer, data[i], endian);
    }

    return ByteData(std::move(buffer));
  }

  const ByteData::ByteBuffer&
  ByteData::getBuffer() const noexcept {
    return m_buffer;
  }

  ByteData::ByteBuffer&
  ByteData::getBuffer() noexcept {
    return m_buffer;
  }

  size_t
  ByteData::getLengthInBytes() const noexcept {
    return m_buffer.size();
  }

  float32_t
  ByteData::getFloat32(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 4, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];
    uint8_t b2 = m_buffer[offset + 2];
    uint8_t b3 = m_buffer[offset + 3];

    return m_getFloatFromBytes(b0, b1, b2, b3, endian);
  }

  float64_t
  ByteData::getFloat64(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 8, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];
    uint8_t b2 = m_buffer[offset + 2];
    uint8_t b3 = m_buffer[offset + 3];
    uint8_t b4 = m_buffer[offset + 4];
    uint8_t b5 = m_buffer[offset + 5];
    uint8_t b6 = m_buffer[offset + 6];
    uint8_t b7 = m_buffer[offset + 7];

    return m_getFloatFromBytes(b0, b1, b2, b3, b4, b5, b6, b7, endian);
  }

  int8_t
  ByteData::getInt8(uint64_t offset) const noexcept {
    return static_cast<int8_t>(m_buffer[offset]);
  }

  int16_t
  ByteData::getInt16(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 2, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];

    return m_getIntegralFromBytes<int16_t>(b0, b1, endian);
  }

  int32_t
  ByteData::getInt32(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 4, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];
    uint8_t b2 = m_buffer[offset + 2];
    uint8_t b3 = m_buffer[offset + 3];

    return m_getIntegralFromBytes<int32_t>(b0, b1, b2, b3, endian);
  }

  int64_t
  ByteData::getInt64(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 8, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];
    uint8_t b2 = m_buffer[offset + 2];
    uint8_t b3 = m_buffer[offset + 3];
    uint8_t b4 = m_buffer[offset + 4];
    uint8_t b5 = m_buffer[offset + 5];
    uint8_t b6 = m_buffer[offset + 6];
    uint8_t b7 = m_buffer[offset + 7];

    return m_getIntegralFromBytes<int64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian);
  }

  uint8_t
  ByteData::getUint8(uint64_t offset) const noexcept {
    return m_buffer[offset];
  }

  uint16_t
  ByteData::getUint16(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 2, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];

    return m_getIntegralFromBytes<uint16_t>(b0, b1, endian);
  }

  uint32_t
  ByteData::getUint32(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 4, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];
    uint8_t b2 = m_buffer[offset + 2];
    uint8_t b3 = m_buffer[offset + 3];

    return m_getIntegralFromBytes<uint32_t>(b0, b1, b2, b3, endian);
  }

  uint64_t
  ByteData::getUint64(uint64_t offset, Endian endian) const noexcept(false) {
    m_checkValidOffset(offset, 8, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    uint8_t b0 = m_buffer[offset];
    uint8_t b1 = m_buffer[offset + 1];
    uint8_t b2 = m_buffer[offset + 2];
    uint8_t b3 = m_buffer[offset + 3];
    uint8_t b4 = m_buffer[offset + 4];
    uint8_t b5 = m_buffer[offset + 5];
    uint8_t b6 = m_buffer[offset + 6];
    uint8_t b7 = m_buffer[offset + 7];

    return m_getIntegralFromBytes<uint64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian);
  }

  void
  ByteData::setFloat32(uint64_t offset, float32_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 4, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 4, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 4; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 4; ++i)
        m_buffer[offset + i] = bytes[3 - i];
    }
  }

  void
  ByteData::setFloat64(uint64_t offset, float64_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 8, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 8, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 8; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 8; ++i)
        m_buffer[offset + i] = bytes[7 - i];
    }
  }

  void
  ByteData::setInt8(uint64_t offset, int8_t value) noexcept {
    m_buffer[offset] = static_cast<uint8_t>(value);
  }

  void
  ByteData::setInt16(uint64_t offset, int16_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 2, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 2, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 2; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 2; ++i)
        m_buffer[offset + i] = bytes[1 - i];
    }
  }

  void
  ByteData::setInt32(uint64_t offset, int32_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 4, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 4, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 4; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 4; ++i)
        m_buffer[offset + i] = bytes[3 - i];
    }
  }

  void
  ByteData::setInt64(uint64_t offset, int64_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 8, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 8, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 8; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 8; ++i)
        m_buffer[offset + i] = bytes[7 - i];
    }
  }

  void
  ByteData::setUint8(uint64_t offset, uint8_t value) noexcept {
    m_buffer[offset] = value;
  }

  void
  ByteData::setUint16(uint64_t offset, uint16_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 2, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 2, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 2; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 2; ++i)
        m_buffer[offset + i] = bytes[1 - i];
    }
  }

  void
  ByteData::setUint32(uint64_t offset, uint32_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 4, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 4, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 4; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 4; ++i)
        m_buffer[offset + i] = bytes[3 - i];
    }
  }

  void
  ByteData::setUint64(uint64_t offset, uint64_t value, Endian endian) noexcept(false) {
    m_checkValidOffset(offset, 8, FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE);

    FLUTTER_IOT_AS_CARRAY(uint8_t, bytes, 8, value);
    if (Endian::LITTLE == endian) {
      for (int i = 0; i < 8; ++i)
        m_buffer[offset + i] = bytes[i];
    } else {
      for (int i = 0; i < 8; ++i)
        m_buffer[offset + i] = bytes[7 - i];
    }
  }

  Float32List
  ByteData::asFloat32List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 4))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (float32_t -> 4 Bytes/Element )."));

    if (offsetInBytes + length * 4 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 4);

    Float32List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 4];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 4];
      uint8_t b2 = m_buffer[offsetInBytes + 2 + i * 4];
      uint8_t b3 = m_buffer[offsetInBytes + 3 + i * 4];

      list.push_back(m_getFloatFromBytes(b0, b1, b2, b3, endian));
    }

    return list;
  }

  Float64List
  ByteData::asFloat64List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 8))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (float64_t -> 8 Bytes/Element )."));

    if (offsetInBytes + length * 8 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 8);

    Float64List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 8];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 8];
      uint8_t b2 = m_buffer[offsetInBytes + 2 + i * 8];
      uint8_t b3 = m_buffer[offsetInBytes + 3 + i * 8];
      uint8_t b4 = m_buffer[offsetInBytes + 4 + i * 8];
      uint8_t b5 = m_buffer[offsetInBytes + 5 + i * 8];
      uint8_t b6 = m_buffer[offsetInBytes + 6 + i * 8];
      uint8_t b7 = m_buffer[offsetInBytes + 7 + i * 8];

      list.push_back(m_getFloatFromBytes(b0, b1, b2, b3, b4, b5, b6, b7, endian));
    }

    return list;
  }

  Int8List
  ByteData::asInt8List(size_t length, uint64_t offsetInBytes) const noexcept(false) {
    if (offsetInBytes + length > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, offsetInBytes + length);

    Int8List list;
    list.reserve(length);
    list.insert(list.end(),
                m_buffer.begin() + offsetInBytes,
                m_buffer.begin() + offsetInBytes + length);

    return list;
  }

  Int16List
  ByteData::asInt16List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 2))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (int16_t -> 2 Bytes/Element )."));

    if (offsetInBytes + length * 2 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 2);

    Int16List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 2];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 2];

      list.push_back(m_getIntegralFromBytes<int16_t>(b0, b1, endian));
    }

    return list;
  }

  Int32List
  ByteData::asInt32List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 4))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (int32_t -> 4 Bytes/Element )."));

    if (offsetInBytes + length * 4 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 4);

    Int32List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 4];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 4];
      uint8_t b2 = m_buffer[offsetInBytes + 2 + i * 4];
      uint8_t b3 = m_buffer[offsetInBytes + 3 + i * 4];

      list.push_back(m_getIntegralFromBytes<int32_t>(b0, b1, b2, b3, endian));
    }

    return list;
  }

  Int64List
  ByteData::asInt64List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 8))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (int64_t -> 8 Bytes/Element )."));

    if (offsetInBytes + length * 8 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 8);

    Int64List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 8];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 8];
      uint8_t b2 = m_buffer[offsetInBytes + 2 + i * 8];
      uint8_t b3 = m_buffer[offsetInBytes + 3 + i * 8];
      uint8_t b4 = m_buffer[offsetInBytes + 4 + i * 8];
      uint8_t b5 = m_buffer[offsetInBytes + 5 + i * 8];
      uint8_t b6 = m_buffer[offsetInBytes + 6 + i * 8];
      uint8_t b7 = m_buffer[offsetInBytes + 7 + i * 8];

      list.push_back(
        m_getIntegralFromBytes<int64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian));
    }

    return list;
  }

  Uint8List
  ByteData::asUint8List(size_t length, uint64_t offsetInBytes) const noexcept(false) {
    if (offsetInBytes + length > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, offsetInBytes + length);

    Uint8List list;
    list.reserve(length);
    list.insert(list.end(),
                m_buffer.begin() + offsetInBytes,
                m_buffer.begin() + offsetInBytes + length);

    return list;
  }

  Uint16List
  ByteData::asUint16List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 2))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (uint16_t -> 2 Bytes/Element )."));

    if (offsetInBytes + length * 2 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 2);

    Uint16List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 2];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 2];

      list.push_back(m_getIntegralFromBytes<uint16_t>(b0, b1, endian));
    }

    return list;
  }

  Uint32List
  ByteData::asUint32List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 4))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (uint32_t -> 4 Bytes/Element )."));

    if (offsetInBytes + length * 4 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 4);

    Uint32List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 4];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 4];
      uint8_t b2 = m_buffer[offsetInBytes + 2 + i * 4];
      uint8_t b3 = m_buffer[offsetInBytes + 3 + i * 4];

      list.push_back(m_getIntegralFromBytes<uint32_t>(b0, b1, b2, b3, endian));
    }

    return list;
  }

  Uint64List
  ByteData::asUint64List(size_t length, uint64_t offsetInBytes, Endian endian) const
    noexcept(false) {
    if (!isMultipleOf(offsetInBytes, 8))
      throw RangeError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Offset (" + toString(offsetInBytes) +
          ") "
          "must be a multiple of BYTES_PER_ELEMENT (uint64_t -> 8 Bytes/Element )."));

    if (offsetInBytes + length * 8 > getLengthInBytes())
      throw RangeError::value(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              offsetInBytes + length * 8);

    Uint64List list;
    list.reserve(length);
    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer[offsetInBytes + 0 + i * 8];
      uint8_t b1 = m_buffer[offsetInBytes + 1 + i * 8];
      uint8_t b2 = m_buffer[offsetInBytes + 2 + i * 8];
      uint8_t b3 = m_buffer[offsetInBytes + 3 + i * 8];
      uint8_t b4 = m_buffer[offsetInBytes + 4 + i * 8];
      uint8_t b5 = m_buffer[offsetInBytes + 5 + i * 8];
      uint8_t b6 = m_buffer[offsetInBytes + 6 + i * 8];
      uint8_t b7 = m_buffer[offsetInBytes + 7 + i * 8];

      list.push_back(
        m_getIntegralFromBytes<uint64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian));
    }

    return list;
  }



  const std::array<uint8_t, 8> WriteBuffer::m_zeroBuffer{0, 0, 0, 0, 0, 0, 0, 0};

  void
  WriteBuffer::m_alignTo(uint64_t alignment) noexcept {
    FLUTTER_IOT_ASSERT(!m_isDone);
    FLUTTER_IOT_ASSERT(alignment < 256);
    m_alignBufferTo(m_buffer, static_cast<uint8_t>(alignment));
  }

  WriteBuffer::WriteBuffer() noexcept : m_buffer(), m_isDone(false) {}

  WriteBuffer::~WriteBuffer() noexcept {}

  void
  WriteBuffer::putUint8(uint8_t value) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_buffer.push_back(value);
  }

  void
  WriteBuffer::putUint16(uint16_t value, Endian endian) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_addBytes(m_buffer, value, endian);
  }

  void
  WriteBuffer::putUint32(uint32_t value, Endian endian) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_addBytes(m_buffer, value, endian);
  }

  void
  WriteBuffer::putInt32(int32_t value, Endian endian) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_addBytes(m_buffer, value, endian);
  }

  void
  WriteBuffer::putInt64(int64_t value, Endian endian) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_addBytes(m_buffer, value, endian);
  }

  void
  WriteBuffer::putFloat64(float64_t value, Endian endian) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_alignTo(8);
    m_addBytes(m_buffer, value, endian);
  }

  void
  WriteBuffer::putUint8List(const Uint8List& buffer) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_buffer.insert(m_buffer.end(), buffer.begin(), buffer.end());
  }

  void
  WriteBuffer::putInt32List(const Int32List& buffer) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_alignTo(4);
    m_addBytesFromBuffer(m_buffer, buffer);
  }

  void
  WriteBuffer::putInt64List(const Int64List& buffer) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_alignTo(8);
    m_addBytesFromBuffer(m_buffer, buffer);
  }

  void
  WriteBuffer::putFloat32List(const Float32List& buffer) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_alignTo(4);
    m_addBytesFromBuffer(m_buffer, buffer);
  }

  void
  WriteBuffer::putFloat64List(const Float64List& buffer) {
    FLUTTER_IOT_ASSERT(!m_isDone);
    m_alignTo(8);
    m_addBytesFromBuffer(m_buffer, buffer);
  }

  const ByteData
  WriteBuffer::done() {
    if (m_isDone) {
      throw StateError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        "done() must not be called more than once on the same WriteBuffer.");
    }

    m_isDone = true;
    auto doneBuffer = ByteData::ByteBuffer(m_buffer.begin(), m_buffer.end());
    m_buffer.clear();

    return ByteData(std::move(doneBuffer));
  }



  void
  ReadBuffer::m_alignTo(uint64_t alignment) noexcept {
    FLUTTER_IOT_ASSERT(alignment < 256);
    m_alignPositionTo(m_position, static_cast<uint8_t>(alignment));
  }

  ReadBuffer::ReadBuffer(const ByteData& data) noexcept :
      m_buffer(data.m_buffer), m_position(0) {}

  ReadBuffer::~ReadBuffer() noexcept {}

  bool
  ReadBuffer::hasRemaining() const noexcept {
    return m_position < m_buffer.size();
  }

  uint8_t
  ReadBuffer::getUint8() noexcept(false) {
    return m_buffer.at(m_position++);
  }

  uint16_t
  ReadBuffer::getUint16(Endian endian) noexcept(false) {
    uint8_t b0 = m_buffer.at(m_position);
    uint8_t b1 = m_buffer.at(m_position + 1);

    m_position += 2;

    return m_getIntegralFromBytes<uint16_t>(b0, b1, endian);
  }

  uint32_t
  ReadBuffer::getUint32(Endian endian) noexcept(false) {
    uint8_t b0 = m_buffer.at(m_position);
    uint8_t b1 = m_buffer.at(m_position + 1);
    uint8_t b2 = m_buffer.at(m_position + 2);
    uint8_t b3 = m_buffer.at(m_position + 3);

    m_position += 4;

    return m_getIntegralFromBytes<uint32_t>(b0, b1, b2, b3, endian);
  }

  int32_t
  ReadBuffer::getInt32(Endian endian) noexcept(false) {
    uint8_t b0 = m_buffer.at(m_position);
    uint8_t b1 = m_buffer.at(m_position + 1);
    uint8_t b2 = m_buffer.at(m_position + 2);
    uint8_t b3 = m_buffer.at(m_position + 3);

    m_position += 4;

    return m_getIntegralFromBytes<int32_t>(b0, b1, b2, b3, endian);
  }

  int64_t
  ReadBuffer::getInt64(Endian endian) noexcept(false) {
    uint8_t b0 = m_buffer.at(m_position);
    uint8_t b1 = m_buffer.at(m_position + 1);
    uint8_t b2 = m_buffer.at(m_position + 2);
    uint8_t b3 = m_buffer.at(m_position + 3);
    uint8_t b4 = m_buffer.at(m_position + 4);
    uint8_t b5 = m_buffer.at(m_position + 5);
    uint8_t b6 = m_buffer.at(m_position + 6);
    uint8_t b7 = m_buffer.at(m_position + 7);

    m_position += 8;

    return m_getIntegralFromBytes<int64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian);
  }

  float64_t
  ReadBuffer::getFloat64(Endian endian) noexcept(false) {
    m_alignTo(8);

    uint8_t b0 = m_buffer.at(m_position);
    uint8_t b1 = m_buffer.at(m_position + 1);
    uint8_t b2 = m_buffer.at(m_position + 2);
    uint8_t b3 = m_buffer.at(m_position + 3);
    uint8_t b4 = m_buffer.at(m_position + 4);
    uint8_t b5 = m_buffer.at(m_position + 5);
    uint8_t b6 = m_buffer.at(m_position + 6);
    uint8_t b7 = m_buffer.at(m_position + 7);

    m_position += 8;

    return m_getFloatFromBytes(b0, b1, b2, b3, b4, b5, b6, b7, endian);
  }

  Uint8List
  ReadBuffer::getUint8List(size_t length) noexcept(false) {
    Uint8List list;

    list.reserve(length);
    list.insert(
      list.end(), m_buffer.begin() + m_position, m_buffer.begin() + m_position + length);

    m_position += length;

    return list;
  }

  Int32List
  ReadBuffer::getInt32List(size_t length, Endian endian) noexcept(false) {
    m_alignTo(4);

    Int32List list;
    list.reserve(length);

    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer.at(m_position);
      uint8_t b1 = m_buffer.at(m_position + 1);
      uint8_t b2 = m_buffer.at(m_position + 2);
      uint8_t b3 = m_buffer.at(m_position + 3);

      list.push_back(m_getIntegralFromBytes<int32_t>(b0, b1, b2, b3, endian));

      m_position += 4;
    }

    return list;
  }

  Int64List
  ReadBuffer::getInt64List(size_t length, Endian endian) noexcept(false) {
    m_alignTo(8);

    Int64List list;
    list.reserve(length);

    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer.at(m_position);
      uint8_t b1 = m_buffer.at(m_position + 1);
      uint8_t b2 = m_buffer.at(m_position + 2);
      uint8_t b3 = m_buffer.at(m_position + 3);
      uint8_t b4 = m_buffer.at(m_position + 4);
      uint8_t b5 = m_buffer.at(m_position + 5);
      uint8_t b6 = m_buffer.at(m_position + 6);
      uint8_t b7 = m_buffer.at(m_position + 7);

      list.push_back(
        m_getIntegralFromBytes<int64_t>(b0, b1, b2, b3, b4, b5, b6, b7, endian));

      m_position += 8;
    }

    return list;
  }

  Float32List
  ReadBuffer::getFloat32List(size_t length, Endian endian) noexcept(false) {
    m_alignTo(4);

    Float32List list;
    list.reserve(length);

    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer.at(m_position);
      uint8_t b1 = m_buffer.at(m_position + 1);
      uint8_t b2 = m_buffer.at(m_position + 2);
      uint8_t b3 = m_buffer.at(m_position + 3);

      list.push_back(m_getFloatFromBytes(b0, b1, b2, b3, endian));

      m_position += 4;
    }

    return list;
  }

  Float64List
  ReadBuffer::getFloat64List(size_t length, Endian endian) noexcept(false) {
    m_alignTo(8);

    Float64List list;
    list.reserve(length);

    for (size_t i = 0; i < length; ++i) {
      uint8_t b0 = m_buffer.at(m_position);
      uint8_t b1 = m_buffer.at(m_position + 1);
      uint8_t b2 = m_buffer.at(m_position + 2);
      uint8_t b3 = m_buffer.at(m_position + 3);
      uint8_t b4 = m_buffer.at(m_position + 4);
      uint8_t b5 = m_buffer.at(m_position + 5);
      uint8_t b6 = m_buffer.at(m_position + 6);
      uint8_t b7 = m_buffer.at(m_position + 7);

      list.push_back(m_getFloatFromBytes(b0, b1, b2, b3, b4, b5, b6, b7, endian));

      m_position += 8;
    }

    return list;
  }

}  // namespace flutter_iot
