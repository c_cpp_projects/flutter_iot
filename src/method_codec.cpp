#include "method_codec.hpp"

namespace flutter_iot {

  StandardMethodCodec::StandardMethodCodec(MessageCodecType codec) noexcept :
      m_codec(codec) {}

  StandardMethodCodec::~StandardMethodCodec() noexcept {}

  StandardMethodCodec::MessageCodecType
  StandardMethodCodec::getCodec() const noexcept {
    return m_codec;
  }

  ByteData
  StandardMethodCodec::encodeMethodCall(const MethodCallType& call) {
    WriteBuffer wb;
    m_codec.writeValue(wb, AnyOrNone(call.getMethod()));
    m_codec.writeValue(wb, AnyOrNone(call.getArguments()));
    return wb.done();
  }

  StandardMethodCodec::MethodCallType
  StandardMethodCodec::decodeMethodCall(const boost::optional<ByteData>& call) {
    if (!call.has_value())
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, "Invalid method call!");

    ReadBuffer rb(call.value());
    auto method = m_codec.readValue(rb);
    auto arguments = m_codec.readValue(rb);

    if (rb.hasRemaining())
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid method call!\n"
                            "ReadBuffer has remaining data.");

    if (!method.has_value() || !FLUTTER_IOT_ANY_IS_TYPE(std::string, method.value()))
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid method call!\n"
                            "The method read from the ReadBuffer has no "
                            "value or is not of type std::string!");

    // If arguments is not boost::none, then it has to be of
    // type MessageCodecType::ValueTypeLabel::Map .
    if (arguments.has_value()) {
      if (!FLUTTER_IOT_ANY_IS_TYPE(MessageCodecType::ValueTypeLabel::Map,
                                   arguments.value()))
        throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                              "Arguments of a MethodCall have to be boost::none, or a "
                              "MessageCodecType::ValueTypeLabel::Map .");

      return MethodCallType(
        FLUTTER_IOT_ANY_CAST(std::string, method.value()),
        FLUTTER_IOT_ANY_CAST(MessageCodecType::ValueTypeLabel::Map, arguments.value()));
    }

    return MethodCallType(FLUTTER_IOT_ANY_CAST(std::string, method.value()),
                          MessageCodecType::ValueTypeLabel::Map());
  }

  AnyOrNone
  StandardMethodCodec::decodeEnvelope(const ByteData& envelope) {
    ReadBuffer rb(envelope);

    // First byte is zero in success case, and non-zero otherwise.
    if (0 == rb.getUint8())
      return m_codec.readValue(rb);

    auto errorCode = m_codec.readValue(rb);
    auto errorMessage = m_codec.readValue(rb);
    auto errorDetails = m_codec.readValue(rb);
    auto errorStackTrace = rb.hasRemaining() ? m_codec.readValue(rb) : AnyOrNone();

    if (!errorCode.has_value() ||
        !FLUTTER_IOT_ANY_IS_TYPE(std::string, errorCode.value()))
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid envelope!\n"
                            "The errorCode read from the ReadBuffer has no "
                            "value or is not of type std::string!");

    if (errorMessage.has_value() &&
        !FLUTTER_IOT_ANY_IS_TYPE(std::string, errorMessage.value()))
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid envelope!\n"
                            "The errorMessage read from the ReadBuffer has a "
                            "value but it is not of type std::string!");

    // In the flutter sdk errorDetails is of type Object?, but for comfort
    // we require it to be a std::string here.
    if (errorDetails.has_value() &&
        !FLUTTER_IOT_ANY_IS_TYPE(std::string, errorDetails.value()))
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid envelope!\n"
                            "The errorDetails read from the ReadBuffer has a "
                            "value but it is not of type std::string!");

    if (errorStackTrace.has_value() &&
        !FLUTTER_IOT_ANY_IS_TYPE(std::string, errorStackTrace.value()))
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid envelope!\n"
                            "The errorStackTrace read from the ReadBuffer has a "
                            "value but it is not of type std::string!");

    if (rb.hasRemaining())
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            "Invalid envelope!\n"
                            "ReadBuffer has remaining data.");

    boost::optional<std::string> message;
    if (errorMessage.has_value())
      message = FLUTTER_IOT_ANY_CAST(std::string, errorMessage.value());

    ErrorDetails details;
    if (errorDetails.has_value())
      details = FLUTTER_IOT_ANY_CAST(std::string, errorDetails.value());

    boost::optional<std::string> stacktrace;
    if (errorStackTrace.has_value())
      stacktrace = FLUTTER_IOT_ANY_CAST(std::string, errorStackTrace.value());

    throw PlatformException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
                            FLUTTER_IOT_ANY_CAST(std::string, errorCode.value()),
                            message,
                            details,
                            stacktrace);
  }

  ByteData
  StandardMethodCodec::encodeSuccessEnvelope(const AnyOrNone& result) {
    WriteBuffer wb;
    wb.putUint8(0);
    m_codec.writeValue(wb, result);
    return wb.done();
  }

  ByteData
  StandardMethodCodec::encodeErrorEnvelope(std::string code,
                                           boost::optional<std::string> message,
                                           ErrorDetails details) {
    WriteBuffer wb;
    wb.putUint8(1);
    m_codec.writeValue(wb, makeAnyOrNone(code));
    m_codec.writeValue(wb, makeAnyOrNone(message));
    m_codec.writeValue(wb, makeAnyOrNone(details));
    return wb.done();
  }

}  // namespace flutter_iot
