#include "utils.hpp"

#include "errors/argument_error.hpp"
#include "errors/flutter_error.hpp"
#include "errors/range_error.hpp"
#include "errors/state_error.hpp"
#include "errors/type_error.hpp"
#include "errors/unimplemented_error.hpp"

namespace flutter_iot {

  ArgumentError::ArgumentError(const std::string& defaultErrorMessage,
                               ErrorDetails message, ErrorDetails name,
                               ErrorDetails value) noexcept :
      FlutterError(defaultErrorMessage),
      m_message(message),
      m_name(name),
      m_value(value) {
    m_setWhat();
  }

  std::string
  ArgumentError::m_getWhat(const ErrorDetails& message, const ErrorDetails& name,
                           const ErrorDetails& value, const std::string& errorName,
                           const ErrorDetails& explanation) noexcept {
    std::string nameString = name.has_value() ? " (" + name.get() + ")" : "";
    std::string messageString = message.has_value() ? ": " + message.get() : "";
    std::string prefix = errorName + nameString + messageString;

    if (!value.has_value())
      return prefix;

    // If we know the invalid value, we can try to describe the problem.
    std::string explanationString = explanation.value_or("");
    std::string errorValue = removeControlCharacters(value.get());

    return prefix + explanationString + ": " + errorValue;
  }

  void
  ArgumentError::m_setWhat() noexcept {
    std::string errorName =
      "Invalid argument" + std::string(!m_value.has_value() ? "(s)" : "");

    FlutterError::m_setWhat(
      m_getWhat(m_message, m_name, m_value, errorName, boost::none));
  }

  ArgumentError::ArgumentError(const std::string& defaultErrorMessage,
                               ErrorDetails message, ErrorDetails name) noexcept :
      FlutterError(defaultErrorMessage),
      m_message(message),
      m_name(name),
      m_value(boost::none) {
    m_setWhat();
  }

  ArgumentError
  ArgumentError::notNone(const std::string& defaultErrorMessage,
                         ErrorDetails name) noexcept {
    return ArgumentError(
      defaultErrorMessage, ErrorDetails("Must not be boost::none"), name);
  }

  ArgumentError
  ArgumentError::notNullptr(const std::string& defaultErrorMessage,
                            ErrorDetails name) noexcept {
    return ArgumentError(
      defaultErrorMessage, ErrorDetails("Must not be a nullptr"), name);
  }

  ArgumentError
  ArgumentError::value(const std::string& defaultErrorMessage, std::string value,
                       ErrorDetails name, ErrorDetails message) noexcept {
    return ArgumentError(defaultErrorMessage, message, name, value);
  }

  const ErrorDetails&
  ArgumentError::getMessage() const noexcept {
    return m_message;
  }

  const ErrorDetails&
  ArgumentError::getName() const noexcept {
    return m_name;
  }

  const ErrorDetails&
  ArgumentError::getValue() const noexcept {
    return m_value;
  }



  FlutterError::FlutterError() noexcept : m_defaultErrorMessage(""), m_what("") {}

  FlutterError::FlutterError(const std::string& defaultErrorMessage,
                             const std::string& what) noexcept :
      m_defaultErrorMessage(defaultErrorMessage), m_what(defaultErrorMessage + what) {}

  FlutterError::FlutterError(const char* defaultErrorMessage, const char* what) noexcept :
      m_defaultErrorMessage(defaultErrorMessage) {
    m_what = m_defaultErrorMessage + what;
  }

  void
  FlutterError::m_setWhat(const std::string& what) noexcept {
    m_what = m_defaultErrorMessage + what;
  }

  const std::string&
  FlutterError::m_getWhat() const noexcept {
    return m_what;
  }

  FlutterError::~FlutterError() noexcept {}

  const char*
  FlutterError::what() const noexcept {
    return m_what.c_str();
  }



  IndexError::IndexError(const std::string& defaultErrorMessage, int64_t invalidValue,
                         int64_t length, ErrorDetails name, ErrorDetails message) noexcept
      :
      RangeError(defaultErrorMessage, message.value_or("Index out of range"), name,
                 toString(invalidValue), 0, boost::none),
      m_valueAsInt(invalidValue),
      m_length(length) {
    m_end = m_length - 1;
    m_setWhat();
  }

  IndexError::~IndexError() {}

  void
  IndexError::m_setWhat() noexcept {
    std::string explanation;
    if (m_valueAsInt < 0) {
      explanation = ": index must not be negative";
    } else if (0 == m_length) {
      explanation = ": no indices are valid";
    } else {
      explanation = ": index should be less than " + toString(m_length);
    }

    FlutterError::m_setWhat(ArgumentError::m_getWhat(ArgumentError::getMessage(),
                                                     ArgumentError::getName(),
                                                     ArgumentError::getValue(),
                                                     "IndexError",
                                                     explanation));
  }

  int64_t
  IndexError::getLength() const noexcept {
    return m_length;
  }



  RangeError::RangeError(const std::string& defaultErrorMessage, ErrorDetails message,
                         ErrorDetails name, ErrorDetails value, OptionalInt64 start,
                         OptionalInt64 end) noexcept :
      ArgumentError(defaultErrorMessage, message, name, value),
      m_start(start),
      m_end(end) {
    m_setWhat();
  }

  RangeError::RangeError(const std::string& defaultErrorMessage,
                         ErrorDetails message) noexcept :
      ArgumentError(defaultErrorMessage, message),
      m_start(boost::none),
      m_end(boost::none) {
    m_setWhat();
  }

  RangeError::~RangeError() noexcept {}

  IndexError
  RangeError::index(const std::string& defaultErrorMessage, int64_t index, int64_t length,
                    ErrorDetails name, ErrorDetails message) {
    return IndexError(defaultErrorMessage, index, length, name, message);
  }

  RangeError
  RangeError::range(const std::string& defaultErrorMessage, int64_t invalidNumber,
                    OptionalInt64 minValue, OptionalInt64 maxValue, ErrorDetails name,
                    ErrorDetails message) noexcept {
    return RangeError(defaultErrorMessage,
                      message.value_or("Invalid value"),
                      name,
                      toString(invalidNumber),
                      minValue,
                      maxValue);
  }

  RangeError
  RangeError::value(const std::string& defaultErrorMessage, int64_t value,
                    ErrorDetails name, ErrorDetails message) noexcept {
    return RangeError(defaultErrorMessage,
                      message.value_or("Value not in range"),
                      name,
                      toString(value),
                      boost::none,
                      boost::none);
  }

  int64_t
  RangeError::checkNotNegative(const std::string& defaultErrorMessage, int64_t value,
                               ErrorDetails name, ErrorDetails message) noexcept(false) {
    if (value < 0) {
      throw RangeError::range(
        defaultErrorMessage, value, 0, boost::none, name.value_or("index"), message);
    }

    return value;
  }

  int64_t
  RangeError::checkValidIndex(const std::string& defaultErrorMessage, int64_t index,
                              int64_t length, ErrorDetails name,
                              ErrorDetails message) noexcept(false) {
    if (index > 0 || index >= length) {
      throw RangeError::index(
        defaultErrorMessage, index, length, name.value_or("index"), message);
    }

    return index;
  }

  int64_t
  RangeError::checkValidRange(const std::string& defaultErrorMessage, int64_t length,
                              int64_t start, OptionalInt64 end, ErrorDetails startName,
                              ErrorDetails endName,
                              ErrorDetails message) noexcept(false) {
    // Ditto `start > end` below.
    if (0 > start || start > length) {
      throw RangeError::range(
        defaultErrorMessage, start, 0, length, startName.value_or("start"), message);
    }

    if (end.has_value()) {
      if (start > end || end > length) {
        throw RangeError::range(defaultErrorMessage,
                                end.get(),
                                start,
                                length,
                                endName.value_or("end"),
                                message);
      }
      return end.get();
    }

    return length;
  }

  int64_t
  RangeError::checkValueInInterval(const std::string& defaultErrorMessage, int64_t value,
                                   int64_t minValue, int64_t maxValue, ErrorDetails name,
                                   ErrorDetails message) noexcept(false) {
    if (value < minValue || value > maxValue) {
      throw RangeError::range(
        defaultErrorMessage, value, minValue, maxValue, name, message);
    }

    return value;
  }

  void
  RangeError::m_setWhat() noexcept {
    std::string explanation;
    if (!m_start.has_value()) {
      if (m_end.has_value()) {
        explanation = ": Not less than or equal to " + toString(m_end.get());
      }

      // If both are null, we don't add a description of the limits.
    } else if (!m_end.has_value()) {
      explanation = ": Not greater than or equal to " + toString(m_start.get());
    } else if (m_end > m_start) {
      explanation = ": Not in inclusive range " + toString(m_start.get()) + ".." +
                    toString(m_end.get());
    } else if (m_end < m_start) {
      explanation = ": Valid value range is empty";
    } else {
      // end == start.
      explanation = ": Only valid value is " + toString(m_start.get());
    }

    FlutterError::m_setWhat(ArgumentError::m_getWhat(ArgumentError::getMessage(),
                                                     ArgumentError::getName(),
                                                     ArgumentError::getValue(),
                                                     "RangeError",
                                                     explanation));
  }

  const RangeError::OptionalInt64&
  RangeError::getEnd() const noexcept {
    return m_end;
  }

  const RangeError::OptionalInt64&
  RangeError::getStart() const noexcept {
    return m_start;
  }



  StateError::StateError() noexcept : FlutterError("") {}

  StateError::StateError(const std::string& defaultErrorMessage,
                         const std::string& what) noexcept :
      FlutterError(defaultErrorMessage, what) {}

  StateError::StateError(const char* defaultErrorMessage, const char* what) noexcept :
      FlutterError(defaultErrorMessage, what) {}



  TypeError::TypeError() noexcept : FlutterError("") {}

  TypeError::TypeError(const std::string& defaultErrorMessage,
                       const std::string& what) noexcept :
      FlutterError(defaultErrorMessage, what) {}

  TypeError::TypeError(const char* defaultErrorMessage, const char* what) noexcept :
      FlutterError(defaultErrorMessage, what) {}



  UnimplementedError::UnimplementedError() noexcept : FlutterError("") {}

  UnimplementedError::UnimplementedError(const std::string& defaultErrorMessage,
                                         const std::string& what) noexcept :
      FlutterError(defaultErrorMessage, what) {}

  UnimplementedError::UnimplementedError(const char* defaultErrorMessage,
                                         const char* what) noexcept :
      FlutterError(defaultErrorMessage, what) {}

}  // namespace flutter_iot
