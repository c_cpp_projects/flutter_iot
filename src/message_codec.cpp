#include "message_codec.hpp"

namespace flutter_iot {

  bool
  StandardMessageCodec::ValueTypeLabel::Equivalent::equal(
    const AnyOrNone& a, const AnyOrNone& b) noexcept(false) {
    // Same address -> Same object
    if (&a == &b)
      return true;

    // Both hold VALUE_NULL -> Same
    if (!a.has_value() && !b.has_value())
      return true;

    // One is null and the other holds a value
    // -> Cannot be equal
    if (a.has_value() != b.has_value())
      return false;

    const auto& typeA = a.get().type();

    // Definitely different because type is different.
    if (typeA != b.get().type())
      return false;

    if (FLUTTER_IOT_IS_TYPE(bool, typeA)) {
      return (FLUTTER_IOT_ANY_CAST(bool, a.get()) == FLUTTER_IOT_ANY_CAST(bool, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(float64_t, typeA)) {
      return (FLUTTER_IOT_ANY_CAST(float64_t, a.get()) ==
              FLUTTER_IOT_ANY_CAST(float64_t, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(int32_t, typeA)) {
      return (FLUTTER_IOT_ANY_CAST(int32_t, a.get()) ==
              FLUTTER_IOT_ANY_CAST(int32_t, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(int64_t, typeA)) {
      return (FLUTTER_IOT_ANY_CAST(int64_t, a.get()) ==
              FLUTTER_IOT_ANY_CAST(int64_t, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(std::string, typeA)) {
      return (FLUTTER_IOT_ANY_CAST(std::string, a.get()) ==
              FLUTTER_IOT_ANY_CAST(std::string, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(Uint8List, typeA)) {
      return boost::equal(FLUTTER_IOT_ANY_CAST(Uint8List, a.get()),
                          FLUTTER_IOT_ANY_CAST(Uint8List, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(Int32List, typeA)) {
      return boost::equal(FLUTTER_IOT_ANY_CAST(Int32List, a.get()),
                          FLUTTER_IOT_ANY_CAST(Int32List, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(Int64List, typeA)) {
      return boost::equal(FLUTTER_IOT_ANY_CAST(Int64List, a.get()),
                          FLUTTER_IOT_ANY_CAST(Int64List, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(Float32List, typeA)) {
      return boost::equal(FLUTTER_IOT_ANY_CAST(Float32List, a.get()),
                          FLUTTER_IOT_ANY_CAST(Float32List, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(Float64List, typeA)) {
      return boost::equal(FLUTTER_IOT_ANY_CAST(Float64List, a.get()),
                          FLUTTER_IOT_ANY_CAST(Float64List, b.get()));
    }

    if (FLUTTER_IOT_IS_TYPE(List, typeA)) {
      const auto& listA = FLUTTER_IOT_ANY_CAST(List, a.get());
      const auto& listB = FLUTTER_IOT_ANY_CAST(List, b.get());

      if (listA.size() != listB.size())
        return false;

      return boost::equal(listA, listB, Equivalent());
    }

    if (FLUTTER_IOT_IS_TYPE(ValueTypeLabel::Map, typeA)) {
      const auto& mapA = FLUTTER_IOT_ANY_CAST(ValueTypeLabel::Map, a.get());
      const auto& mapB = FLUTTER_IOT_ANY_CAST(ValueTypeLabel::Map, b.get());

      if (mapA.size() != mapB.size())
        return false;

      const Equivalent equal;

      for (const auto& pairA : mapA) {
        bool hasValue = false;

        for (const auto& pairB : mapB) {
          if (equal(pairA.first, pairB.first) && equal(pairA.second, pairB.second)) {
            hasValue = true;
            break;
          }
        }

        if (!hasValue)
          return false;
      }

      return true;
    }



    throw ArgumentError(
      FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
      "ValueTypeLabel::Equivalent::equal does not support the following type: '" +
        FLUTTER_IOT_DEMANGLE_TYPE_NAME(typeA.name()) + "'.");
  }

  uint64_t
  StandardMessageCodec::ValueTypeLabel::Hashing::hash(const AnyOrNone& any) noexcept(
    false) {
    if (!any.has_value()) {
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + 0;  // 0 because no value -> null
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(bool, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(bool, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<bool>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(float64_t, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(float64_t, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<float64_t>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(int32_t, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(int32_t, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<int32_t>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(int64_t, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(int64_t, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<int64_t>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(std::string, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(std::string, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<std::string>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Uint8List, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(Uint8List, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<Uint8List>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Int32List, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(Int32List, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<Int32List>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Int64List, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(Int64List, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<Int64List>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Float32List, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(Float32List, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<Float32List>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Float64List, any.value())) {
      const auto& value = FLUTTER_IOT_ANY_CAST(Float64List, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;
      result = prime * result + std::hash<Float64List>()(value);
      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(List, any.value())) {
      const auto& list = FLUTTER_IOT_ANY_CAST(List, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;

      const Hashing hash;
      for (const auto& value : list) {
        result = prime * result + hash(value);
      }

      return result;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(ValueTypeLabel::Map, any.value())) {
      const auto& map = FLUTTER_IOT_ANY_CAST(ValueTypeLabel::Map, any.value());
      constexpr uint64_t prime = 31;
      uint64_t result = 1;

      const Hashing hash;
      for (const auto& pair : map) {
        result = prime * result + hash(pair.first);
        result = prime * result + hash(pair.second);
      }

      return result;
    }



    throw ArgumentError(
      FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
      "ValueTypeLabel::Hash::operator() does not support the following type: '" +
        FLUTTER_IOT_DEMANGLE_ANY(any.value()) + "'.");
  }

  bool
  StandardMessageCodec::ValueTypeLabel::Comparator::compare(
    const AnyOrNone& lhs, const AnyOrNone& rhs) noexcept(false) {
    auto lHash = Hashing::hash(lhs);
    auto rHash = Hashing::hash(rhs);
    return lHash > rHash;
  }

  constexpr StandardMessageCodec::ValueTypeLabel::ValueTypeLabel(Type type) noexcept :
      m_value(type) {}

  StandardMessageCodec::ValueTypeLabel::ValueTypeLabel(uint8_t type) noexcept(false) {
    if (type > LABEL_FLOAT32_LIST) {
      throw ArgumentError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Type cannot be bigger than the biggest value in the enum "
          "'" FLUTTER_IOT_STRINGIFY(StandardMessageCodec::ValueTypeLabel::Type) "'."));
    }

    m_value = static_cast<Type>(type);
  }

  StandardMessageCodec::ValueTypeLabel::ValueTypeLabel(
    const ValueTypeLabel& source) noexcept :
      m_value(source.m_value) {}

  StandardMessageCodec::ValueTypeLabel&
  StandardMessageCodec::ValueTypeLabel::operator=(const ValueTypeLabel& source) noexcept {
    m_value = static_cast<Type>(source.m_value);
    return *this;
  }

  StandardMessageCodec::ValueTypeLabel&
  StandardMessageCodec::ValueTypeLabel::operator=(Type source) noexcept {
    m_value = static_cast<Type>(source);
    return *this;
  }

  StandardMessageCodec::ValueTypeLabel&
  StandardMessageCodec::ValueTypeLabel::operator=(uint8_t source) noexcept(false) {
    if (source > LABEL_FLOAT32_LIST) {
      throw ArgumentError(
        FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
        std::string(
          "Type cannot be bigger than the biggest value in the enum "
          "'" FLUTTER_IOT_STRINGIFY(StandardMessageCodec::ValueTypeLabel::Type) "'."));
    }

    m_value = static_cast<Type>(source);

    return *this;
  }

  constexpr bool
  operator==(const StandardMessageCodec::ValueTypeLabel& a,
             const StandardMessageCodec::ValueTypeLabel& b) noexcept {
    return a.m_value == b.m_value;
  }

  constexpr bool
  operator!=(const StandardMessageCodec::ValueTypeLabel& a,
             const StandardMessageCodec::ValueTypeLabel& b) noexcept {
    return !(a == b);
  }

  constexpr bool
  operator==(const StandardMessageCodec::ValueTypeLabel& a,
             const StandardMessageCodec::ValueTypeLabel::Type b) noexcept {
    return a.m_value == b;
  }

  constexpr bool
  operator!=(const StandardMessageCodec::ValueTypeLabel& a,
             const StandardMessageCodec::ValueTypeLabel::Type b) noexcept {
    return !(a == b);
  }

  constexpr bool
  operator==(const StandardMessageCodec::ValueTypeLabel::Type a,
             const StandardMessageCodec::ValueTypeLabel& b) noexcept {
    return a == b.m_value;
  }

  constexpr bool
  operator!=(const StandardMessageCodec::ValueTypeLabel::Type a,
             const StandardMessageCodec::ValueTypeLabel& b) noexcept {
    return !(a == b);
  }

  // Allow switch and comparisons.
  constexpr StandardMessageCodec::ValueTypeLabel::operator StandardMessageCodec::
    ValueTypeLabel::Type() const noexcept {
    return m_value;
  }

  std::string
  StandardMessageCodec::ValueTypeLabel::toString(const AnyOrNone& any) noexcept(false) {
    if (!any.has_value()) {
      return "null";
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(bool, any.value())) {
      return FLUTTER_IOT_ANY_CAST(bool, any.value()) ? "true" : "false";
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(float64_t, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(float64_t, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(int32_t, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(int32_t, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(int64_t, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(int64_t, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(std::string, any.value())) {
      return FLUTTER_IOT_ANY_CAST(std::string, any.value());
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Uint8List, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(Uint8List, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Int32List, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(Int32List, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Int64List, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(Int64List, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Float32List, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(Float32List, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Float64List, any.value())) {
      return flutter_iot::toString(FLUTTER_IOT_ANY_CAST(Float64List, any.value()));
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(List, any.value())) {
      const auto& list = FLUTTER_IOT_ANY_CAST(List, any.value());
      return toString(list);
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(ValueTypeLabel::Map, any.value())) {
      const auto& map = FLUTTER_IOT_ANY_CAST(ValueTypeLabel::Map, any.value());
      return toString(map);
    }



    throw ArgumentError(
      FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
      "ValueTypeLabel::toString does not support the following type: '" +
        FLUTTER_IOT_DEMANGLE_ANY(any.value()) + "'.");
  }

  std::string
  StandardMessageCodec::ValueTypeLabel::toString(const List& list) noexcept(false) {
    std::stringstream stream;

    stream << '[';

    if (!list.empty()) {
      size_t i;
      for (i = 0; i < list.size() - 1; ++i) {
        stream << ValueTypeLabel::toString(list[i]) << ", ";
      }
      if (i < list.size()) {
        stream << ValueTypeLabel::toString(list[i]);
      }
    }

    stream << ']';

    return stream.str();
  }

  std::string
  StandardMessageCodec::ValueTypeLabel::toString(const Map& map) noexcept(false) {
    std::stringstream stream;

    stream << '{';

    if (!map.empty()) {
      size_t i;
      auto iter = map.begin();
      auto end = map.end();

      for (i = 0; i < map.size() - 1 && iter != end; ++i, ++iter) {
        const auto& pair = *iter;
        stream << ValueTypeLabel::toString(pair.first) << ": "
               << ValueTypeLabel::toString(pair.second) << ", ";
      }
      if (i < map.size()) {
        const auto& pair = *iter;
        stream << ValueTypeLabel::toString(pair.first) << ": "
               << ValueTypeLabel::toString(pair.second);
      }
    }

    stream << '}';

    return stream.str();
  }



  StandardMessageCodec::StandardMessageCodec() noexcept {}

  StandardMessageCodec::~StandardMessageCodec() noexcept {}

  boost::optional<ByteData>
  StandardMessageCodec::encodeMessage(const AnyOrNone& message) {
    if (!message.has_value())
      return boost::none;

    WriteBuffer buffer;
    writeValue(buffer, message);

    return buffer.done();
  }

  boost::optional<AnyOrNone>
  StandardMessageCodec::decodeMessage(const boost::optional<ByteData>& message) {
    if (!message.has_value())
      return boost::none;

    ReadBuffer buffer(message.value());
    auto result = readValue(buffer);

    if (buffer.hasRemaining())
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, "Message corrupted");

    return result;
  }

  void
  StandardMessageCodec::writeValue(WriteBuffer& buffer, const AnyOrNone& value) const
    noexcept(false) {
    if (!value.has_value()) {
      buffer.putUint8(ValueTypeLabel::LABEL_NULL);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(bool, value.value())) {
      buffer.putUint8(FLUTTER_IOT_ANY_CAST(bool, value.value())
                        ? ValueTypeLabel::LABEL_TRUE
                        : ValueTypeLabel::LABEL_FALSE);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(float64_t, value.value())) {
      // Double precedes int because in JS everything is a double.
      // Therefore in JS, both `is int` and `is double` always
      // return `true`. If we check int first, we'll end up treating
      // all numbers as ints and attempt the int32/int64 conversion,
      // which is wrong. This precedence rule is irrelevant when
      // decoding because we use tags to detect the type of value.

      buffer.putUint8(ValueTypeLabel::LABEL_FLOAT64);
      buffer.putFloat64(FLUTTER_IOT_ANY_CAST(float64_t, value.value()));

      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(int32_t, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_INT32);
      buffer.putInt32(FLUTTER_IOT_ANY_CAST(int32_t, value.value()));
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(int64_t, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_INT64);
      buffer.putInt64(FLUTTER_IOT_ANY_CAST(int64_t, value.value()));
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(std::string, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_STRING);

      const auto& s = FLUTTER_IOT_ANY_CAST(std::string, value.value());
      writeSize(buffer, static_cast<uint32_t>(s.size()));

      Uint8List bytes(s.c_str(), s.c_str() + s.size());
      buffer.putUint8List(bytes);

      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Uint8List, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_UINT8_LIST);
      const auto& list = FLUTTER_IOT_ANY_CAST(Uint8List, value.value());
      writeSize(buffer, static_cast<uint32_t>(list.size()));
      buffer.putUint8List(list);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Int32List, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_INT32_LIST);
      const auto& list = FLUTTER_IOT_ANY_CAST(Int32List, value.value());
      writeSize(buffer, static_cast<uint32_t>(list.size()));
      buffer.putInt32List(list);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Int64List, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_INT64_LIST);
      const auto& list = FLUTTER_IOT_ANY_CAST(Int64List, value.value());
      writeSize(buffer, static_cast<uint32_t>(list.size()));
      buffer.putInt64List(list);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Float32List, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_FLOAT32_LIST);
      const auto& list = FLUTTER_IOT_ANY_CAST(Float32List, value.value());
      writeSize(buffer, static_cast<uint32_t>(list.size()));
      buffer.putFloat32List(list);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(Float64List, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_FLOAT64_LIST);
      const auto& list = FLUTTER_IOT_ANY_CAST(Float64List, value.value());
      writeSize(buffer, static_cast<uint32_t>(list.size()));
      buffer.putFloat64List(list);
      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(ValueTypeLabel::List, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_LIST);

      const auto& list = FLUTTER_IOT_ANY_CAST(ValueTypeLabel::List, value.value());
      writeSize(buffer, static_cast<uint32_t>(list.size()));

      for (const auto& i : list) {
        writeValue(buffer, i);
      }

      return;
    }

    if (FLUTTER_IOT_ANY_IS_TYPE(ValueTypeLabel::Map, value.value())) {
      buffer.putUint8(ValueTypeLabel::LABEL_MAP);

      const auto& map = FLUTTER_IOT_ANY_CAST(ValueTypeLabel::Map, value.value());
      writeSize(buffer, static_cast<uint32_t>(map.size()));

      for (const auto& i : map) {
        writeValue(buffer, i.first);   // key
        writeValue(buffer, i.second);  // value
      }

      return;
    }



    throw ArgumentError(
      FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,
      "StandardMessageCodec::writeValue does not support the following type: '" +
        FLUTTER_IOT_DEMANGLE_ANY(value.value()) + "'.");
  }

  void
  StandardMessageCodec::writeSize(WriteBuffer& buffer, uint32_t value) const noexcept {
    // Dont need assertion, because uint32_t can only
    // contain 4 bytes
    // assert(0 <= value && value <= 0xffffffff);

    if (value < 254u) {
      buffer.putUint8(static_cast<uint8_t>(value));
      return;
    }

    if (value <= 0xFFFFu) {
      buffer.putUint8(254u);
      buffer.putUint16(static_cast<uint16_t>(value));
      return;
    }

    buffer.putUint8(255u);
    buffer.putUint32(value);
  }

  AnyOrNone
  StandardMessageCodec::readValue(ReadBuffer& buffer) const noexcept(false) {
    if (!buffer.hasRemaining())
      throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, "Message corrupted");

    ValueTypeLabel type = buffer.getUint8();
    return readValueOfType(buffer, type);
  }

  AnyOrNone
  StandardMessageCodec::readValueOfType(ReadBuffer& buffer, ValueTypeLabel type) const
    noexcept(false) {
    switch (type) {
      case ValueTypeLabel::LABEL_NULL:
        return AnyOrNone();

      case ValueTypeLabel::LABEL_TRUE:
        return AnyOrNone(true);

      case ValueTypeLabel::LABEL_FALSE:
        return AnyOrNone(false);

      case ValueTypeLabel::LABEL_INT32:
        return AnyOrNone(buffer.getInt32());

      case ValueTypeLabel::LABEL_INT64:
        return AnyOrNone(buffer.getInt64());

      case ValueTypeLabel::LABEL_FLOAT64:
        return AnyOrNone(buffer.getFloat64());

      case ValueTypeLabel::LABEL_LARGE_INT:
      case ValueTypeLabel::LABEL_STRING: {
        uint32_t length = readSize(buffer);
        auto list = buffer.getUint8List(length);
        std::string s(list.begin(), list.end());
        return AnyOrNone(s);
      };

      case ValueTypeLabel::LABEL_UINT8_LIST: {
        uint32_t length = readSize(buffer);
        auto list = buffer.getUint8List(length);
        return AnyOrNone(list);
      };

      case ValueTypeLabel::LABEL_INT32_LIST: {
        uint32_t length = readSize(buffer);
        auto list = buffer.getInt32List(length);
        return AnyOrNone(list);
      };

      case ValueTypeLabel::LABEL_INT64_LIST: {
        uint32_t length = readSize(buffer);
        auto list = buffer.getInt64List(length);
        return AnyOrNone(list);
      };

      case ValueTypeLabel::LABEL_FLOAT32_LIST: {
        uint32_t length = readSize(buffer);
        auto list = buffer.getFloat32List(length);
        return AnyOrNone(list);
      };

      case ValueTypeLabel::LABEL_FLOAT64_LIST: {
        uint32_t length = readSize(buffer);
        auto list = buffer.getFloat64List(length);
        return AnyOrNone(list);
      };

      case ValueTypeLabel::LABEL_LIST: {
        uint32_t length = readSize(buffer);
        ValueTypeLabel::List result(length, AnyOrNone());
        for (size_t i = 0; i < length; ++i) {
          result[i] = readValue(buffer);
        }
        return AnyOrNone(result);
      };

      case ValueTypeLabel::LABEL_MAP: {
        uint32_t length = readSize(buffer);
        ValueTypeLabel::Map result;
        for (size_t i = 0; i < length; ++i) {
          auto key = readValue(buffer);
          auto value = readValue(buffer);
          result.insert(std::make_pair(key, value));
        }
        return AnyOrNone(result);
      };

      default:
        throw FormatException(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, "Message corrupted");
    }
  }

  uint32_t
  StandardMessageCodec::readSize(ReadBuffer& buffer) const noexcept {
    const uint8_t value = buffer.getUint8();

    if (254u == value)
      return static_cast<uint32_t>(buffer.getUint16());

    if (255u == value)
      return buffer.getUint32();

    return static_cast<uint32_t>(value);
  }

}  // namespace flutter_iot
