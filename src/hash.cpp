#include "hash.hpp"

namespace flutter_iot {

  uint64_t
  Jenkins::combine(uint64_t hash, uint64_t objectHash) noexcept {
    hash = 0x1fffffff & (hash + objectHash);
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  uint64_t
  Jenkins::finish(uint64_t hash) noexcept {
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }

  uint64_t
  Jenkins::one_at_a_time(const uint8_t* key, size_t length) noexcept {
    uint64_t result = 0;

    size_t i = 0;
    while (i != length) {
      result += key[i++];
      result += result << 10;
      result ^= result >> 6;
    }

    result += result << 3;
    result ^= result >> 11;
    result += result << 15;

    return result;
  }

  uint64_t
  Jenkins::one_at_a_time(const char* key) noexcept {
    uint64_t result = 0;

    for (; '\0' != *key; ++key) {
      result += +(*key);
      result += result << 10;
      result ^= result >> 6;
    }

    result += result << 3;
    result ^= result >> 11;
    result += result << 15;

    return result;
  }



  uint64_t
  hashValues(uint64_t hash01, uint64_t hash02,
             const std::set<uint64_t>& hashes) noexcept {
    uint64_t result = 0;
    result = Jenkins::combine(result, hash01);
    result = Jenkins::combine(result, hash02);

    for (uint64_t hash : hashes) {
      result = Jenkins::combine(result, hash);
    }

    return result;
  }

  uint64_t
  hash(const char* cstr) {
    uint64_t result = 0;

    for (; '\0' != *cstr; ++cstr) {
      result = Jenkins::combine(result, +(*cstr));
    }

    return Jenkins::finish(result);
  }

}  // namespace flutter_iot
