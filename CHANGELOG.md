## 0.3.9

  * Added `hash` overload for `std::string` and `const char*`.

## 0.3.8

  * Fixed include error in ordered_map.hpp

## 0.3.7

  * Fixed some internal include errors.

## 0.3.6

  * Added print format for `ssize_t`.
  
  * Added type trait for checking if a class has a member function with a specific signature.

  * Added type traits for checking if a type fulfills a [named requirement](https://en.cppreference.com/w/cpp/named_req) from the standard.

  * Added operators for converting string and number literals into **AnyOrNone** objects.

## 0.3.5

  * Added CMake Cache Options `FLUTTER_IOT_ANSI_ENABLE` and `FLUTTER_IOT_TEST_LOG_ENABLE`.

  * Added more toString function to **include/utils.hpp**.

    ```cpp
    flutter_iot::toString(bool)
    flutter_iot::toString(const void* pointer)
    flutter_iot::toString(long double value, uint8_t precision)
    flutter_iot::toString(double value, uint8_t precision)
    flutter_iot::toString(float value, uint8_t precision)
    ```

  * Included **include/preprocessor/build_type.hpp** into **include/preprocessor/assert.hpp**, this is needed, otherwise the macro default build type `FLUTTER_IOT_IS_DEBUG_BUILD` is not defined in **include/preprocessor/assert.hpp**.

## 0.3.4

  * Fixed a bug in StandardMessageCodec, where decoding a MethodCall with no arguments caued a `bad_optional_access` exception.

  * Added **scripts/platform_io.sh** which allows you to convert this library to a library for PIO projects.