#ifndef FLUTTER_IOT_INCLUDE_UTILS_HPP
#define FLUTTER_IOT_INCLUDE_UTILS_HPP

#include <cstdio>
#include <cstring>

#include <algorithm>
#include <future>
#include <memory>
#include <string>

#include <boost/type_traits.hpp>



namespace flutter_iot {

  /**
   * @brief This is an internal library function and therefore should not
   *        be called outside the @c flutter_iot namespace.
   *    
   *        Save the contents of @c message so that you can pass it by
   *        reference and call @c std::string.c_str() without having to
   *        fear undefined behaviour because @c message may only be an
   *        rvalue and you should not call @c std::string.c_str() on a
   *        rvalue @c std::string .
   * 
   * @param message The std::string you want to save.
   * 
   * @return Const reference to the saved std::string.
   *         Valid until @c m_saveDefaultErrorMessage is called again with a different std::string.
   */
  const std::string&
  m_saveDefaultErrorMessage(const std::string& message);



  template<typename D, typename S>
  inline D
  bit_cast(const S& source) {
    static_assert(sizeof(D) == sizeof(S),
                  "Source and destination must have the same size");

    D destination;
    // This use of memcpy is safe: source and destination cannot overlap.
    std::memcpy(&destination, &source, sizeof(destination));

    return destination;
  }

  /**
   * @brief Create a copy of @c s and remove all ASCII control characters
   *        from the copy.
   * 
   * @return @c s without any ASCII control characters.
   * 
   * @see https://www.ascii-code.com/
   */
  std::string
  removeControlCharacters(std::string s) noexcept;

  /**
   * @brief Check if @c number is a multiple of @c base .
   */
  template<typename Integer,
           boost::enable_if_t<                    //
             boost::is_integral<Integer>::value,  //
             bool                                 //
             > = true                             //
           >
  constexpr inline bool
  isMultipleOf(Integer number, uint64_t base) {
    return 0 == (number % base);
  }



  template<typename T,
           boost::enable_if_t<                                               ///
             boost::is_arithmetic<T>::value || boost::is_pointer<T>::value,  //
             bool                                                            //
             > = true                                                        ///
           >
  inline std::string
  m_toStringImpl(const char* format, T value) {
    // see https://stackoverflow.com/a/3923207/14972798
    int size = snprintf(nullptr, 0, format, value);
    char buffer[size + 1];
    sprintf(buffer, format, value);

    return std::string(buffer);
  }

  inline std::string
  toString(bool value) {
    return value ? "true" : "false";
  }

  inline std::string
  toString(int value) {
    return m_toStringImpl("%d", value);
  }

  inline std::string
  toString(long int value) {
    return m_toStringImpl("%ld", value);
  }

  inline std::string
  toString(long long int value) {
    return m_toStringImpl("%lld", value);
  }

  inline std::string
  toString(unsigned int value) {
    return m_toStringImpl("%u", value);
  }

  inline std::string
  toString(unsigned long int value) {
    return m_toStringImpl("%lu", value);
  }

  inline std::string
  toString(unsigned long long int value) {
    return m_toStringImpl("%llu", value);
  }

  inline std::string
  toString(float value) {
    return m_toStringImpl("%f", value);
  }

  inline std::string
  toString(float value, uint8_t precision) {
    const std::string fmt = "%." + toString(precision) + "f";
    return m_toStringImpl(fmt.c_str(), value);
  }

  inline std::string
  toString(double value) {
    return m_toStringImpl("%lf", value);
  }

  inline std::string
  toString(double value, uint8_t precision) {
    const std::string fmt = "%." + toString(precision) + "lf";
    return m_toStringImpl(fmt.c_str(), value);
  }

  inline std::string
  toString(long double value) {
    return m_toStringImpl("%Lf", value);
  }

  inline std::string
  toString(long double value, uint8_t precision) {
    const std::string fmt = "%." + toString(precision) + "Lf";
    return m_toStringImpl(fmt.c_str(), value);
  }

  inline std::string
  toString(const void* pointer) {
    return m_toStringImpl("%p", pointer);
  }

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_UTILS_HPP