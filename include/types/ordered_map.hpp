#ifndef FLUTTER_IOT_INCLUDE_TYPES_ORDERED_MAP_HPP
#define FLUTTER_IOT_INCLUDE_TYPES_ORDERED_MAP_HPP

#include <cstdint>

#include <algorithm>
#include <exception>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

#include <boost/type_traits.hpp>

#include "../preprocessor/error_utils.hpp"



namespace flutter_iot {

  /**
   * @brief flutter_iot::OrderedMap is a sorted associative container that
   *        contains key-value pairs with unique keys.
   * 
   *        KV-Pairs are sorted in insertion order.
   */
  template<typename Key,                           //
           typename Value,                         //
           typename KeyEqual = std::equal_to<Key>  //
           >
  class OrderedMap {
   public:
    using key_type = Key;
    using mapped_type = Value;
    using key_equal = KeyEqual;
    using value_type = std::pair<const Key, Value>;
    using value_container = std::vector<value_type>;

    using reference = typename value_container::reference;
    using const_reference = typename value_container::const_reference;

    using iterator = typename value_container::iterator;
    using const_iterator = typename value_container::const_iterator;

    using pointer = typename value_container::pointer;
    using const_pointer = typename value_container::const_pointer;

    using difference_type = typename value_container::difference_type;
    using size_type = typename value_container::size_type;

   private:
    value_container m_entries;
    key_equal m_equal;

   public:
    OrderedMap() = default;

    /**
     * @brief Attempt to preallocate enough memory for
     *        @c capacity number of entries.
     */
    OrderedMap(size_type capacity) noexcept(false) {
      m_entries.reserve(capacity);
    }

    /**
     * @brief Constructs the container with the contents of the
     *        range [ @c first , @c last ). 
     *        
     *        If multiple entries in the range have keys that compare
     *        equivalent, it is unspecified which element is inserted
     *        (pending LWG2844).
     */
    template<class InputIt>
    OrderedMap(InputIt first, InputIt last) {
      for (; first != last; ++first) {
        insert(*first);
      }
    }

    /**
     * @brief Constructs the container with the contents of the
     *        initializer list @c ilist .
     *        
     *        If multiple entries in the initializer list have keys
     *        that compare equivalent, it is unspecified which element
     *        is inserted (pending LWG2844).
     * 
     * @see https://cplusplus.github.io/LWG/issue2844
     */
    OrderedMap(std::initializer_list<value_type> ilist) {
      for (auto& pair : ilist) {
        insert(pair);
      }
    }

    OrderedMap(const OrderedMap&) = default;

    OrderedMap(OrderedMap&&) = default;

    virtual ~OrderedMap() = default;

    OrderedMap&
    operator=(const OrderedMap&) = default;

    OrderedMap&
    operator=(OrderedMap&&) = default;

    /**
     * @brief Returns a reference to the value that is mapped to a
     *        key equivalent to @c key , performing an insertion if
     *        such key does not already exist.
     * 
     * @return Reference to the mapped value of the new entry if no
     *         entry with key key existed. Otherwise a reference to
     *         the mapped value of the existing entry whose key is
     *         equivalent to @c key . 
     */
    Value&
    operator[](const Key& key) {
      auto end_ = end();
      auto found = find(key);

      if (found != end_)
        return found->second;

      m_entries.push_back(value_type(  ///
        std::piecewise_construct,      //
        std::forward_as_tuple(key),    //
        std::tuple<>()                 //
        ));                            ///

      auto it = begin() + (size() - 1);
      return it->second;
    }

    /**
     * @brief Returns a reference to the value that is mapped to a
     *        key equivalent to @c key , performing an insertion if
     *        such key does not already exist.
     * 
     * @return Reference to the mapped value of the new entry if no
     *         entry with key key existed. Otherwise a reference to
     *         the mapped value of the existing entry whose key is
     *         equivalent to @c key . 
     */
    inline Value&
    operator[](Key&& key) {
      return this->operator[](key);
    }

    /**
     * @return A reference to the mapped value of the element
     *         with key equivalent to @c key .
     *         If no such element exists, an exception of type
     *         @c std::out_of_range is thrown. 
     */
    Value&
    at(const Key& key) {
      auto end_ = end();
      auto found = find(key);

      if (found == end_)
        throw std::out_of_range(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE +
                                "There is no entry with the given key.");

      return found->second;
    }

    /**
     * @return A reference to the mapped value of the element
     *         with key equivalent to @c key .
     *         If no such element exists, an exception of type
     *         @c std::out_of_range is thrown. 
     */
    const Value&
    at(const Key& key) const {
      auto end_ = end();
      auto found = find(key);

      if (found == end_)
        throw std::out_of_range(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE +
                                "There is no entry with the given key.");

      return found->second;
    }

    /**
     * @return The number of entries with key that compares equal to the
     *         specified argument @c key , which is either 1 or 0 since
     *         this container does not allow duplicates.
     */
    size_type
    count(const Key& key) const {
      auto end_ = end();
      auto found = find(key);

      if (found == end_)
        return 0;

      return 1;
    }

    /**
     * @brief Finds an entry with key equivalent to @c key .
     * 
     * @return Iterator to an entry with key equivalent to @c key .
     *         If no such entry is found, past-the-end (see @c end() )
     *         iterator is returned.  
     */
    iterator
    find(const Key& key) {
      return std::find_if(begin(), end(), [&](const_reference ref) {
        return m_equal(key, ref.first);
      });
    }

    /**
     * @brief Finds an entry with key equivalent to @c key .
     * 
     * @return Iterator to an entry with key equivalent to @c key .
     *         If no such entry is found, past-the-end (see @c end() )
     *         iterator is returned.  
     */
    const_iterator
    find(const Key& key) const {
      return std::find_if(begin(), end(), [&](const_reference ref) {
        return m_equal(key, ref.first);
      });
    }

    /**
     * @return A range containing all entries with key @c key in
     *         the container. The range is defined by two iterators,
     *         the first pointing to the first entry of the wanted
     *         range and the second pointing past the last entry of the range.
     */
    inline std::pair<iterator, iterator>
    equal_range(const Key& key) {
      auto found = find(key);
      return std::make_pair(found, end());
    }

    /**
     * @return A range containing all entries with key @c key in
     *         the container. The range is defined by two iterators,
     *         the first pointing to the first entry of the wanted
     *         range and the second pointing past the last entry of the range.
     */
    inline std::pair<const_iterator, const_iterator>
    equal_range(const Key& key) const {
      auto found = find(key);
      return std::make_pair(found, end());
    }

    /**
     * @brief Returns a read/write iterator that points to the first
     *        entry in the @c OrderedMap . Iteration is done in ordinary
     *        element order.
     */
    inline iterator
    begin() noexcept {
      return m_entries.begin();
    }

    /**
     * @brief Returns a read-only (constant) iterator that points to the
     *        first entry in the @c OrderedMap . Iteration is done in ordinary
     *        element order.
     */
    inline const_iterator
    begin() const noexcept {
      return m_entries.begin();
    }

    /**
     * @brief Returns a read-only (constant) iterator that points to the
     *        first entry in the @c OrderedMap . Iteration is done in ordinary
     *        element order.
     */
    inline const_iterator
    cbegin() const noexcept {
      return m_entries.cbegin();
    }

    /**
     * @brief Returns a read/write iterator that points one past the last
     *        entry in the @c OrderedMap . Iteration is done in ordinary
     *        element order.
     */
    inline iterator
    end() noexcept {
      return m_entries.end();
    }

    /**
     * @brief Returns a read-only (constant) iterator that points one past
     *        the last entry in the @c OrderedMap . Iteration is done in ordinary
     *        element order.
     */
    inline const_iterator
    end() const noexcept {
      return m_entries.end();
    }

    /**
     * @brief Returns a read-only (constant) iterator that points one past
     *        the last element in the %vector. Iteration is done in
     *        ordinary element order.
     */
    inline const_iterator
    cend() const noexcept {
      return m_entries.cend();
    }

    /**
     * @brief Returns @b true if the @c OrderedMap is empty.
     *        ( Thus @c begin() would equal @c end() .)
     */
    inline bool
    empty() const noexcept {
      return m_entries.empty();
    }

    /**
     * @brief Returns the number of entries in the @c OrderedMap .
     */
    inline size_type
    size() const noexcept {
      return m_entries.size();
    }

    /**
     * @brief Returns the maximum number of entries the @c OrderedMap is
     *        able to hold due to system or library implementation limitations.
     */
    inline size_type
    max_size() const noexcept {
      return m_entries.max_size();
    }

    /**
     * @brief Erases all the entries. Note that this function only erases
     *        the entries, and that if the entries themselves hold pointers,
     *        the pointed-to memory is not touched in any way. Managing the
     *        pointer is the user's responsibility.
     */
    inline void
    clear() noexcept {
      m_entries.clear();
    }

    /**
     * @brief Inserts value. 
     * 
     * @return A bool denoting whether the insertion took place ( @b true if insertion
     *         happened, @b false if it did not) and a pair consisting of an iterator
     *         to the inserted entry, if the insertion took place, or to the
     *         entry that prevented the insertion. 
     */
    std::pair<iterator, bool>
    insert(const value_type& value) {
      auto begin_ = begin();
      auto end_ = end();
      for (; begin_ != end_; ++begin_) {
        // found same key -> insert failed
        if (m_equal(begin_->first, value.first))
          return std::make_pair(begin_, false);
      }

      m_entries.push_back(value);
      return std::make_pair(begin() + (size() - 1), true);
    }

    /**
     * @brief Inserts value. 
     * 
     * @return A bool denoting whether the insertion took place ( @b true if insertion
     *         happened, @b false if it did not) and a pair consisting of an iterator
     *         to the inserted entry, if the insertion took place, or to the
     *         entry that prevented the insertion. 
     */
    template<class P,                                          //
             boost::enable_if_t<                               ///
               std::is_same<value_type, P>::value,             //
               bool                                            //
               > = true,                                       ///
                                                               //
             boost::enable_if_t<                               ///
               std::is_constructible<value_type, P&&>::value,  //
               bool                                            //
               > = true                                        ///
             >
    inline std::pair<iterator, bool>
    insert(P&& value) {
      value_type pair(std::forward<P&&>(value));
      return insert(pair);
    }

    /**
     * @brief Inserts value, using hint as a non-binding suggestion to
     *        where the search should start. 
     * 
     * @return Returns an iterator to the inserted entry, or to the
     *         entry that prevented the insertion.
     */
    iterator
    insert(const_iterator hint, const value_type& value) {
      // Search from [hint;end[
      auto begin1 = hint;
      auto end_ = end();
      for (; begin1 != end_; ++begin1) {
        // found same key -> insert failed
        if (m_equal(begin1->first, value.first))
          return begin1;
      }

      // Search from [begin;hint[
      auto begin2 = begin();
      for (; begin2 != hint; ++begin2) {
        // found same key -> insert failed
        if (m_equal(begin2->first, value.first))
          return begin2;
      }

      // Insert value and return iterator to value
      m_entries.push_back(value);
      return begin() + (size() - 1);
    }

    /**
     * @brief Inserts value, using hint as a non-binding suggestion to
     *        where the search should start. 
     * 
     * @return Returns an iterator to the inserted entry, or to the
     *         entry that prevented the insertion.
     */
    template<class P,
             boost::enable_if_t<                               ///
               std::is_same<value_type, P>::value,             //
               bool                                            //
               > = true,                                       ///
                                                               //
             boost::enable_if_t<                               ///
               std::is_constructible<value_type, P&&>::value,  //
               bool                                            //
               > = true                                        ///
             >
    inline iterator
    insert(const_iterator hint, P&& value) {
      value_type pair(std::forward<P&&>(value));
      return insert(hint, pair);
    }

    /**
     * @brief Inserts elements from range [ @c first , @c last ).
     *        If multiple elements in the range have keys that compare
     *        equivalent, it is unspecified which element is inserted
     *        (pending LWG2844).
     * 
     * @see https://cplusplus.github.io/LWG/issue2844
     */
    template<class InputIt>
    inline void
    insert(InputIt first, InputIt last) {
      for (; first != last; ++first) {
        insert(*first);
      }
    }

    /**
     * @brief Inserts elements from initializer list @c ilist .
     *        If multiple elements in the range have keys that
     *        compare equivalent, it is unspecified which element
     *        is inserted (pending LWG2844).
     * 
     * @see https://cplusplus.github.io/LWG/issue2844
     */
    inline void
    insert(std::initializer_list<value_type> ilist) {
      for (auto& value : ilist) {
        insert(value);
      }
    }

    /**
     * @brief Inserts a new element into the container constructed
     *        in-place with the given args if there is no entry with
     *        the key in the container. 
     * 
     *        Careful use of emplace allows the new entry to be constructed
     *        while avoiding unnecessary copy or move operations. The
     *        constructor of the new entry (i.e. std::pair<const Key, T>)
     *        is called with exactly the same arguments as supplied to emplace,
     *        forwarded via std::forward<Args>(args).... The element may be
     *        constructed even if there already is an element with the key in
     *        the container, in which case the newly constructed element will
     *        be destroyed immediately. 
     * 
     * @return A bool denoting whether the insertion took place ( @b true if insertion
     *         happened, @b false if it did not) and a pair consisting of an iterator
     *         to the inserted entry, if the insertion took place, or to the
     *         entry that prevented the insertion. 
     */
    template<class... Args>
    std::pair<iterator, bool>
    emplace(Args&&... args) {
      value_type value(std::forward<Args>(args)...);

      auto begin_ = begin();
      auto end_ = end();
      for (; begin_ != end_; ++begin_) {
        // found same key -> insert failed
        if (m_equal(begin_->first, value.first))
          return std::make_pair(begin_, false);
      }

      m_entries.push_back(std::move(value));
      return std::make_pair(begin() + (size() - 1), true);
    }

    /**
     * @brief Inserts a new entry to the container, using hint
     *        as a suggestion where the entry should go. The
     *        entry is constructed in-place, i.e. no copy or move
     *        operations are performed.
     * 
     *        The constructor of the entry type (value_type, that is,
     *        std::pair<const Key, T>) is called with exactly the same
     *        arguments as supplied to the function, forwarded with
     *        std::forward<Args>(args)... .  
     * 
     * @return An iterator to the newly inserted entry.
     *         
     *         If the insertion failed because the entry already exists,
     *         returns an iterator to the already existing entry with
     *         the equivalent key. 
     */
    template<class... Args>
    iterator
    emplace_hint(const_iterator hint, Args&&... args) {
      value_type value(std::forward<Args>(args)...);

      // Search from [hint;end[
      auto begin1 = hint;
      auto end_ = end();
      for (; begin1 != end_; ++begin1) {
        // found same key -> insert failed
        if (m_equal(begin1->first, value.first))
          return begin1;
      }

      // Search from [begin;hint[
      auto begin2 = begin();
      for (; begin2 != hint; ++begin2) {
        // found same key -> insert failed
        if (m_equal(begin2->first, value.first))
          return begin2;
      }

      // Insert value and return iterator to value
      m_entries.push_back(std::move(value));
      return begin() + (size() - 1);
    }

    /**
     * @brief  Removes the element at @c position . 
     * 
     * @return Iterator following the last removed entry.
     *         
     *         If position refers to the last entry, then the @c end()
     *         iterator is returned.
     */
    inline iterator
    erase(iterator position) {
      m_entries.erase(position);
    }

    /**
     * @brief Removes the entry at @c position . 
     * 
     * @return Iterator following the last removed entry.
     *         
     *         If position refers to the last entry, then the @c end()
     *         iterator is returned.
     */
    inline iterator
    erase(const_iterator position) {
      return m_entries.erase(position);
    }

    /**
     * @brief Removes the entries in the range [ @c first , @c last ),
     *        which must be a valid range in this.
     * 
     * @return Iterator following the last removed entry.
     * 
     *        If @c last == @c end() prior to removal, then the updated
     *        @c end() iterator is returned.
     *        
     *        If [ @c first , @c last ) is an empty range, then
     *        @c last is returned. 
     */
    inline iterator
    erase(const_iterator first, const_iterator last) {
      return m_entries.erase(first, last);
    }

    /**
     * @brief Removes the entry (if one exists) with the
     *        key equivalent to @c key.
     * 
     * @return Number of entries removed (0 or 1).
     */
    size_type
    erase(const Key& key) {
      auto end_ = end();
      auto found = find(key);

      if (found == end_)
        return 0;

      erase(found);

      return 1;
    }

    /**
     * @brief Exchanges the contents of the container with those of @c other.
     *        Does not invoke any move, copy, or swap operations on individual elements.
     * 
     *        All iterators and references remain valid. The past-the-end
     *        iterator is invalidated. 
     */
    inline void
    swap(OrderedMap<Key, Value, KeyEqual>& other) {
      m_entries.swap(other.m_entries);
    }
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPES_ORDERED_MAP_HPP