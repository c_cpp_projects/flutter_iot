#ifndef FLUTTER_IOT_INCLUDE_TYPES_TYPES_UTILS_HPP
#define FLUTTER_IOT_INCLUDE_TYPES_TYPES_UTILS_HPP

#include <functional>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#include <boost/any.hpp>
#include <boost/optional.hpp>

#include "any_or_none.hpp"
#include "ordered_map.hpp"

namespace flutter_iot {

  using float32_t = float;
  using float64_t = double;
  using float128_t = long double;

  template<typename T>
  using List = std::vector<T>;

  template<typename KeyEqual>
  using Map = OrderedMap<AnyOrNone, AnyOrNone, KeyEqual>;

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPES_TYPES_UTILS_HPP