#ifndef FLUTTER_IOT_INCLUDE_TYPES_ERROR_DETAILS_HPP
#define FLUTTER_IOT_INCLUDE_TYPES_ERROR_DETAILS_HPP

#include <string>

#include <boost/optional.hpp>

namespace flutter_iot {

  using ErrorDetails = boost::optional<std::string>;

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPES_ERROR_DETAILS_HPP