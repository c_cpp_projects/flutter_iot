#ifndef FLUTTER_IOT_INCLUDE_TYPES_TYPED_DATA_HPP
#define FLUTTER_IOT_INCLUDE_TYPES_TYPED_DATA_HPP

#include <cinttypes>

#include <functional>
#include <ostream>
#include <string>
#include <vector>

#include "types_utils.hpp"

namespace flutter_iot {

  using Uint8List = std::vector<uint8_t>;
  using Uint16List = std::vector<uint16_t>;
  using Uint32List = std::vector<uint32_t>;
  using Uint64List = std::vector<uint64_t>;

  using Int8List = std::vector<int8_t>;
  using Int16List = std::vector<int16_t>;
  using Int32List = std::vector<int32_t>;
  using Int64List = std::vector<int64_t>;

  using Float32List = std::vector<float32_t>;
  using Float64List = std::vector<float64_t>;



  std::string
  toString(const Uint8List& list) noexcept;

  std::string
  toString(const Uint16List& list) noexcept;

  std::string
  toString(const Uint32List& list) noexcept;

  std::string
  toString(const Uint64List& list) noexcept;

  std::string
  toString(const Int8List& list) noexcept;

  std::string
  toString(const Int16List& list) noexcept;

  std::string
  toString(const Int32List& list) noexcept;

  std::string
  toString(const Int64List& list) noexcept;

  std::string
  toString(const Float32List& list) noexcept;

  std::string
  toString(const Float64List& list) noexcept;



  /**
   * @brief  Write Uint8List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Uint8List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Uint8List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Uint16List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Uint16List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Uint16List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Uint32List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Uint32List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Uint32List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Uint64List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Uint64List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Uint64List& list) noexcept {
    stream << toString(list);
    return stream;
  }


  /**
   * @brief  Write Int8List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Int8List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Int8List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Int16List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Int16List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Int16List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Int32List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Int32List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Int32List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Int64List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Int64List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Int64List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Float32List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Float32List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Float32List& list) noexcept {
    stream << toString(list);
    return stream;
  }

  /**
   * @brief  Write Float64List to a stream.
   *  
   * @param stream  Output stream.
   * @param list  Float64List to write out.
   * 
   * @return  Reference to the output stream.
   */
  template<typename CharType, typename Traits>
  inline std::basic_ostream<CharType, Traits>&
  operator<<(std::basic_ostream<CharType, Traits>& stream,
             const Float64List& list) noexcept {
    stream << toString(list);
    return stream;
  }

}  // namespace flutter_iot

// Add std::hash specialization for
// Uint8List, Uint16List, Uint32List, Uint64List,
// Int8List, Int16List, Int32List, Int64List,
// Float32List, Float64List
namespace std {

  template<>
  struct hash<flutter_iot::Uint8List> :
      public __hash_base<size_t, flutter_iot::Uint8List> {
    size_t
    operator()(const flutter_iot::Uint8List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size());
    }
  };

  template<>
  struct hash<flutter_iot::Uint16List> :
      public __hash_base<size_t, flutter_iot::Uint16List> {
    size_t
    operator()(const flutter_iot::Uint16List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 2);
    }
  };

  template<>
  struct hash<flutter_iot::Uint32List> :
      public __hash_base<size_t, flutter_iot::Uint32List> {
    size_t
    operator()(const flutter_iot::Uint32List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 4);
    }
  };

  template<>
  struct hash<flutter_iot::Uint64List> :
      public __hash_base<size_t, flutter_iot::Uint64List> {
    size_t
    operator()(const flutter_iot::Uint64List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 8);
    }
  };

  template<>
  struct hash<flutter_iot::Int8List> : public __hash_base<size_t, flutter_iot::Int8List> {
    size_t
    operator()(const flutter_iot::Int8List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size());
    }
  };

  template<>
  struct hash<flutter_iot::Int16List> :
      public __hash_base<size_t, flutter_iot::Int16List> {
    size_t
    operator()(const flutter_iot::Int16List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 2);
    }
  };

  template<>
  struct hash<flutter_iot::Int32List> :
      public __hash_base<size_t, flutter_iot::Int32List> {
    size_t
    operator()(const flutter_iot::Int32List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 4);
    }
  };

  template<>
  struct hash<flutter_iot::Int64List> :
      public __hash_base<size_t, flutter_iot::Int64List> {
    size_t
    operator()(const flutter_iot::Int64List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 8);
    }
  };

  template<>
  struct hash<flutter_iot::Float32List> :
      public __hash_base<size_t, flutter_iot::Float32List> {
    size_t
    operator()(const flutter_iot::Float32List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 4);
    }
  };

  template<>
  struct hash<flutter_iot::Float64List> :
      public __hash_base<size_t, flutter_iot::Float64List> {
    size_t
    operator()(const flutter_iot::Float64List& list) const noexcept {
      return std::_Hash_impl::hash(list.data(), list.size() * 8);
    }
  };

}  // namespace std

#endif  // FLUTTER_IOT_INCLUDE_TYPES_TYPED_DATA_HPP