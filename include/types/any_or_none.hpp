#ifndef FLUTTER_IOT_INCLUDE_TYPES_ANY_OR_NONE_HPP
#define FLUTTER_IOT_INCLUDE_TYPES_ANY_OR_NONE_HPP

#include <utility>

#include <boost/any.hpp>
#include <boost/optional.hpp>

namespace flutter_iot {

  using AnyOrNone = boost::optional<boost::any>;

  template<typename T = boost::none_t>
  inline AnyOrNone
  makeAnyOrNone(T&& value = boost::none) {
    return AnyOrNone(std::forward<T&&>(value));
  }

  template<typename OptionalType>
  inline AnyOrNone
  makeAnyOrNone(boost::optional<OptionalType>&& value) {
    return value.has_value() ? AnyOrNone(std::forward<OptionalType&>(value.value()))
                             : AnyOrNone(boost::none);
  }

}  // namespace flutter_iot


#endif  // FLUTTER_IOT_INCLUDE_TYPES_ANY_OR_NONE_HPP