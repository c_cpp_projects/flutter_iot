#ifndef FLUTTER_IOT_INCLUDE_LITERALS_HPP
#define FLUTTER_IOT_INCLUDE_LITERALS_HPP

#include <string>

#include <boost/type_traits.hpp>

#include "types/any_or_none.hpp"



namespace flutter_iot {

  namespace literals {

    inline AnyOrNone operator"" _any(const char* str, size_t len) {
      return AnyOrNone(std::string(str, len));
    }

    inline AnyOrNone operator"" _any(const char16_t* str, size_t len) {
      return AnyOrNone(std::u16string(str, len));
    }

    inline AnyOrNone operator"" _any(const char32_t* str, size_t len) {
      return AnyOrNone(std::u32string(str, len));
    }

    inline AnyOrNone operator"" _any(const wchar_t* str, size_t len) {
      return AnyOrNone(std::wstring(str, len));
    }

    inline AnyOrNone operator"" _any(unsigned long long int value) {
      return AnyOrNone(value);
    }

    inline AnyOrNone operator"" _any(long double value) {
      return AnyOrNone(value);
    }

  }  // namespace literals

}  // namespace flutter_iot



#endif  // FLUTTER_IOT_INCLUDE_LITERALS_HPP