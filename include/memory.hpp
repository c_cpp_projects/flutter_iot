#ifndef FLUTTER_IOT_INCLUDE_MEMORY_HPP
#define FLUTTER_IOT_INCLUDE_MEMORY_HPP

#include <cstdlib>

#include <string>

#include "exceptions/bad_allocation_exception.hpp"
#include "preprocessor/error_utils.hpp"

namespace flutter_iot {

  /**
   * @brief Allocates size bytes of uninitialized storage. 
   *        
   *        Throws a @c BadAllocationException exception in case no storage
   *        could be allocated. 
   * 
   *        ( Calls @c std::malloc with @c size )
   * 
   * @return A pointer to the lowest (first) byte in the allocated memory block
   *         that is suitably aligned for any scalar type, or @c nullptr if
   *         the passed @c size was zero.
   */
  void*
  flutter_malloc(std::size_t size) noexcept(false);

  /**
   * @brief Allocates memory for an array of @c num objects of size @c size and
   *        initializes it to all bits zero. 
   *        
   *        Throws a @c BadAllocationException exception in case no storage
   *        could be allocated. 
   * 
   *        ( Calls @c std::calloc with @c num and @c size )
   * 
   * @return A pointer to the lowest (first) byte in the allocated memory block
   *         that is suitably aligned for any scalar type, or @c nullptr if
   *         the passed @c size was zero.
   */
  void*
  flutter_calloc(std::size_t num, std::size_t size) noexcept(false);

  /**
   * @brief Reallocates the given area of memory. It must be previously allocated by
   *        @c flutter_malloc , @c flutter_calloc or @c flutter_realloc
   *        and not yet freed with @c flutter_free , otherwise, the results
   *        are undefined. 
   * 
   *        Throws a @c BadAllocationException exception if there is not enough
   *        memory, but the old memory block is not freed. 
   *        
   *        ( Calls @c std::realloc with @c ptr and @c new_size )
   *  
   * @return If @c ptr is a @c nullptr then return @c nullptr .
   * 
   *         If @c ptr is @b not a @c nullptr , but @c new_size is zero 
   *         then return @c nullptr and do not modify @c ptr .
   * 
   *         If @c ptr is not a @c nullptr and @c new_size is greater than zero,
   *         return a pointer to the beginning of newly allocated memory. The original
   *         pointer @c ptr is invalidated and any access to it is undefined behavior.
   */
  void*
  flutter_realloc(void* ptr, std::size_t new_size) noexcept(false);

  /**
   * @brief Deallocates the space previously allocated by @c flutter_malloc ,
   *        @c flutter_calloc or @c flutter_realloc .
   * 
   *        If @c ptr is a nullptr, the function does nothing. 
   * 
   *        The behavior is undefined if the value of @c ptr does not equal a value
   *        returned earlier by @c flutter_malloc , @c flutter_calloc ,
   *        @c flutter_realloc . 
   * 
   *        The behavior is undefined if the memory area referred to by @c ptr has already
   *        been deallocated, that is, @c flutter_free or @c flutter_realloc
   *        has already been called with @c ptr as the argument and no calls to
   *        @c flutter_malloc , @c flutter_calloc , or
   *        @c flutter_realloc resulted in a pointer equal to @c ptr afterwards. 
   * 
   *        The behavior is undefined if after @c flutter_free returns, an access
   *        is made through the pointer @c ptr (unless another allocation function happened to
   *        result in a pointer value equal to @c ptr ) 
   *        
   *        ( Calls @c std::free with @c ptr )
   */
  void
  flutter_free(void* ptr) noexcept;

}  // namespace flutter_iot


#endif  // FLUTTER_IOT_INCLUDE_MEMORY_HPP