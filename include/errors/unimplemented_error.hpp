#ifndef FLUTTER_IOT_INCLUDE_ERRORS_UNIMPLEMENTED_ERROR_HPP
#define FLUTTER_IOT_INCLUDE_ERRORS_UNIMPLEMENTED_ERROR_HPP

#include <string>

#include "flutter_error.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/dart-core/UnimplementedError-class.html
   */
  class UnimplementedError : public FlutterError {
   public:
    UnimplementedError() noexcept;

    UnimplementedError(const std::string& defaultErrorMessage,
                       const std::string& what = "") noexcept;

    UnimplementedError(const char* defaultErrorMessage, const char* what = "") noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_ERRORS_UNIMPLEMENTED_ERROR_HPP