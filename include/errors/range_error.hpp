#ifndef FLUTTER_IOT_INCLUDE_ERRORS_RANGE_ERROR_HPP
#define FLUTTER_IOT_INCLUDE_ERRORS_RANGE_ERROR_HPP

#include <cstdint>

#include "argument_error.hpp"

namespace flutter_iot {

  class IndexError;

  /**
   * @see https://api.flutter.dev/flutter/dart-core/RangeError-class.html
   */
  class RangeError : public ArgumentError {
   public:
    using OptionalInt64 = boost::optional<int64_t>;

   protected:
    OptionalInt64 m_start;
    OptionalInt64 m_end;

   protected:
    RangeError(const std::string& defaultErrorMessage, ErrorDetails message,
               ErrorDetails name, ErrorDetails value, OptionalInt64 start,
               OptionalInt64 end) noexcept;

   private:
    void
    m_setWhat() noexcept;

   public:
    /**
     * @brief Create a new RangeError with the given @c message.  
     */
    RangeError(const std::string& defaultErrorMessage,
               ErrorDetails message = boost::none) noexcept;

    virtual ~RangeError() noexcept;

    /**
     * @brief Creates a new @c RangeError stating that @c index is not a valid index
     *        into @c indexable.
     * 
     *        An optional name can specify the argument @c name that has the
     *        invalid value, and the @c message can override the default
     *        error description.
     * 
     *        The @c length is the length of @c indexable at the time of the error.
     *        If @c length is omitted, it defaults to @c indexable.size .
     */
    static IndexError
    index(const std::string& defaultErrorMessage, int64_t index, int64_t length,
          ErrorDetails name = boost::none, ErrorDetails message = boost::none);

    /**
     * @brief Create a new @c RangeError for a value being outside the valid range.
     *        
     *        The allowed range is from @c minValue to @c maxValue, inclusive.
     *        If @c minValue or @c maxValue are @b boost::none, the range is
     *        infinite in that direction.
     * 
     *        For a range from 0 to the length of something, end exclusive,
     *        use @c RangeError.index .
     * 
     *        An optional name can specify the argument name that has the invalid
     *        value, and the message can override the default error description.
     */
    static RangeError
    range(const std::string& defaultErrorMessage, int64_t invalidNumber,
          OptionalInt64 minValue = boost::none, OptionalInt64 maxValue = boost::none,
          ErrorDetails name = boost::none, ErrorDetails message = boost::none) noexcept;

    /**
     * @brief Create a new @c RangeError with a @c message for the given value.
     *        
     *        An optional name can specify the argument @c name that has the
     *        invalid @c value, and the @c message can override the default
     *        error description.
     */
    static RangeError
    value(const std::string& defaultErrorMessage, int64_t value,
          ErrorDetails name = boost::none, ErrorDetails message = boost::none) noexcept;

    /**
     * @brief Check that an integer value is non-negative.
     * 
     *        Throws if the @c value is negative.
     * 
     *        If name or message are provided, they are used as the parameter
     *        @c name and @c message text of the thrown error. If name is omitted,
     *        it defaults to @b "index" .
     * 
     * @return @c value if it is not negative.
     */
    static int64_t
    checkNotNegative(const std::string& defaultErrorMessage, int64_t value,
                     ErrorDetails name = boost::none,
                     ErrorDetails message = boost::none) noexcept(false);

    /**
     * @brief Check that @c index is a valid index into an @c indexable @c Container.
     * 
     *        Throws if @c index is not a valid index into @c indexable.
     * 
     *        An indexable object is one that has a @c indexable.size and
     *        that accepts an index if 0 <= index < indexable.size.
     *        The size of @c indexable is cast to @c int64_t .
     * 
     *        If @c name or @c message are provided, they are used as the parameter
     *        name and message text of the thrown error. If name is omitted,
     *        it defaults to @b "index" .
     * 
     *        If length is provided, it is used as the length of the indexable
     *        object, otherwise the length is found as indexable.length.
     * 
     * @tparam Container https://en.cppreference.com/w/cpp/named_req/Container
     * 
     * @return @c index if it is a valid index.
     */
    static int64_t
    checkValidIndex(const std::string& defaultErrorMessage, int64_t index, int64_t length,
                    ErrorDetails name = boost::none,
                    ErrorDetails message = boost::none) noexcept(false);

    /**
     * @brief Check that a range represents a slice of an indexable object.
     * 
     *        Throws if the range is not valid for an indexable object with the
     *        given length. A range is valid for an indexable object with a
     *        given length.
     * 
     *        if 0 <= [start] <= [end] <= [length]. An end of null is considered
     *        equivalent to length.
     * 
     *        The @c startName and @c endName defaults to @b "start" and @b "end",
     *        respectively.
     * 
     * @return The actual end value, which is @c length if @c end is @b boost::none ,
     *         and end otherwise.
     */
    static int64_t
    checkValidRange(const std::string& defaultErrorMessage, int64_t length, int64_t start,
                    OptionalInt64 end = boost::none, ErrorDetails startName = boost::none,
                    ErrorDetails endName = boost::none,
                    ErrorDetails message = boost::none) noexcept(false);

    /**
     * @brief Check that an integer value lies in a specific interval.
     * 
     *        Throws if @c value is not in the interval. The interval is from
     *        @c minValue to @c maxValue, both inclusive.
     * 
     *        If @c name or @c message are provided, they are used as the
     *        parameter name and message text of the thrown error.
     * 
     * @return @c value if it is in the interval.
     */
    static int64_t
    checkValueInInterval(const std::string& defaultErrorMessage, int64_t value,
                         int64_t minValue, int64_t maxValue,
                         ErrorDetails name = boost::none,
                         ErrorDetails message = boost::none) noexcept(false);

    /**
     * @brief The maximum value that value is allowed to assume. 
     */
    const OptionalInt64&
    getEnd() const noexcept;

    /**
     * @brief The minimum value that value is allowed to assume. 
     */
    const OptionalInt64&
    getStart() const noexcept;
  };

  /**
   * @see https://api.flutter.dev/flutter/dart-core/IndexError-class.html
   */
  class IndexError : public RangeError {
   private:
    int64_t m_valueAsInt;
    int64_t m_length;

   private:
    void
    m_setWhat() noexcept;

   public:
    /**
     * @brief Creates a new IndexError stating that @c invalidValue is not a valid
     *        index into @c indexable.
     * 
     * @param length The length of @c indexable at the time of the error. 
     *               If @c length is @b boost::none , it defaults to @c indexable.size .
     * @param message Used as part of the string representation of the error.
     */
    IndexError(const std::string& defaultErrorMessage, int64_t invalidValue,
               int64_t length, ErrorDetails name = boost::none,
               ErrorDetails message = boost::none) noexcept;

    virtual ~IndexError();

    /**
     * @brief The length of @c indexable at the time of the error.
     */
    int64_t
    getLength() const noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_ERRORS_RANGE_ERROR_HPP