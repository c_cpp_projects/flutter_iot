#ifndef FLUTTER_IOT_INCLUDE_ERRORS_FLUTTER_ERROR_HPP
#define FLUTTER_IOT_INCLUDE_ERRORS_FLUTTER_ERROR_HPP

#include <exception>
#include <string>

namespace flutter_iot {

  /**
   * @brief FlutterError objects thrown in the case of a program failure.
   *        
   *        An Error object represents a program failure that the programmer
   *        should have avoided.
   *        
   *        Examples include calling a function with invalid arguments, or even
   *        with the wrong number of arguments, or calling it at a time when
   *        it is not allowed.
   *        
   *        These are not errors that a caller should expect or catch - if they occur,
   *        the program is erroneous, and terminating the program may be the safest response.
   * 
   * @see https://api.flutter.dev/flutter/dart-core/Error-class.html
   */
  class FlutterError : public std::exception {
   private:
    std::string m_defaultErrorMessage;
    std::string m_what;

   protected:
    FlutterError() noexcept;

    FlutterError(const std::string& defaultErrorMessage,
                 const std::string& what = "") noexcept;

    FlutterError(const char* defaultErrorMessage, const char* what = "") noexcept;

    void
    m_setWhat(const std::string& what) noexcept;

    const std::string&
    m_getWhat() const noexcept;

   public:
    virtual ~FlutterError() noexcept;

    virtual const char*
    what() const noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_ERRORS_FLUTTER_ERROR_HPP