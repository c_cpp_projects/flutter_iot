#ifndef FLUTTER_IOT_INCLUDE_ERRORS_STATE_ERROR_HPP
#define FLUTTER_IOT_INCLUDE_ERRORS_STATE_ERROR_HPP

#include <string>

#include "flutter_error.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/dart-core/StateError-class.html
   */
  class StateError : public FlutterError {
   public:
    StateError() noexcept;

    StateError(const std::string& defaultErrorMessage,
               const std::string& what = "") noexcept;

    StateError(const char* defaultErrorMessage, const char* what = "") noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_ERRORS_STATE_ERROR_HPP