#ifndef FLUTTER_IOT_INCLUDE_ERRORS_ARGUMENT_ERROR_HPP
#define FLUTTER_IOT_INCLUDE_ERRORS_ARGUMENT_ERROR_HPP

#include "../types/error_details.hpp"

#include "flutter_error.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/dart-core/ArgumentError-class.html
   */
  class ArgumentError : public FlutterError {
   private:
    ErrorDetails m_message;
    ErrorDetails m_name;
    ErrorDetails m_value;

   protected:
    ArgumentError(const std::string& defaultErrorMessage, ErrorDetails message,
                  ErrorDetails name, ErrorDetails value) noexcept;

    static std::string
    m_getWhat(const ErrorDetails& message, const ErrorDetails& name,
              const ErrorDetails& value, const std::string& errorName,
              const ErrorDetails& explanation) noexcept;

   private:
    void
    m_setWhat() noexcept;

   public:
    /**
     * @param message Description of the problem with an argument.
     * @param name Should be the name of the parameter which was invalid,
     *             if provided.
     */
    ArgumentError(const std::string& defaultErrorMessage, ErrorDetails message,
                  ErrorDetails name = boost::none) noexcept;

    /**
     * @brief Creates an argument error for a @c boost::optional argument that
     *        must not be @c boost::none .
     * 
     * @param name The name of the parameter.
     */
    static ArgumentError
    notNone(const std::string& defaultErrorMessage,
            ErrorDetails name = boost::none) noexcept;

    /**
     * @brief Creates an argument error for a pointer argument that
     *        must not be @c nullptr .
     * 
     * @param name The name of the parameter.
     */
    static ArgumentError
    notNullptr(const std::string& defaultErrorMessage,
               ErrorDetails name = boost::none) noexcept;

    /**
     * @brief Creates error containing the a std::string representation of the
     *        invalid value.
     * 
     * @param value A string representation of the invalid value .
     * @param name The name of the parameter.
     * @param message Extra details.
     */
    static ArgumentError
    value(const std::string& defaultErrorMessage, std::string value,
          ErrorDetails name = boost::none, ErrorDetails message = boost::none) noexcept;

    /**
     * @brief  Message describing the problem.
     */
    const ErrorDetails&
    getMessage() const noexcept;

    /**
     * @brief  Name of the invalid argument.
     */
    const ErrorDetails&
    getName() const noexcept;

    /**
     * @brief String representation of the invalid value.
     */
    const ErrorDetails&
    getValue() const noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_ERRORS_ARGUMENT_ERROR_HPP
