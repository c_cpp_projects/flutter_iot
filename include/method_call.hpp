#ifndef FLUTTER_IOT_INCLUDE_METHOD_CALL_HPP
#define FLUTTER_IOT_INCLUDE_METHOD_CALL_HPP

#include <string>

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/services/MethodCall-class.html
   */
  template<typename ArgsContainer>
  class MethodCall {
   private:
    std::string m_method;
    ArgsContainer m_arguments;

   public:
    /**
    * @brief Construct a new Method Call object.
    * 
    * @param method The name of the method to be called.
    * 
    * @param arguments The arguments for the method.
    */
    MethodCall(std::string method, const ArgsContainer& arguments) noexcept :
        m_method(method), m_arguments(arguments) {}

    MethodCall(std::string method, ArgsContainer&& arguments) noexcept :
        m_method(method), m_arguments(std::move(arguments)) {}

    virtual ~MethodCall() noexcept = default;

    const std::string&
    getMethod() const noexcept {
      return m_method;
    }

    const ArgsContainer&
    getArguments() const noexcept {
      return m_arguments;
    }
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_METHOD_CALL_HPP