#ifndef FLUTTER_IOT_INCLUDE_EXCEPTIONS_MISSING_PLUGIN_EXCEPTION_HPP
#define FLUTTER_IOT_INCLUDE_EXCEPTIONS_MISSING_PLUGIN_EXCEPTION_HPP

#include <string>

#include "flutter_exception.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/services/MissingPluginException-class.html
   */
  class MissingPluginException : public FlutterException {
   public:
    MissingPluginException() noexcept;

    MissingPluginException(const std::string& defaultErrorMessage,
                           const std::string& what = "") noexcept;

    MissingPluginException(const char* defaultErrorMessage,
                           const char* what = "") noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_EXCEPTIONS_MISSING_PLUGIN_EXCEPTION_HPP