#ifndef FLUTTER_IOT_INCLUDE_EXCEPTIONS_FLUTTER_EXCEPTION_HPP
#define FLUTTER_IOT_INCLUDE_EXCEPTIONS_FLUTTER_EXCEPTION_HPP

#include <exception>
#include <string>

namespace flutter_iot {

  /**
   * @brief An FlutterException is intended to convey information to the user
   *        about a failure, so that the error can be addressed programmatically.
   *        
   *        It is intended to be caught, and it should contain useful data fields.
   * 
   * @see https://api.flutter.dev/flutter/dart-core/Exception-class.html
   */
  class FlutterException : public std::exception {
   private:
    std::string m_defaultErrorMessage;
    std::string m_what;

   protected:
    void
    m_setWhat(const std::string& what) noexcept;

    const std::string&
    m_getWhat() const noexcept;

   public:
    FlutterException() noexcept;

    FlutterException(const std::string& defaultErrorMessage,
                     const std::string& what = "") noexcept;

    FlutterException(const char* defaultErrorMessage, const char* what = "") noexcept;

    virtual const char*
    what() const noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_EXCEPTIONS_FLUTTER_EXCEPTION_HPP