#ifndef FLUTTER_IOT_INCLUDE_EXCEPTIONS_BAD_ALLOCATION_EXCEPTION_HPP
#define FLUTTER_IOT_INCLUDE_EXCEPTIONS_BAD_ALLOCATION_EXCEPTION_HPP

#include <string>

#include "flutter_exception.hpp"

namespace flutter_iot {

  /**
   * @brief Only thrown by @c flutter_malloc , @c flutter_calloc or @c flutter_realloc .
   */
  class BadAllocationException : public FlutterException {
   public:
    BadAllocationException() noexcept;

    BadAllocationException(const std::string& defaultErrorMessage,
                           const std::string& what = "") noexcept;

    BadAllocationException(const char* defaultErrorMessage,
                           const char* what = "") noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_EXCEPTIONS_BAD_ALLOCATION_EXCEPTION_HPP
