#ifndef FLUTTER_IOT_INCLUDE_EXCEPTIONS_PLATFORM_EXCEPTION_HPP
#define FLUTTER_IOT_INCLUDE_EXCEPTIONS_PLATFORM_EXCEPTION_HPP

#include <sstream>
#include <string>

#include <boost/optional.hpp>

#include "../types/error_details.hpp"

#include "flutter_exception.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/services/PlatformException-class.html
   */
  class PlatformException : public FlutterException {
   private:
    std::string m_code;
    ErrorDetails m_message;
    ErrorDetails m_details;
    ErrorDetails m_stacktrace;

   protected:
    virtual void
    m_setWhat() noexcept;

   public:
    PlatformException(const std::string& defaultErrorMessage, const std::string& code,
                      const ErrorDetails& message = boost::none,
                      const ErrorDetails& details = boost::none,
                      const ErrorDetails& stacktrace = boost::none) noexcept;

    PlatformException(const PlatformException& source) noexcept;

    PlatformException&
    operator=(const PlatformException& source) noexcept;

    /**
     * @brief An error code.
     */
    const std::string&
    getCode() const noexcept;

    /**
     * @brief A human-readable error message, possibly @b boost::none .
     */
    const ErrorDetails&
    getMessage() const noexcept;

    /**
     * @brief Error details, possibly @b boost::none .
     */
    const ErrorDetails&
    getDetails() const noexcept;

    /**
     * @brief Stacktrace for the error, possibly @b boost::none .
     */
    const ErrorDetails&
    getStackTrace() const noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_EXCEPTIONS_PLATFORM_EXCEPTION_HPP