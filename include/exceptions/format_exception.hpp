#ifndef FLUTTER_IOT_INCLUDE_EXCEPTIONS_FORMAT_EXCEPTION_HPP
#define FLUTTER_IOT_INCLUDE_EXCEPTIONS_FORMAT_EXCEPTION_HPP

#include <string>

#include "flutter_exception.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/dart-core/FormatException-class.html
   */
  class FormatException : public FlutterException {
   public:
    FormatException() noexcept;

    FormatException(const std::string& defaultErrorMessage,
                    const std::string& what = "") noexcept;

    FormatException(const char* defaultErrorMessage, const char* what = "") noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_EXCEPTIONS_FORMAT_EXCEPTION_HPP