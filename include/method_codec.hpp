#ifndef FLUTTER_IOT_INCLUDE_METHOD_CODEC_HPP
#define FLUTTER_IOT_INCLUDE_METHOD_CODEC_HPP

#include <string>

#include <boost/optional.hpp>

#include "buffer.hpp"
#include "exceptions/format_exception.hpp"
#include "exceptions/platform_exception.hpp"
#include "message_codec.hpp"
#include "method_call.hpp"
#include "types/any_or_none.hpp"
#include "utils.hpp"

namespace flutter_iot {

  /**
   * @see https://api.flutter.dev/flutter/services/MethodCodec-class.html
   */
  template<typename ArgsContainer>
  class MethodCodec {
   public:
    using ArgsContainerType = ArgsContainer;
    using MethodCallType = MethodCall<ArgsContainer>;

   public:
    /**
      * @brief Encodes @c call into an @c EncodedMessage .
      */
    virtual ByteData
    encodeMethodCall(const MethodCallType& call) = 0;

    /**
      * @brief Decodes @c methodCall from an @c EncodedMessage to a @c MethodCall .
      */
    virtual MethodCallType
    decodeMethodCall(const boost::optional<ByteData>& call) = 0;

    /**
     * @brief Decodes the specified @c envelope to a @c DecodedEnvelope .
     */
    virtual AnyOrNone
    decodeEnvelope(const ByteData& envelope) = 0;

    /**
     * @brief Encodes a successful @c result into a @c EncodedMessage .
     */
    virtual ByteData
    encodeSuccessEnvelope(const AnyOrNone& result) = 0;

    /**
     * @brief Encodes an error result into an @c EncodedMessage .
     * 
     * @param code The specified error code.
     * @param message A human-readable error.
     * @param details Error details, possibly @c boost::none .
     */
    virtual ByteData
    encodeErrorEnvelope(std::string code,
                        boost::optional<std::string> message = boost::none,
                        ErrorDetails details = boost::none) = 0;
  };

  /**
   * @see https://api.flutter.dev/flutter/services/StandardMethodCodec-class.html
   */
  class StandardMethodCodec :
      public MethodCodec<StandardMessageCodec::ValueTypeLabel::Map> {
    // The codec method calls, and result envelopes as outlined below. This format
    // must match the Android and iOS counterparts.
    //
    // * Individual values are encoded using [StandardMessageCodec].
    // * Method calls are encoded using the concatenation of the encoding
    //   of the method name String and the arguments value.
    // * Reply envelopes are encoded using first a single byte to distinguish the
    //   success case (0) from the error case (1). Then follows:
    //   * In the success case, the encoding of the result value.
    //   * In the error case, the concatenation of the encoding of the error code
    //     string, the error message string, and the error details value.

   public:
    using MessageCodecType = StandardMessageCodec;

   private:
    MessageCodecType m_codec;

   public:
    StandardMethodCodec(MessageCodecType codec = MessageCodecType()) noexcept;

    virtual ~StandardMethodCodec() noexcept;

    MessageCodecType
    getCodec() const noexcept;

    /**
    * @brief Encodes @c call into an @c StandardEncodedMessage .
    */
    ByteData
    encodeMethodCall(const MethodCallType& call) override;

    /**
    * @brief Decodes @c methodCall from an @c StandardEncodedMessage to a @c MethodCall .
    */
    MethodCallType
    decodeMethodCall(const boost::optional<ByteData>& call) override;

    /**
    * @brief Decodes the specified @c envelope to a @c DecodedEnvelope .
    */
    AnyOrNone
    decodeEnvelope(const ByteData& envelope) override;

    /**
    * @brief Encodes a successful @c result into a @c StandardEncodedMessage .
    */
    ByteData
    encodeSuccessEnvelope(const AnyOrNone& result) override;

    /**
    * @brief Encodes an error result into a @c StandardEncodedMessage .
    * 
    * @param code The specified error code.
    * @param message A human-readable error.
    * @param details Error details, possibly @c boost::none .
    */
    ByteData
    encodeErrorEnvelope(std::string code,
                        boost::optional<std::string> message = boost::none,
                        ErrorDetails details = boost::none) override;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_METHOD_CODEC_HPP