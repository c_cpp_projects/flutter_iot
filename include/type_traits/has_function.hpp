#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_HAS_FUNCTION_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_HAS_FUNCTION_HPP

#include <cstddef>

#include <boost/type_traits.hpp>

/**
 * @brief Implementation of flutter_iot::has_function
 *        
 *        A struct called @c has_function_name can be used to check if
 *        a type @c T has a function called @c T::name .
 * 
 * @see https://www.boost.org/doc/libs/1_78_0/libs/type_traits/doc/html/boost_typetraits/reference/has_new_operator.html
 */
namespace flutter_iot {

#define DEFINE_HAS_FUNCTION(returnType, functionName, argTypesList, constQualification) \
  namespace detail {                                                                    \
    namespace has_function {                                                            \
      template<class U, U x>                                                            \
      struct test;                                                                      \
                                                                                        \
      template<typename T>                                                              \
      struct functionName##_impl {                                                      \
        template<class U>                                                               \
        static boost::type_traits::yes_type                                             \
        check_sig(U*, test<returnType(U::*) argTypesList constQualification,            \
                           &U::functionName>* = nullptr);                               \
        template<class U>                                                               \
        static boost::type_traits::no_type                                              \
        check_sig(...);                                                                 \
                                                                                        \
        BOOST_STATIC_CONSTANT(                                                          \
          unsigned, s1 = sizeof(functionName##_impl<T>::check_sig<T>(nullptr)));        \
                                                                                        \
        BOOST_STATIC_CONSTANT(bool,                                                     \
                              value = (s1 == sizeof(boost::type_traits::yes_type)));    \
      };                                                                                \
                                                                                        \
      template<class T>                                                                 \
      struct functionName :                                                             \
          public boost::integral_constant<bool, functionName##_impl<T>::value> {};      \
                                                                                        \
    } /* namespace has_function */                                                      \
  }   /* namespace detail */



  DEFINE_HAS_FUNCTION(typename U::iterator, begin, (), /* no const qualification */)
  template<typename T>
  using has_function_begin = detail::has_function::begin<T>;

  DEFINE_HAS_FUNCTION(typename U::reverse_iterator, rbegin, (),
                      /* no const qualification */)
  template<typename T>
  using has_function_rbegin = detail::has_function::rbegin<T>;

  DEFINE_HAS_FUNCTION(typename U::iterator, end, (), /* no const qualification */)
  template<typename T>
  using has_function_end = detail::has_function::end<T>;

  DEFINE_HAS_FUNCTION(typename U::reverse_iterator, rend, (),
                      /* no const qualification */)
  template<typename T>
  using has_function_rend = detail::has_function::rend<T>;

  DEFINE_HAS_FUNCTION(typename U::const_iterator, cbegin, (), const)
  template<typename T>
  using has_function_cbegin = detail::has_function::cbegin<T>;

  DEFINE_HAS_FUNCTION(typename U::const_reverse_iterator, crbegin, (), const)
  template<typename T>
  using has_function_crbegin = detail::has_function::crbegin<T>;

  DEFINE_HAS_FUNCTION(typename U::const_iterator, cend, (), const)
  template<typename T>
  using has_function_cend = detail::has_function::cend<T>;

  DEFINE_HAS_FUNCTION(typename U::const_reverse_iterator, crend, (), const)
  template<typename T>
  using has_function_crend = detail::has_function::crend<T>;

  DEFINE_HAS_FUNCTION(void, swap, (U&), /* no const qualification */)
  template<typename T>
  using has_function_swap = detail::has_function::swap<T>;

  DEFINE_HAS_FUNCTION(typename U::size_type, size, (), const)
  template<typename T>
  using has_function_size = detail::has_function::size<T>;

  DEFINE_HAS_FUNCTION(typename U::size_type, max_size, (), const)
  template<typename T>
  using has_function_max_size = detail::has_function::max_size<T>;

  DEFINE_HAS_FUNCTION(bool, empty, (), const)
  template<typename T>
  using has_function_empty = detail::has_function::empty<T>;

  // Have to use boost::is_member_function_pointer for get_allocator, because
  // the has_function struct cannot see functions that have been inherited from
  // base classes.
  template<typename T>
  using has_function_get_allocator =
    boost::is_member_function_pointer<decltype(&T::get_allocator)>;

#undef DEFINE_HAS_FUNCTION

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_HAS_FUNCTION_HPP