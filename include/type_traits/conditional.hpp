#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_CONDITIONAL_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_CONDITIONAL_HPP

#include <boost/type_traits.hpp>

namespace flutter_iot {

  template<bool b>
  using Conditional = boost::conditional<b, boost::true_type, boost::false_type>;

  template<bool b>
  using ConditionalType = typename Conditional<b>::type;

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_CONDITIONAL_HPP