#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_HAS_TYPEDEF_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_HAS_TYPEDEF_HPP

#include <boost/type_traits.hpp>

/**
 * @brief Implementation of flutter_iot::has_typedef
 *        
 *        A struct called @c has_typedef_name can be used to check if
 *        a type @c T has a typedef called @c T::name .
 * 
 * @see https://www.boost.org/doc/libs/1_78_0/libs/type_traits/doc/html/boost_typetraits/reference/has_new_operator.html
 */
namespace flutter_iot {

/**
 * @brief Declare a struct called @c has_##typedef in the namescape
 *        @c has_typedef . 
 *        
 *        The declared struct can be used to check if a type
 *        @c T has a typedef called @c T::value_type .
 */
#define DEFINE_HAS_TYPEDEF(typedef_)                                                 \
  namespace detail {                                                                 \
    namespace has_typedef {                                                          \
      template<class U>                                                              \
      struct test;                                                                   \
                                                                                     \
      template<typename T>                                                           \
      struct typedef_##_impl {                                                       \
        template<class U>                                                            \
        static boost::type_traits::yes_type check_##typedef_(                        \
          U*, test<typename U::typedef_>* = nullptr);                                \
                                                                                     \
        template<class U>                                                            \
        static boost::type_traits::no_type check_##typedef_(...);                    \
                                                                                     \
                                                                                     \
        BOOST_STATIC_CONSTANT(                                                       \
          unsigned, s1 = sizeof(typedef_##_impl<T>::check_##typedef_<T>(nullptr)));  \
                                                                                     \
        BOOST_STATIC_CONSTANT(bool,                                                  \
                              value = (s1 == sizeof(boost::type_traits::yes_type))); \
      };                                                                             \
                                                                                     \
      template<class T>                                                              \
      struct typedef_ :                                                              \
          public boost::integral_constant<bool, typedef_##_impl<T>::value> {};       \
    } /* namespace has_typedef */                                                    \
  }   /* namespace detail*/



  DEFINE_HAS_TYPEDEF(value_type)
  template<typename T>
  using has_typedef_value_type = detail::has_typedef::value_type<T>;

  DEFINE_HAS_TYPEDEF(reference)
  template<typename T>
  using has_typedef_reference = detail::has_typedef::reference<T>;

  DEFINE_HAS_TYPEDEF(const_reference)
  template<typename T>
  using has_typedef_const_reference = detail::has_typedef::const_reference<T>;

  DEFINE_HAS_TYPEDEF(iterator)
  template<typename T>
  using has_typedef_iterator = detail::has_typedef::iterator<T>;

  DEFINE_HAS_TYPEDEF(const_iterator)
  template<typename T>
  using has_typedef_const_iterator = detail::has_typedef::const_iterator<T>;

  DEFINE_HAS_TYPEDEF(difference_type)
  template<typename T>
  using has_typedef_difference_type = detail::has_typedef::difference_type<T>;

  DEFINE_HAS_TYPEDEF(size_type)
  template<typename T>
  using has_typedef_size_type = detail::has_typedef::size_type<T>;

  DEFINE_HAS_TYPEDEF(reverse_iterator)
  template<typename T>
  using has_typedef_reverse_iterator = detail::has_typedef::reverse_iterator<T>;

  DEFINE_HAS_TYPEDEF(const_reverse_iterator)
  template<typename T>
  using has_typedef_const_reverse_iterator =
    detail::has_typedef::const_reverse_iterator<T>;

  DEFINE_HAS_TYPEDEF(allocator_type)
  template<typename T>
  using has_typedef_allocator_type = detail::has_typedef::allocator_type<T>;

#undef DEFINE_HAS_TYPEDEF

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_HAS_TYPEDEF_HPP