#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_SWAPPABLE_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_SWAPPABLE_HPP

#include <boost/type_traits.hpp>

// See std::is_swappable from C++17

namespace flutter_iot {

  namespace details {

    template<typename T>
    struct is_swappable_impl {
      template<typename = decltype(std::swap(std::declval<T&>(), std::declval<T&>()))>
      static boost::true_type
      test(int);

      template<typename>
      static boost::false_type
      test(...);

      typedef decltype(test<T>(0)) type;
    };

  }  // namespace details

  template<typename T>
  struct is_swappable : public details::is_swappable_impl<T>::type {};

}  // namespace flutter_iot


#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_SWAPPABLE_HPP