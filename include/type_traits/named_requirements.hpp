#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_NAMED_REQUIREMENTS_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_NAMED_REQUIREMENTS_HPP

#include <type_traits>

#include <boost/type_traits.hpp>

#include "conditional.hpp"
#include "has_function.hpp"
#include "has_typedef.hpp"
#include "is_swappable.hpp"
#include "to_reference.hpp"
#include "type_traits_utils.hpp"

/**
 * @brief Conditions for checking if a type T fulfills the requirements
 *        listed in https://en.cppreference.com/w/cpp/named_req
 */

namespace flutter_iot {

  /**
   * @brief Check if @c Container fulfills the named requirements
   *        for a Container.
   * 
   * @see https://en.cppreference.com/w/cpp/named_req/Container
   */
  template<typename Container>
  using IsContainer = ConditionalType<
    // Check all necessary typedefs
    has_typedef_value_type<Container>::value &&       //
    has_typedef_reference<Container>::value &&        //
    has_typedef_const_reference<Container>::value &&  //
    has_typedef_iterator<Container>::value &&         //
    has_typedef_const_iterator<Container>::value &&   //
    has_typedef_difference_type<Container>::value &&  //
    has_typedef_size_type<Container>::value &&        //
    // Check all necessary functions
    boost::is_default_constructible<Container>::value &&  //
    boost::is_copy_constructible<Container>::value &&     //
    std::is_move_constructible<Container>::value &&       //
    boost::is_copy_assignable<Container>::value &&        //
    std::is_move_assignable<Container>::value &&          //
    boost::is_destructible<Container>::value &&           //
    has_function_begin<Container>::value &&               //
    has_function_end<Container>::value &&                 //
    has_function_cbegin<Container>::value &&              //
    has_function_cend<Container>::value &&                //
    boost::has_equal_to<Container>::value &&              //
    boost::has_not_equal_to<Container>::value &&          //
    is_swappable<Container>::value &&                     //
    has_function_swap<Container>::value &&                //
    has_function_size<Container>::value &&                //
    has_function_max_size<Container>::value &&            //
    has_function_empty<Container>::value>;

  /**
   * @brief Check if @c ReversibleContainer fulfills the named requirements
   *        for a ReversibleContainer.
   * 
   * @see https://en.cppreference.com/w/cpp/named_req/ReversibleContainer
   */
  template<typename ReversibleContainer>
  using IsReversibleContainer =
    ConditionalType<IsContainer<ReversibleContainer>::value &&  //
                    // Check all necessary typedefs
                    has_typedef_reverse_iterator<ReversibleContainer>::value &&        //
                    has_typedef_const_reverse_iterator<ReversibleContainer>::value &&  //
                    // Check all necessary functions
                    has_function_rbegin<ReversibleContainer>::value &&   //
                    has_function_rend<ReversibleContainer>::value &&     //
                    has_function_crbegin<ReversibleContainer>::value &&  //
                    has_function_crend<ReversibleContainer>::value>;

  /**
   * @brief Check if @c AllocatorAwareContainer fulfills the named requirements
   *        for a ReversibleContainer.
   * 
   * @see https://en.cppreference.com/w/cpp/named_req/AllocatorAwareContainer
   */
  template<typename AllocatorAwareContainer>
  using IsAllocatorAwareContainer = ConditionalType<
    IsContainer<AllocatorAwareContainer>::value &&  //
    // Check all necessary typedefs
    has_typedef_allocator_type<AllocatorAwareContainer>::value &&  //
    // Check all necessary functions
    has_function_get_allocator<AllocatorAwareContainer>::value &&          //
    boost::is_constructible<                                               ///
      AllocatorAwareContainer,                                             //
      typename AllocatorAwareContainer::allocator_type                     //
      >::value &&                                                          ///
    boost::is_constructible<                                               ///
      AllocatorAwareContainer,                                             //
      ToLValueRef<AllocatorAwareContainer>,                                //
      typename AllocatorAwareContainer::allocator_type                     //
      >::value &&                                                          ///
    boost::is_constructible<                                               ///
      AllocatorAwareContainer,                                             //
      ToConstRValueRef<AllocatorAwareContainer>,                           //
      typename AllocatorAwareContainer::allocator_type                     //
      >::value &&                                                          ///
    std::is_nothrow_move_constructible<AllocatorAwareContainer>::value &&  //
    boost::is_constructible<                                               ///
      AllocatorAwareContainer,                                             //
      ToRValueRef<AllocatorAwareContainer>,                                //
      typename AllocatorAwareContainer::allocator_type                     //
      >::value>;

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_NAMED_REQUIREMENTS_HPP