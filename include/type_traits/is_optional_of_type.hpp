#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_OPTIONAL_OF_TYPE_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_OPTIONAL_OF_TYPE_HPP

#include <boost/optional.hpp>
#include <boost/type_traits.hpp>

#include "../preprocessor/preprocessor_utils.hpp"
#include "has_typedef.hpp"
#include "type_traits_utils.hpp"

namespace flutter_iot {

  namespace detail {

    template<typename Optional, typename T>
    struct IsOptionalOfTypeHelper {
      FLUTTER_IOT_STATIC_CONST(
        bool, value = (flutter_iot::IsContainerOfType<Optional, T>::value &&
                       boost::is_same<Optional, boost::optional<T>>::value));
    };

    template<bool integral_type>
    struct IsOptionalOfTypeSelectHelper {
      template<typename Optional, typename T>
      struct rebind {
        typedef IsOptionalOfTypeHelper<Optional, T> type;
      };
    };

    template<>
    struct IsOptionalOfTypeSelectHelper<false> {
      template<typename Optional, typename T>
      struct rebind {
        typedef boost::false_type type;
      };
    };

    template<typename Optional, typename T>
    struct IsOptionalOfTypeImpl {
      typedef IsOptionalOfTypeSelectHelper<
        flutter_iot::has_typedef_value_type<Optional>::value>
        selector;
      typedef typename selector::template rebind<Optional, T> binder;
      typedef typename binder::type type;

      // FLUTTER_IOT_STATIC_CONST(bool, value = type::value);
      static const bool value = type::value;
    };

  }  // namespace detail

  /**
   * @brief Check if @c Optional is a @c boost::optional<T> .
   */
  template<typename Optional, typename T>
  struct IsOptionalOfType :
      public boost::integral_constant<
        bool, flutter_iot::detail::IsOptionalOfTypeImpl<Optional, T>::value> {};

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_OPTIONAL_OF_TYPE_HPP