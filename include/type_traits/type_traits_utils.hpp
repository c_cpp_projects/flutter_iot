#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_TYPE_TRAITS_UTILS_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_TYPE_TRAITS_UTILS_HPP

#include <iterator>
#include <type_traits>

#include <boost/optional.hpp>
#include <boost/type_traits.hpp>

#include "conditional.hpp"
#include "named_requirements.hpp"

namespace flutter_iot {

  /**
   * @brief Check if @c Container::value_type is the same as @c Type .
   */
  template<typename Container, typename Type>
  using IsContainerOfType =
    ConditionalType<IsContainer<Container>::value &&
                    boost::is_same<typename Container::value_type, Type>::value>;

  /**
   * @brief Check if @c Container::value_type is an arithmetic type.
   */
  template<typename Container>
  using IsContainerOfArithmeticType =
    ConditionalType<IsContainer<Container>::value &&
                    boost::is_arithmetic<typename Container::value_type>::value>;

  template<typename Func, typename... Args>
  using Result = typename std::result_of<Func(Args...)>::type;

  template<typename InputIterator>
  using RequireInputIterator = typename boost::enable_if_t<             //
    boost::is_convertible<                                              //
      typename std::iterator_traits<InputIterator>::iterator_category,  //
      std::input_iterator_tag>::value                                   //
    >;                                                                  //

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_TYPE_TRAITS_UTILS_HPP