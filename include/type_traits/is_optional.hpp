#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_OPTIONAL_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_OPTIONAL_HPP

#include <boost/optional.hpp>
#include <boost/type_traits.hpp>

#include "../preprocessor/preprocessor_utils.hpp"
#include "has_typedef.hpp"

namespace flutter_iot {

  namespace detail {

    template<typename T>
    struct IsOptionalHelper {
      FLUTTER_IOT_STATIC_CONST(
        bool,
        value = (boost::is_same<T, boost::optional<typename T::value_type>>::value));
    };

    template<bool integral_type>
    struct IsOptionalSelectHelper {
      template<typename T>
      struct rebind {
        typedef IsOptionalHelper<T> type;
      };
    };

    template<>
    struct IsOptionalSelectHelper<false> {
      template<typename T>
      struct rebind {
        typedef boost::false_type type;
      };
    };

    template<typename T>
    struct IsOptionalImpl {
      typedef IsOptionalSelectHelper<flutter_iot::has_typedef_value_type<T>::value>
        selector;
      typedef typename selector::template rebind<T> binder;
      typedef typename binder::type type;
      FLUTTER_IOT_STATIC_CONST(bool, value = type::value);
    };

  }  // namespace detail

  /**
   * @brief Check if @c T is a @c boost::optional .
   *        The @c boost::optional::value_type is not needed for this check.
   */
  template<typename T>
  struct IsOptional :
      public boost::integral_constant<bool,
                                      flutter_iot::detail::IsOptionalImpl<T>::value> {};

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_IS_OPTIONAL_HPP