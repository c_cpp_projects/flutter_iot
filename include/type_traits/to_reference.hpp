#ifndef FLUTTER_IOT_INCLUDE_TYPE_TRAITS_TO_REFERENCE_HPP
#define FLUTTER_IOT_INCLUDE_TYPE_TRAITS_TO_REFERENCE_HPP

#include <boost/type_traits.hpp>

namespace flutter_iot {

  template<typename T>
  using ToLValueRef = boost::add_lvalue_reference_t<boost::remove_cv_ref_t<T>>;

  template<typename T>
  using ToConstLValueRef =
    boost::add_lvalue_reference_t<boost::add_const_t<boost::remove_cv_ref_t<T>>>;

  template<typename T>
  using ToRValueRef = boost::add_rvalue_reference_t<boost::remove_cv_ref_t<T>>;

  template<typename T>
  using ToConstRValueRef =
    boost::add_rvalue_reference_t<boost::add_const_t<boost::remove_cv_ref_t<T>>>;

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_TYPE_TRAITS_TO_REFERENCE_HPP