#ifndef FLUTTER_IOT_INCLUDE_HASH_HPP
#define FLUTTER_IOT_INCLUDE_HASH_HPP

#include <climits>
#include <cstdint>

#include <set>
#include <string>

#include <boost/type_traits.hpp>

#include "method_call.hpp"
#include "method_codec.hpp"



namespace flutter_iot {

  /**
   * @brief  Jenkins hash function, optimized for small integers.
   *         
   *         Borrowed from the dart sdk: sdk/lib/math/jenkins_smi_hash.dart.
   */
  class Jenkins final {
   private:
    Jenkins() = delete;

   public:
    static uint64_t
    combine(uint64_t hash, uint64_t objectHash) noexcept;

    static uint64_t
    finish(uint64_t hash) noexcept;

    /**
     * @see https://en.wikipedia.org/wiki/Jenkins_hash_function#one_at_a_time
     */
    static uint64_t
    one_at_a_time(const uint8_t* key, size_t length) noexcept;

    static uint64_t
    one_at_a_time(const char* key) noexcept;
  };

  /**
   * @brief Combine all hashes into a single hash.
   */
  uint64_t
  hashValues(uint64_t hash01, uint64_t hash02, const std::set<uint64_t>& hashes) noexcept;

  /**
   * @brief Combine @c hash01 and @c hash02 into a single hash.
   */
  inline uint64_t
  hashValues(uint64_t hash01, uint64_t hash02) noexcept {
    return hashValues(hash01, hash02, {});
  }

  /**
   * @brief Combine all integers from @c Container into a single hash.
   */
  template<typename Container,
           boost::enable_if_t<                                             ///
             IsContainer<Container>::value &&                              //
               boost::is_integral<typename Container::value_type>::value,  //
             bool                                                          //
             > = true                                                      ///
           >
  inline uint64_t
  hashList(const Container& arguments) noexcept {
    uint64_t result = 0;

    for (auto&& value : arguments) {
      result = Jenkins::combine(result, value);
    }

    return Jenkins::finish(result);
  }

  /**
   * @brief Combine all floats from @c Container into a single hash.
   */
  template<typename Container,
           boost::enable_if_t<                                                   ///
             IsContainer<Container>::value &&                                    //
               boost::is_floating_point<typename Container::value_type>::value,  //
             bool                                                                //
             > = true                                                            ///
           >
  inline uint64_t
  hashList(const Container& arguments) noexcept {
    using ValueType = typename Container::value_type;

    uint64_t result = 0;

    for (auto&& value : arguments) {
      result = Jenkins::combine(
        result, Jenkins::one_at_a_time((uint8_t*) &value, sizeof(ValueType)));
    }

    return Jenkins::finish(result);
  }

  template<typename ArgsContainer>
  inline uint64_t
  hash(MethodCodec<ArgsContainer>& codec,
       const typename MethodCodec<ArgsContainer>::MethodCallType& call) {
    auto encoded = codec.encodeMethodCall(call);
    return hashList(encoded.getBuffer());
  }

  uint64_t
  hash(const char* cstr);

  inline uint64_t
  hash(const std::string& str) {
    return hash(str.c_str());
  }

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_HASH_HPP