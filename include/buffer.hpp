#ifndef FLUTTER_IOT_INCLUDE_BUFFER_HPP
#define FLUTTER_IOT_INCLUDE_BUFFER_HPP

#include <endian.h>

#include <cassert>
#include <cinttypes>
#include <cstring>

#include <algorithm>
#include <array>
#include <iterator>
#include <list>
#include <memory>
#include <type_traits>
#include <vector>

#include <boost/make_unique.hpp>
#include <boost/optional.hpp>
#include <boost/range.hpp>

#include "errors/argument_error.hpp"
#include "errors/range_error.hpp"
#include "errors/state_error.hpp"
#include "preprocessor/print_format.hpp"
#include "type_traits/type_traits_utils.hpp"
#include "types/typed_data.hpp"



namespace flutter_iot {

  /** 
  *  @see https://en.cppreference.com/w/cpp/types/endian
  *  @see https://api.flutter.dev/flutter/dart-typed_data/Endian-class.html
  */
  enum class Endian {
    LITTLE = LITTLE_ENDIAN,
    BIG = BIG_ENDIAN,
    HOST = BYTE_ORDER,
  };

  /**
   * @see https://api.flutter.dev/flutter/dart-typed_data/ByteData-class.html
   */
  class ByteData final {
    // TODO(obemu): Add alignment for storing certain data types
    // like floats or typed data lists.

   public:
    using ByteBuffer = std::vector<uint8_t>;

   private:
    friend class WriteBuffer;
    friend class ReadBuffer;

    ByteBuffer m_buffer;

   private:
    void
    m_checkValidOffset(int64_t offset, size_t typeSize, const std::string& message) const
      noexcept(false);

   public:
    /**
     * @brief Construct a new Byte Data object.
     *        
     *        Moves the elements from @c buffer into @c m_buffer
     */
    ByteData(ByteBuffer&& buffer) noexcept;

    /**
     * @brief Creates a ByteData of the specified @c lengthInBytes, where all
     *        bytes are initially zero.
     */
    ByteData(size_t lengthInBytes) noexcept(false);

    /**
     * @brief Creates a ByteData object of the specified
     *        @c data .
     */
    ByteData(const uint8_t* data, size_t size) noexcept(false);


    template<typename InputIterator,

             typename = RequireInputIterator<InputIterator>>
    inline ByteData(const InputIterator first, const InputIterator last) noexcept(false) :
        m_buffer(first, last) {}

    ByteData(const ByteData& source) noexcept;

    ByteData(ByteData&& source) noexcept;

    ByteData&
    operator=(const ByteData& source) noexcept;

    ByteData&
    operator=(ByteData&& source) noexcept;

    /**
     * @brief Creates a ByteData view on a range of elements of @c data. 
     *        View the bytes of data with indices from @c start until @c end.
     *        The bytes from @c data are copied to the buffer of this @c ByteData . 
     *        
     *        @c start and @c end must satisfy: 0 ≤ @c start ≤ @c end 
     * 
     * @param start The first valid index of data.
     * @param end The last valid index of data
     */
    static ByteData
    sublistCopy(const Uint8List& data, size_t start, size_t end) noexcept(false);

    /**
     * @brief Creates a ByteData view on a range of elements of @c data. 
     *        View the bytes of data with indices from @c start until @c end.
     *        The bytes from @c data are copied to the buffer of this @c ByteData . 
     *        
     *        @c start and @c end must satisfy: 0 ≤ @c start ≤ @c end 
     * 
     * @param start The first valid index of data.
     * @param end The last valid index of data
     */
    static ByteData
    sublistCopy(const Int32List& data, size_t start, size_t end,
                Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Creates a ByteData view on a range of elements of @c data. 
     *        View the bytes of data with indices from @c start until @c end.
     *        The bytes from @c data are copied to the buffer of this @c ByteData . 
     *        
     *        @c start and @c end must satisfy: 0 ≤ @c start ≤ @c end 
     * 
     * @param start The first valid index of data.
     * @param end The last valid index of data
     */
    static ByteData
    sublistCopy(const Int64List& data, size_t start, size_t end,
                Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Creates a ByteData view on a range of elements of @c data. 
     *        View the bytes of data with indices from @c start until @c end.
     *        The bytes from @c data are copied to the buffer of this @c ByteData . 
     *        
     *        @c start and @c end must satisfy: 0 ≤ @c start ≤ @c end 
     * 
     * @param start The first valid index of data.
     * @param end The last valid index of data
     */
    static ByteData
    sublistCopy(const Float32List& data, size_t start, size_t end,
                Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Creates a ByteData view on a range of elements of @c data. 
     *        View the bytes of data with indices from @c start until @c end.
     *        The bytes from @c data are copied to the buffer of this @c ByteData . 
     *        
     *        @c start and @c end must satisfy: 0 ≤ @c start ≤ @c end 
     * 
     * @param start The first valid index of data.
     * @param end The last valid index of data
     */
    static ByteData
    sublistCopy(const Float64List& data, size_t start, size_t end,
                Endian endian = Endian::BIG) noexcept(false);

    const ByteBuffer&
    getBuffer() const noexcept;

    ByteBuffer&
    getBuffer() noexcept;

    size_t
    getLengthInBytes() const noexcept;

    /**
     * @brief Returns the floating point number represented by the four bytes at the
     *        specified @c offset in this object, in IEEE 754 single-precision binary
     *        floating-point format (binary32). 
     */
    float32_t
    getFloat32(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the floating point number represented by the eight bytes at the
     *        specified @c offset in this object, in IEEE 754 double-precision binary
     *        floating-point format (binary64).
     */
    float64_t
    getFloat64(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the (possibly negative) integer represented by the byte at the
     *        specified offset in this object, in two's complement binary representation.
     *        
     * @return A value between -128 and 127, inclusive.
     */
    int8_t
    getInt8(uint64_t offset) const noexcept;

    /**
     * @brief Returns the (possibly negative) integer represented by the two bytes at
     *        the specified @c offset in this object, in two's complement binary form.
     * 
     * @return A value between -2^(15) and 2^(15) - 1, inclusive.
     */
    int16_t
    getInt16(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the (possibly negative) integer represented by the four bytes at
     *        the specified @c offset in this object, in two's complement binary form.
     * 
     * @return A value between -2^(31) and 2^(31) - 1, inclusive. 
     */
    int32_t
    getInt32(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the (possibly negative) integer represented by the eight bytes at
     *        the specified @c offset in this object, in two's complement binary form.
     * 
     * @return A value between -2^(63) and 2^(63) - 1, inclusive. 
     */
    int64_t
    getInt64(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the positive integer represented by the byte at the
     *        specified @c offset in this object, in unsigned binary form.
     * 
     * @return A value between 0 and 255, inclusive. 
     */
    uint8_t
    getUint8(uint64_t offset) const noexcept;

    /**
     * @brief Returns the positive integer represented by the two bytes starting
     *        at the specified @c offset in this object, in unsigned binary form.
     * 
     * @return A value between 0 and 2^(16) - 1, inclusive. 
     */
    uint16_t
    getUint16(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the positive integer represented by the four bytes starting
     *        at the specified @c offset in this object, in unsigned binary form.
     * 
     * @return A value between 0 and 2^(32) - 1, inclusive. 
     */
    uint32_t
    getUint32(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Returns the positive integer represented by the eight bytes starting
     *        at the specified @c offset in this object, in unsigned binary form.
     * 
     * @return A value between 0 and 2^(64) - 1, inclusive. 
     */
    uint64_t
    getUint64(uint64_t offset, Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Sets the four bytes starting at the specified @c offset in this object to
     *        the IEEE 754 single-precision binary floating-point (binary32) representation
     *        of the specified @c value.
     */
    void
    setFloat32(uint64_t offset, float32_t value,
               Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the eight bytes starting at the specified @c offset in this object to
     *        the IEEE 754 double-precision binary floating-point (binary64) representation
     *        of the specified @c value.
     */
    void
    setFloat64(uint64_t offset, float64_t value,
               Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the byte at the specified @c offset in this object to the two's
     *        complement binary representation of the specified @c value, which must fit
     *        in a single byte.
     * 
     * @param value An int8_t between -128 and 127, inclusive.
     */
    void
    setInt8(uint64_t offset, int8_t value) noexcept;

    /**
     * @brief Sets the two bytes starting at the specified @c offset in this object to
     *        the two's complement binary representation of the specified @c value, which
     *        must fit in two bytes.
     * 
     * @param value An int16_t between -2^(15) and 2^(15) - 1, inclusive.
     */
    void
    setInt16(uint64_t offset, int16_t value, Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the four bytes starting at the specified @c offset in this object to
     *        the two's complement binary representation of the specified @c value, which
     *        must fit in four bytes.
     * 
     * @param value An int32_t between -2^(31) and 2^(31) - 1, inclusive.
     */
    void
    setInt32(uint64_t offset, int32_t value, Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the eight bytes starting at the specified @c offset in this object to
     *        the two's complement binary representation of the specified @c value, which
     *        must fit in eight bytes.
     * 
     * @param value An int64_t between -2^(63) and 2^(63) - 1, inclusive.
     */
    void
    setInt64(uint64_t offset, int64_t value, Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the byte at the specified @c offset in this object to the
     *        unsigned binary representation of the specified @c value, which
     *        must fit in a single byte.
     * 
     * @param value An uint8_t between 0 and 255, inclusive.
     */
    void
    setUint8(uint64_t offset, uint8_t value) noexcept;

    /**
     * @brief Sets the two bytes starting at the specified @c offset in this object
     *        to the unsigned binary representation of the specified @c value, which
     *        must fit in two bytes.
     * 
     * @param value An uint16_t between 0 and 2^(16) - 1, inclusive.
     */
    void
    setUint16(uint64_t offset, uint16_t value,
              Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the four bytes starting at the specified @c offset in this object
     *        to the unsigned binary representation of the specified @c value, which
     *        must fit in four bytes.
     * 
     * @param value An uint32_t between 0 and 2^(32) - 1, inclusive.
     */
    void
    setUint32(uint64_t offset, uint32_t value,
              Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Sets the eight bytes starting at the specified @c offset in this object
     *        to the unsigned binary representation of the specified @c value, which
     *        must fit in eight bytes.
     * 
     * @param value An uint64_t between 0 and 2^(64) - 1, inclusive.
     */
    void
    setUint64(uint64_t offset, uint64_t value,
              Endian endian = Endian::BIG) noexcept(false);

    /**
     * @brief Creates a @c Float32List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 4 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown.
     * 
     * @param length The amount of float32_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData .
     *                      Must be divisible by 4. 
     */
    Float32List
    asFloat32List(size_t length, uint64_t offsetInBytes = 0,
                  Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Float64List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 8 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown.
     * 
     * @param length The amount of float64_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 8.
     */
    Float64List
    asFloat64List(size_t length, uint64_t offsetInBytes = 0,
                  Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Int8List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     * 
     * @param length The amount of int8_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 1.
     */
    Int8List
    asInt8List(size_t length, uint64_t offsetInBytes = 0) const noexcept(false);

    /**
     * @brief Creates a @c Int16List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 2 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     * 
     * @param length The amount of int16_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 2.
     */
    Int16List
    asInt16List(size_t length, uint64_t offsetInBytes = 0,
                Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Int32List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 4 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     *  
     * @param length The amount of int32_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 4.
     */
    Int32List
    asInt32List(size_t length, uint64_t offsetInBytes = 0,
                Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Int64List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 8 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     *  
     * @param length The amount of int64_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 8.
     */
    Int64List
    asInt64List(size_t length, uint64_t offsetInBytes = 0,
                Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Uint8List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     * 
     * @param length The amount of uint8_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 1.
     */
    Uint8List
    asUint8List(size_t length, uint64_t offsetInBytes = 0) const noexcept(false);

    /**
     * @brief Creates a @c Uint16List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 2 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     *  
     * @param length The amount of uint16_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 2.
     */
    Uint16List
    asUint16List(size_t length, uint64_t offsetInBytes = 0,
                 Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Uint32List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 4 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     * 
     * @param length The amount of uint32_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 4-
     */
    Uint32List
    asUint32List(size_t length, uint64_t offsetInBytes = 0,
                 Endian endian = Endian::BIG) const noexcept(false);

    /**
     * @brief Creates a @c Uint64List copy of a region of this @c ByteData .
     *        
     *        @c offsetInBytes + @c length * 8 must not be greater than
     *        @c getLengthInBytes() ,else an @c ArgumentError gets thrown. 
     * 
     * @param length The amount of uint64_t the returned list should contain.
     * @param offsetInBytes Start index for reading bytes from this @c ByteData . 
     *                      Must be divisible by 8.
     */
    Uint64List
    asUint64List(size_t length, uint64_t offsetInBytes = 0,
                 Endian endian = Endian::BIG) const noexcept(false);
  };

  using OptionalByteData = boost::optional<ByteData>;

  /** 
  * @see https://api.flutter.dev/flutter/foundation/WriteBuffer-class.html
  */
  class WriteBuffer {
   private:
    static const std::array<uint8_t, 8> m_zeroBuffer;

    std::list<uint8_t> m_buffer;
    bool m_isDone;

   private:
    void
    m_alignTo(uint64_t alignment) noexcept;

   public:
    WriteBuffer() noexcept;

    WriteBuffer(const WriteBuffer&) = delete;

    WriteBuffer(WriteBuffer&&) = delete;

    virtual ~WriteBuffer() noexcept;

    WriteBuffer&
    operator=(const WriteBuffer&) = delete;

    WriteBuffer&
    operator=(WriteBuffer&&) = delete;

    /**
     * @brief Write an Uint8 into the buffer.
     */
    void
    putUint8(uint8_t value);

    /**
     * @brief Write an Uint16 into the buffer.
     */
    void
    putUint16(uint16_t value, Endian endian = Endian::HOST);

    /**
     * @brief Write an Uint32 into the buffer.
     */
    void
    putUint32(uint32_t value, Endian endian = Endian::HOST);

    /**
     * @brief Write an Int32 into the buffer.
     */
    void
    putInt32(int32_t value, Endian endian = Endian::HOST);

    /**
     * @brief Write an Int64 into the buffer.
     */
    void
    putInt64(int64_t value, Endian endian = Endian::HOST);

    /**
     * @brief Write a Float64 into the buffer.
     */
    void
    putFloat64(float64_t value, Endian endian = Endian::HOST);

    /**
     * @brief Write all the values from an Uint8List into the buffer.
     */
    void
    putUint8List(const Uint8List& buffer);

    /**
     * @brief Write all the values from an Int32List into the buffer.
     */
    void
    putInt32List(const Int32List& buffer);

    /**
     * @brief Write all the values from an Int64List into the buffer.
     */
    void
    putInt64List(const Int64List& buffer);

    /**
     * @brief Write all the values from a Float32List into the buffer.
     */
    void
    putFloat32List(const Float32List& buffer);

    /**
     * @brief Write all the values from a Float64List into the buffer.
     */
    void
    putFloat64List(const Float64List& buffer);

    /**
     * @brief Finalize and return the written bytes.
     */
    const ByteData
    done() noexcept(false);
  };

  /**
  * @see https://api.flutter.dev/flutter/foundation/ReadBuffer-class.html
  */
  class ReadBuffer {
   private:
    // The underlying data being read.
    const ByteData::ByteBuffer& m_buffer;

    // The position to read next.
    uint64_t m_position;

   private:
    void
    m_alignTo(uint64_t alignment) noexcept;

   public:
    ReadBuffer(const ByteData& data) noexcept;

    ReadBuffer(const ReadBuffer&) = delete;

    ReadBuffer(ReadBuffer&&) = delete;

    virtual ~ReadBuffer() noexcept;

    ReadBuffer&
    operator=(const ReadBuffer&) = delete;

    ReadBuffer&
    operator=(ReadBuffer&&) = delete;

    /**
     * @brief Whether the buffer has data remaining to read.
     */
    bool
    hasRemaining() const noexcept;

    /**
     * @brief Reads a Uint8 from the buffer.
     */
    uint8_t
    getUint8() noexcept(false);

    /**
     * @brief Reads a Uint16 from the buffer.
     */
    uint16_t
    getUint16(Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads a Uint32 from the buffer.
     */
    uint32_t
    getUint32(Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads a Int32 from the buffer.
     */
    int32_t
    getInt32(Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads a Int64 from the buffer.
     */
    int64_t
    getInt64(Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads a Float64 from the buffer.
     */
    float64_t
    getFloat64(Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads the given number of Uint8s from the buffer.
     */
    Uint8List
    getUint8List(size_t length) noexcept(false);

    /**
     * @brief Reads the given number of Int32s from the buffer.
     */
    Int32List
    getInt32List(size_t length, Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads the given number of Int64s from the buffer.
     */
    Int64List
    getInt64List(size_t length, Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads the given number of Float32s from the buffer.
     */
    Float32List
    getFloat32List(size_t length, Endian endian = Endian::HOST) noexcept(false);

    /**
     * @brief Reads the given number of Float64s from the buffer.
     */
    Float64List
    getFloat64List(size_t length, Endian endian = Endian::HOST) noexcept(false);
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_BUFFER_HPP