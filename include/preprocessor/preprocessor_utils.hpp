#ifndef FLUTTER_IOT_INCLUDE_PREPROCESSOR_PREPROCESSOR_UTILS_HPP
#define FLUTTER_IOT_INCLUDE_PREPROCESSOR_PREPROCESSOR_UTILS_HPP

#include <cstdint>

#include <typeinfo>

#include <boost/any.hpp>

#include "stringification.hpp"



#ifndef FLUTTER_IOT_WORDSIZE

#  ifdef __WORDSIZE
#    define FLUTTER_IOT_WORDSIZE __WORDSIZE
#  elif (defined SIZE_WIDTH)
#    define FLUTTER_IOT_WORDSIZE SIZE_WIDTH
#  else
#    if (SIZE_MAX == 18446744073709551615UL)
#      define FLUTTER_IOT_WORDSIZE 64
#    else
#      define FLUTTER_IOT_WORDSIZE 32
#    endif
#  endif  // __WORDSIZE

#endif  // FLUTTER_IOT_WORDSIZE



#define FLUTTER_IOT_STATIC_CONST(type, assignment) static const type assignment

#define FLUTTER_IOT_STATIC_CONSTEXPR(type, assignment) static constexpr type assignment



/**
* @brief Get the byte at @c index of a @c value of type @c type . 
*/
#define FLUTTER_IOT_BYTE_AT(type, value, index)                                         \
  static_cast<uint8_t>(((static_cast<type>(static_cast<type>(value)                     \
                                           << (8 * ((sizeof(type) - 1) - (index))))) >> \
                        (8 * (sizeof(type) - 1))))



/**
 * @brief Cast @c value to a carray and define a new carray
 *        called @c carray with the value of @c value .
 *        
 *        type carray[size] = value
 */
#define FLUTTER_IOT_AS_CARRAY(type, carray, size, value) \
  type(&carray)[(size)] = FLUTTER_IOT_CAST_TO_CARRAY(type, value, size)

/**
 * @brief Cast @c identifier to the type of a c array with the form:
 *        
 *        type identifier[size]
 */
#define FLUTTER_IOT_CAST_TO_CARRAY(type, identifier, size) ((type(&)[(size)])(identifier))



#define FLUTTER_IOT_CURRENT_POSITION __FILE__ ":" FLUTTER_IOT_TO_STRING(__LINE__)



/**
 * @brief Check if a @c typeindex object is of type @c type .
 */
#define FLUTTER_IOT_IS_TYPE(type, typeindex) (typeid(type) == typeindex)



/******************** Utils for boost::any ********************/

/**
 * @brief Check if a @c boost::any object is of type @c _type .
 */
#define FLUTTER_IOT_ANY_IS_TYPE(_type, any) FLUTTER_IOT_IS_TYPE(_type, any.type())

#define FLUTTER_IOT_ANY_CAST(type, any) boost::any_cast<type>(any)



#endif  // FLUTTER_IOT_INCLUDE_PREPROCESSOR_PREPROCESSOR_UTILS_HPP