#ifndef FLUTTER_IOT_INCLUDE_PREPROCESSOR_ERRORS_HPP
#define FLUTTER_IOT_INCLUDE_PREPROCESSOR_ERRORS_HPP

#include <string>

#include "../utils.hpp"
#include "assert.hpp"
#include "preprocessor_utils.hpp"



#define FLUTTER_IOT_DEFAULT_ERROR_MESSAGE                     \
  (("\nFile: " FLUTTER_IOT_CURRENT_POSITION "\nFunction: ") + \
   std::string(FLUTTER_IOT_FUNCTION) + '\n')

/**
 * @brief This macro stores the message from
 *        @c FLUTTER_IOT_DEFAULT_ERROR_MESSAGE
 *        in a @c std::string variable, so it is safe to call 
 *        @c std::string.c_str() and the likes of it.
 * 
 *        The old message gets overwritten each time
 *        @c FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE is used.
 */
#define FLUTTER_IOT_DEFAULT_ERROR_MESSAGE_SAFE \
  flutter_iot::m_saveDefaultErrorMessage(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE)



#define FLUTTER_IOT_STATE_ERROR() \
  flutter_iot::StateError(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE);
#define FLUTTER_IOT_STATE_ERROR_WHAT(what) \
  flutter_iot::StateError(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, what);



#define FLUTTER_IOT_TYPE_ERROR() \
  flutter_iot::TypeError(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE);
#define FLUTTER_IOT_TYPE_ERROR_WHAT(what) \
  flutter_iot::TypeError(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, what);



#define FLUTTER_IOT_UNIMPLEMENTED_ERROR() \
  flutter_iot::UnimplementedError(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE);
#define FLUTTER_IOT_UNIMPLEMENTED_ERROR_WHAT(what) \
  flutter_iot::UnimplementedError(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, what);



#endif  // FLUTTER_IOT_INCLUDE_PREPROCESSOR_ERRORS_HPP
