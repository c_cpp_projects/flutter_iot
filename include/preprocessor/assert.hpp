#ifndef FLUTTER_IOT_INCLUDE_PREPROCESSOR_ASSERT_HPP
#define FLUTTER_IOT_INCLUDE_PREPROCESSOR_ASSERT_HPP

#include <cassert>
#include <cerrno>
#include <cstdio>
#include <cstring>

#include "ansi_escape.hpp"
#include "build_type.hpp"



/**
 * @brief Get the name of the function currently being defined.
 */
#ifndef FLUTTER_IOT_FUNCTION

#  ifdef ESP32
// If used in https://github.com/espressif/arduino-esp32
#    define FLUTTER_IOT_FUNCTION __ASSERT_FUNC
#  else
/* Default */
#    define FLUTTER_IOT_FUNCTION __ASSERT_FUNCTION
#  endif

#endif  // FLUTTER_IOT_FUNCTION



#ifndef FLUTTER_IOT_ASSERT_FUNC

#  ifdef ESP32
// If used in https://github.com/espressif/arduino-esp32
#    define FLUTTER_IOT_ASSERT_FUNC(expr, file, line, function) \
      __assert_func(file, line, function, #expr);
#  else
/* Default */
#    define FLUTTER_IOT_ASSERT_FUNC(expr, file, line, function) \
      __assert_fail(#expr, file, line, function);
#  endif

#endif  // FLUTTER_IOT_ASSERT_FUNC



/**
 * @brief Assert with a formatted message.
 */
#ifndef FLUTTER_IOT_FASSERT

#  ifdef FLUTTER_IOT_IS_DEBUG_BUILD


#    define FLUTTER_IOT_ASSERT_FAIL_MSG                    \
      FLUTTER_IOT_ANSI_FG_FONT(202, FLUTTER_IOT_ANSI_BOLD) \
      "[ASSERTION "                                        \
      "FAILED]" FLUTTER_IOT_ANSI_DEFAULT

#    define FLUTTER_IOT_ERRNO_MSG \
      FLUTTER_IOT_ANSI_FG(196)    \
      "errno:" FLUTTER_IOT_ANSI_DEFAULT

#    define FLUTTER_IOT_FAINT_MSG(msg) \
      FLUTTER_IOT_ANSI_FG_FONT(244, FLUTTER_IOT_ANSI_FAINT) msg FLUTTER_IOT_ANSI_DEFAULT

#    define FLUTTER_IOT_BOLD_MSG(msg) \
      FLUTTER_IOT_ANSI_FONT(FLUTTER_IOT_ANSI_BOLD) msg FLUTTER_IOT_ANSI_DEFAULT



#    define FLUTTER_IOT_FASSERT(expr, format, ...)                                     \
      if (!(expr)) {                                                                   \
        if ((0 == strcmp("FLUTTER_IOT_ASSERT%u", format)) ||                           \
            (0 == strcmp("", format))) {                                               \
          fprintf(stderr,                                                              \
                  FLUTTER_IOT_ASSERT_FAIL_MSG "\n" FLUTTER_IOT_ERRNO_MSG " %s"         \
                                              "\n" FLUTTER_IOT_BOLD_MSG("File: ")      \
                                                FLUTTER_IOT_FAINT_MSG("%s") "\n",      \
                  ((0 == errno) ? FLUTTER_IOT_FAINT_MSG("None") : strerror(errno)),    \
                  FLUTTER_IOT_CURRENT_POSITION);                                       \
        } else {                                                                       \
          fprintf(stderr,                                                              \
                  FLUTTER_IOT_ASSERT_FAIL_MSG                                          \
                  "\n" FLUTTER_IOT_ERRNO_MSG " %s"                                     \
                  "\n" FLUTTER_IOT_BOLD_MSG("File: ")                                  \
                    FLUTTER_IOT_FAINT_MSG("%s") "\n" FLUTTER_IOT_BOLD_MSG("Message: ") \
                      format "\n",                                                     \
                  ((0 == errno) ? FLUTTER_IOT_FAINT_MSG("None") : strerror(errno)),    \
                  FLUTTER_IOT_CURRENT_POSITION,                                        \
                  ##__VA_ARGS__);                                                      \
        }                                                                              \
                                                                                       \
        FLUTTER_IOT_ASSERT_FUNC(expr, __FILE__, __LINE__, FLUTTER_IOT_FUNCTION);       \
      }

#  else  // FLUTTER_IOT_IS_DEBUG_BUILD

#    define FLUTTER_IOT_FASSERT(expr, format, ...)

#  endif  // FLUTTER_IOT_IS_DEBUG_BUILD

#endif  // FLUTTER_IOT_FASSERT



/**
 * @brief Assert without a formatted message.
 */
#define FLUTTER_IOT_ASSERT(expr) FLUTTER_IOT_FASSERT(expr, "FLUTTER_IOT_ASSERT%u", 0)



#endif  // FLUTTER_IOT_INCLUDE_PREPROCESSOR_ASSERT_HPP