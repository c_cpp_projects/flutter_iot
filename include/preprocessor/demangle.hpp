#ifndef FLUTTER_IOT_INCLUDE_PREPROCESSOR_DEMANGLE_HPP
#define FLUTTER_IOT_INCLUDE_PREPROCESSOR_DEMANGLE_HPP

#include <typeinfo>

#include <boost/core/demangle.hpp>



/**
 * @brief Demangle the name of a std::type_info object
 * 
 * @see https://en.cppreference.com/w/cpp/types/type_info/name
 */
#define FLUTTER_IOT_DEMANGLE_TYPE_NAME(name) boost::core::demangle(name)

/**
 * @brief Demangle the type name of a type T.
 */
#define FLUTTER_IOT_DEMANGLE_TYPE(type) \
  FLUTTER_IOT_DEMANGLE_TYPE_NAME(typeid(type).name())

/**
 * @brief Demangle the type name of a @c boost::any
 */
#define FLUTTER_IOT_DEMANGLE_ANY(any) FLUTTER_IOT_DEMANGLE_TYPE_NAME(any.type().name())



#endif  // FLUTTER_IOT_INCLUDE_PREPROCESSOR_DEMANGLE_HPP