#ifndef FLUTTER_IOT_INCLUDE_PREPROCESSOR_ANSI_ESCAPE_HPP
#define FLUTTER_IOT_INCLUDE_PREPROCESSOR_ANSI_ESCAPE_HPP

#include "stringification.hpp"



/******************** ANSI Escape Codes ********************/
// See https://en.wikipedia.org/wiki/ANSI_escape_code#Colors



#ifndef FLUTTER_IOT_ANSI_ENABLE
#  define FLUTTER_IOT_ANSI_ENABLE 0
#endif  // FLUTTER_IOT_ANSI_ENABLE



#if (0 != FLUTTER_IOT_ANSI_ENABLE)

#  define FLUTTER_IOT_ANSI_ESC     "\x1B["
#  define FLUTTER_IOT_ANSI_DEFAULT FLUTTER_IOT_ANSI_ESC "0m"


#  define FLUTTER_IOT_ANSI_BOLD      "1;"
#  define FLUTTER_IOT_ANSI_FAINT     "2;"
#  define FLUTTER_IOT_ANSI_ITALIC    "3;"
#  define FLUTTER_IOT_ANSI_UNDERLINE "4;"


/**
 * @brief Set the font type.
 * 
 *        You can set the @c font by combining @b FLUTTER_IOT_ANSI_BOLD ,
 *        @b FLUTTER_IOT_ANSI_ITALIC and @b FLUTTER_IOT_ANSI_UNDERLINE .
 * 
 * @param font - Determines the font type.
 */
#  define FLUTTER_IOT_ANSI_FONT(font) FLUTTER_IOT_ANSI_ESC font "37m"


/**
 * @brief Set the foreground color with a specific font type.
 * 
 *        You can set the @c font by combining @b FLUTTER_IOT_ANSI_BOLD ,
 *        @b FLUTTER_IOT_ANSI_ITALIC and @b FLUTTER_IOT_ANSI_UNDERLINE .
 * 
 * @param color - uint8_t that determines the color.
 * @param font - Determines the font type.
 */
#  define FLUTTER_IOT_ANSI_FG_FONT(color, font) \
    FLUTTER_IOT_ANSI_ESC font "38;5;" FLUTTER_IOT_STRINGIFY(color) "m"

/**
 * @brief Set the foreground color.
 * 
 * @param color - uint8_t that determines the color.
 */
#  define FLUTTER_IOT_ANSI_FG(color) FLUTTER_IOT_ANSI_FG_FONT(color, "")


/**
 * @brief Set the background color with a specific font type.
 * 
 *        You can set the @c font by combining @b FLUTTER_IOT_ANSI_BOLD ,
 *        @b FLUTTER_IOT_ANSI_ITALIC and @b FLUTTER_IOT_ANSI_UNDERLINE .
 * 
 * @param color - uint8_t that determines the color.
 * @param font - Determines the font type.
 */
#  define FLUTTER_IOT_ANSI_BG_FONT(color, font) \
    FLUTTER_IOT_ANSI_ESC font "48;5;" FLUTTER_IOT_STRINGIFY(color) "m"


/**
 * @brief Set the background color.
 * 
 * @param color - uint8_t that determines the color.
 */
#  define FLUTTER_IOT_ANSI_BG(color) FLUTTER_IOT_ANSI_BG_FONT(color, "")

#else  // FLUTTER_IOT_ANSI_ENABLE

#  define FLUTTER_IOT_ANSI_ESC
#  define FLUTTER_IOT_ANSI_DEFAULT
#  define FLUTTER_IOT_ANSI_BOLD
#  define FLUTTER_IOT_ANSI_FAINT
#  define FLUTTER_IOT_ANSI_ITALIC
#  define FLUTTER_IOT_ANSI_UNDERLINE
#  define FLUTTER_IOT_ANSI_FONT(font)
#  define FLUTTER_IOT_ANSI_FG_FONT(color, font)
#  define FLUTTER_IOT_ANSI_FG(color)
#  define FLUTTER_IOT_ANSI_BG_FONT(color, font)
#  define FLUTTER_IOT_ANSI_BG(color)

#endif  // FLUTTER_IOT_ANSI_ENABLE



#endif  // FLUTTER_IOT_INCLUDE_PREPROCESSOR_ANSI_ESCAPE_HPP