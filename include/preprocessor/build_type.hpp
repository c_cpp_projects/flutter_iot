#ifndef FLUTTER_IOT_INCLUDE_PREPROCESSOR_BUILD_TYPE_HPP
#define FLUTTER_IOT_INCLUDE_PREPROCESSOR_BUILD_TYPE_HPP



/**
 * @brief Determine the build type of the project.
 */



// Throw an error if both symbols were defined.
#if (defined FLUTTER_IOT_IS_RELEASE_BUILD && defined FLUTTER_IOT_IS_DEBUG_BUILD)
#  error "Do not define FLUTTER_IOT_IS_RELEASE_BUILD and FLUTTER_IOT_IS_DEBUG_BUILD"
#endif  // (defined FLUTTER_IOT_IS_RELEASE_BUILD && defined FLUTTER_IOT_IS_DEBUG_BUILD)

// If FLUTTER_IOT_IS_RELEASE_BUILD is not defined then FLUTTER_IOT_IS_DEBUG_BUILD
// may be defined or not, in any case default to FLUTTER_IOT_IS_DEBUG_BUILD
// if FLUTTER_IOT_IS_RELEASE_BUILD was not defined.
#ifndef FLUTTER_IOT_IS_RELEASE_BUILD

#  ifndef FLUTTER_IOT_IS_DEBUG_BUILD
#    define FLUTTER_IOT_IS_DEBUG_BUILD
#  endif  // FLUTTER_IOT_IS_DEBUG_BUILD

#endif  // FLUTTER_IOT_IS_RELEASE_BUILD



#endif  // FLUTTER_IOT_INCLUDE_PREPROCESSOR_BUILD_TYPE_HPP