#ifndef FLUTTER_IOT_INCLUDE_MESSAGE_CODEC_HPP
#define FLUTTER_IOT_INCLUDE_MESSAGE_CODEC_HPP

#include <cstdint>

#include <map>
#include <string>
#include <type_traits>
#include <vector>

#include <boost/any.hpp>
#include <boost/optional.hpp>
#include <boost/range.hpp>


#include "buffer.hpp"
#include "exceptions/format_exception.hpp"
#include "preprocessor/demangle.hpp"
#include "preprocessor/preprocessor_utils.hpp"
#include "types/types_utils.hpp"

namespace flutter_iot {

  /**
   * @brief A message encoding/decoding mechanism. 
   *        Both operations throw an exception, if conversion fails. Such
   *        situations should be treated as programming errors.
   * 
   * @see https://api.flutter.dev/flutter/services/MessageCodec-class.html 
   */
  template<typename DecodedMessage>
  class MessageCodec {
   public:
    using DecodedMessageType = DecodedMessage;

    /**
     * @brief Encodes the specified @c message in binary.
     * 
     * @return @b boost::none , if @c message @b boost::none ,
     *         otherwise the encoded @c message.
     */
    virtual boost::optional<ByteData>
    encodeMessage(const DecodedMessage& message) = 0;

    /**
     * @brief Decodes the specified @c message from binary.
     * 
     * @return @b boost::none , if @c message is @b boost::none ,
     *         otherwise the decoded @c message.
     */
    virtual boost::optional<DecodedMessage>
    decodeMessage(const boost::optional<ByteData>& message) = 0;
  };

  /**
   * @brief Supported messages are acyclic values of these forms:
   *        @b null
   *        @b bools
   *        @b nums
   *        @b Strings
   *        @b Uint8Buffers, @b Int32Buffers, @b Int64Buffers, @b Float64Buffers
   *        @b Lists of supported values
   *        @b Maps from supported values to supported values
   *        
   *        @b null is represented as an object of type @c boost::optional with
   *        no value.
   * 
   * @see https://api.flutter.dev/flutter/services/StandardMessageCodec-class.html
   */
  class StandardMessageCodec : public MessageCodec<AnyOrNone> {
    // TODO(obemu): utf8-decode/encode LABEL_STRING
    // https://stackoverflow.com/questions/16208079/how-to-work-with-utf-8-in-c-conversion-from-other-encodings-to-utf-8
    // https://stackoverflow.com/questions/50403342/how-do-i-properly-use-stdstring-on-utf-8-in-c

   public:
    class ValueTypeLabel final {
     public:
      // The codec serializes messages as outlined below. This format must match the
      // Android and iOS counterparts and cannot change (as it's possible for
      // someone to end up using this for persistent storage).
      //
      // * A single byte with one of the constant values below determines the
      //   type of the value.
      //
      // * The serialization of the value itself follows the type byte.
      //
      // * Numbers are represented using the host endianness throughout.
      //
      // * Lengths and sizes of serialized parts are encoded using an expanding
      //   format optimized for the common case of small non-negative integers:
      //   * values 0..253 inclusive using one byte with that value;
      //   * values 254..2^16 inclusive using three bytes, the first of which is
      //     254, the next two the usual unsigned representation of the value;
      //   * values 2^16+1..2^32 inclusive using five bytes, the first of which is
      //     255, the next four the usual unsigned representation of the value.
      //
      // * null, true, and false have empty serialization; they are encoded directly
      //   in the type byte (using _valueNull, _valueTrue, _valueFalse)
      //
      // * Integers representable in 32 bits are encoded using 4 bytes two's
      //   complement representation.
      //
      // * Larger integers are encoded using 8 bytes two's complement
      //   representation.
      //
      // * doubles are encoded using the IEEE 754 64-bit double-precision binary
      //   format. Zero bytes are added before the encoded double value to align it
      //   to a 64 bit boundary in the full message.
      //
      // * Strings are encoded using their UTF-8 representation. First the length
      //   of that in bytes is encoded using the expanding format, then follows the
      //   UTF-8 encoding itself.
      //
      // * Uint8Lists, Int32Lists, Int64Lists, Float32Lists, and Float64Lists are
      //   encoded by first encoding the list's element count in the expanding
      //   format, then the smallest number of zero bytes needed to align the
      //   position in the full message with a multiple of the number of bytes per
      //   element, then the encoding of the list elements themselves, end-to-end
      //   with no additional type information, using two's complement or IEEE 754
      //   as applicable.
      //
      // * Lists are encoded by first encoding their length in the expanding format,
      //   then follows the recursive encoding of each element value, including the
      //   type byte (Lists are assumed to be heterogeneous).
      //
      // * Maps are encoded by first encoding their length in the expanding format,
      //   then follows the recursive encoding of each key/value pair, including the
      //   type byte for both (Maps are assumed to be heterogeneous).
      //
      // The type labels below must not change, since it's possible for this interface
      // to be used for persistent storage.
      enum Type : uint8_t {
        LABEL_NULL = 0,           // AnyOrNone with no value
        LABEL_TRUE = 1,           // AnyOrNone with bool true
        LABEL_FALSE = 2,          // AnyOrNone with bool false
        LABEL_INT32 = 3,          // AnyOrNone with int32_t
        LABEL_INT64 = 4,          // AnyOrNone with int64_t
        LABEL_LARGE_INT = 5,      // not used
        LABEL_FLOAT64 = 6,        // AnyOrNone with float64_t
        LABEL_STRING = 7,         // AnyOrNone with std::string
        LABEL_UINT8_LIST = 8,     // AnyOrNone with Uint8List
        LABEL_INT32_LIST = 9,     // AnyOrNone with Int32List
        LABEL_INT64_LIST = 10,    // AnyOrNone with Int64List
        LABEL_FLOAT64_LIST = 11,  // AnyOrNone with Float64List
        LABEL_LIST = 12,          // AnyOrNone with ValueTypeLabel::List
        LABEL_MAP = 13,           // AnyOrNone with ValueTypeLabel::Map
        LABEL_FLOAT32_LIST = 14,  // AnyOrNone with Float32List
      };

      /**
     * @brief Assumes that @c a and @c b have a value with a type
     *        from @c Type .
     *
     * @see https://en.cppreference.com/w/cpp/named_req/Compare
     */
      struct Equivalent {
        constexpr Equivalent() = default;
        constexpr Equivalent(const Equivalent&) = default;

        /**
       * @brief Check if @c a and @c b hold the same value.
       * 
       * @return @b True if @c a and @c b are a reference to the same
       *         object and @b true if @c a and @c b hold the same value,
       *         otherwise @b false .
       */
        inline bool
        operator()(const AnyOrNone& a, const AnyOrNone& b) const noexcept(false) {
          return Equivalent::equal(a, b);
        }

        /**
       * @brief Check if @c a and @c b hold the same value.
       * 
       * @return @b True if @c a and @c b are a reference to the same
       *         object and @b true if @c a and @c b hold the same value,
       *         otherwise @b false .
       */
        static bool
        equal(const AnyOrNone& a, const AnyOrNone& b) noexcept(false);
      };

      /**
     * @brief Assumes that @c any has a value with a type from @c Type .
     *
     * @see https://en.cppreference.com/w/cpp/utility/hash
     */
      struct Hashing {
        constexpr Hashing() = default;
        constexpr Hashing(const Hashing&) = default;

        inline uint64_t
        operator()(const AnyOrNone& any) const noexcept(false) {
          return hash(any);
        }

        /**
         * @brief 
         * 
         * @return Hashcode of @c any
         * 
         * @see Inspired by https://www.sitepoint.com/how-to-implement-javas-hashcode-correctly/ 
         */
        static uint64_t
        hash(const AnyOrNone& any) noexcept(false);
      };

      struct Comparator {
        constexpr Comparator() = default;
        constexpr Comparator(const Comparator&) = default;

        /**
         * @brief Compare @c lhs to @c rhs .
         * 
         * @return @b True if @c lhs is greater than @c rhs , else @b false . 
         */
        inline bool
        operator()(const AnyOrNone& lhs, const AnyOrNone& rhs) const noexcept(false) {
          return compare(lhs, rhs);
        }

        /**
         * @brief Compare @c lhs to @c rhs .
         * 
         * @return @b True if @c lhs is greater than @c rhs , else @b false . 
         */
        static bool
        compare(const AnyOrNone& lhs, const AnyOrNone& rhs) noexcept(false);
      };

      using List = flutter_iot::List<AnyOrNone>;
      using Map = flutter_iot::Map<Equivalent>;

     private:
      Type m_value;

     public:
      ValueTypeLabel() = default;

      constexpr ValueTypeLabel(Type type) noexcept;

      ValueTypeLabel(uint8_t type) noexcept(false);

      ValueTypeLabel(const ValueTypeLabel& source) noexcept;

      ValueTypeLabel&
      operator=(const ValueTypeLabel& source) noexcept;

      ValueTypeLabel&
      operator=(Type source) noexcept;

      ValueTypeLabel&
      operator=(uint8_t source) noexcept(false);

      friend constexpr bool
      operator==(const ValueTypeLabel& a, const ValueTypeLabel& b) noexcept;

      friend constexpr bool
      operator!=(const ValueTypeLabel& a, const ValueTypeLabel& b) noexcept;

      friend constexpr bool
      operator==(const ValueTypeLabel& a, const Type b) noexcept;

      friend constexpr bool
      operator!=(const ValueTypeLabel& a, const Type b) noexcept;

      friend constexpr bool
      operator==(const Type a, const ValueTypeLabel& b) noexcept;

      friend constexpr bool
      operator!=(const Type a, const ValueTypeLabel& b) noexcept;

      // Allow switch and comparisons.
      constexpr operator Type() const noexcept;

      // Prevent usage in if-statements.
      explicit operator bool() = delete;

      explicit operator uint8_t() const noexcept {
        return static_cast<uint8_t>(m_value);
      }

      /**
       * @brief Get the string representation of @c any , assuming @c any
       *        has a type from @c Type , if it does not then an exception
       *        is thrown.
       */
      static std::string
      toString(const AnyOrNone& any) noexcept(false);

      static std::string
      toString(const List& list) noexcept(false);

      static std::string
      toString(const Map& map) noexcept(false);
    };

   public:
    StandardMessageCodec() noexcept;

    virtual ~StandardMessageCodec() noexcept;

    /**
     * @brief Encodes the specified @c message in binary.
     * 
     * @return @b boost::none , if @c message @b boost::none ,
     *         otherwise the encoded @c message.
     */
    boost::optional<ByteData>
    encodeMessage(const AnyOrNone& message) override;

    /**
     * @brief Decodes the specified @c message from binary.
     * 
     * @return @b boost::none , if @c message is @b boost::none ,
     *         otherwise the decoded @c message.
     */
    boost::optional<AnyOrNone>
    decodeMessage(const boost::optional<ByteData>& message) override;

    /**
    * Writes @c value to @c buffer by first writing a type discriminator
    * byte, then the @c value itself.
    *
    * This method may be called recursively to serialize container values.
    *
    * Type discriminators 0 through 127 inclusive are reserved for use by the
    * base class, as follows:
    *
    * @b null = 0
    * @b true = 1
    * @b false = 2
    * @b 32_bit_integer = 3
    * @b 64_bit_integer = 4
    * @b larger_integers = 5 (see below)
    * @b 64_bit_floating-point_number = 6
    * @b String = 7
    * @b Uint8List = 8
    * @b Int32List = 9
    * @b Int64List = 10
    * @b Float64List = 11
    * @b List = 12
    * @b Map = 13
    * @b Float32List = 14
    * @b Reserved for future expansion: 15..127
    *
    * The codec can be extended by overriding this method, calling super
    * for values that the extension does not handle. Type discriminators
    * used by extensions must be greater than or equal to 128 in order to avoid
    * clashes with any later extensions to the base class.
    *
    * The "larger integers" type, 5, is never used by [writeValue]. A subclass
    * could represent big integers from another package using that type. The
    * format is first the type byte (0x05), then the actual number as an ASCII
    * string giving the hexadecimal representation of the integer, with the
    * string's length as encoded by [writeSize] followed by the string bytes. On
    * Android, that would get converted to a `java.math.BigInteger` object. On
    * iOS, the string representation is returned.
    */
    void
    writeValue(WriteBuffer& buffer, const AnyOrNone& value) const noexcept(false);

    /**
     * @brief Writes a non-negative 32-bit integer @c value to @c buffer ,
     *        using an expanding 1-5 byte encoding that optimizes for small values.
     *        This method is intended for use by subclasses overriding @c writeValue .
     */
    void
    writeSize(WriteBuffer& buffer, uint32_t value) const noexcept;

    /**
     * @brief Reads a value from @c buffer as written by @c writeValue .
     *        This method is intended for use by subclasses overriding
     *        @c readValueOfType .
     */
    virtual AnyOrNone
    readValue(ReadBuffer& buffer) const noexcept(false);

    /**
     * @brief Reads a value of the indicated @c type from @c buffer .
     *        The codec can be extended by overriding this method, calling super
     *        for  types that the extension does not handle.
     *        See the discussion at @c writeValue.
     * 
     * @param buffer 
     * @param type 
     * @return StandardDecodedMessageType 
     */
    AnyOrNone
    readValueOfType(ReadBuffer& buffer, ValueTypeLabel type) const noexcept(false);

    /**
     * @brief Reads a non-negative int from @c buffer as written by @c writeSize .
     *        This method is intended for use by subclasses overriding
     *        @c readValueOfType .
     */
    uint32_t
    readSize(ReadBuffer& buffer) const noexcept;
  };

}  // namespace flutter_iot

#endif  // FLUTTER_IOT_INCLUDE_MESSAGE_CODEC_HPP