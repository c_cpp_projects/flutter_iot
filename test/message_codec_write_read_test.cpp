#include "tests.inc"

int main() {
  using namespace flutter_iot;

  StandardMessageCodec codec;
  WriteBuffer wb;

  AnyOrNone method(std::string("myCppMethod"));
  AnyOrNone args(StandardMessageCodec::ValueTypeLabel::Map({
    {AnyOrNone(std::string("id")), AnyOrNone((int32_t) 99)},
    {AnyOrNone(std::string("name")), AnyOrNone(std::string("Hans Peter Friedrich"))},
    {AnyOrNone(std::string("grade")), AnyOrNone((float64_t) 2.145)},
  }));

  codec.writeValue(wb, method);
  codec.writeValue(wb, args);
  auto buffer = wb.done();

  ReadBuffer rb(buffer);
  auto readMethod = codec.readValue(rb);
  auto readArgs = codec.readValue(rb);

  StandardMessageCodec::ValueTypeLabel::Equivalent equal;
  assert(equal(method, readMethod));
  assert(equal(args, readArgs));

  return 0;
}
