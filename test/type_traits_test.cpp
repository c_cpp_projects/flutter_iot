#include "tests.inc"

#include <list>
#include <queue>


void
testIsContainer();
void
testIsReversibleContainer();
void
testIsAllocatorAwareContainer();

int
main() {
  using namespace flutter_iot;

  testIsContainer();
  testIsReversibleContainer();
  testIsAllocatorAwareContainer();

  return 0;
}

void
testIsContainer() {
  using namespace flutter_iot;

#define TEST_TYPE(type)                                              \
  {                                                                  \
    TEST_EXPECT(has_typedef_value_type<type>::value, true);          \
    TEST_EXPECT(has_typedef_reference<type>::value, true);           \
    TEST_EXPECT(has_typedef_const_reference<type>::value, true);     \
    TEST_EXPECT(has_typedef_iterator<type>::value, true);            \
    TEST_EXPECT(has_typedef_const_iterator<type>::value, true);      \
    TEST_EXPECT(has_typedef_difference_type<type>::value, true);     \
    TEST_EXPECT(has_typedef_size_type<type>::value, true);           \
                                                                     \
    TEST_EXPECT(boost::is_default_constructible<type>::value, true); \
    TEST_EXPECT(boost::is_copy_constructible<type>::value, true);    \
    TEST_EXPECT(std::is_move_constructible<type>::value, true);      \
    TEST_EXPECT(boost::is_copy_assignable<type>::value, true);       \
    TEST_EXPECT(std::is_move_assignable<type>::value, true);         \
    TEST_EXPECT(boost::is_destructible<type>::value, true);          \
    TEST_EXPECT(has_function_begin<type>::value, true);              \
    TEST_EXPECT(has_function_end<type>::value, true);                \
    TEST_EXPECT(has_function_cbegin<type>::value, true);             \
    TEST_EXPECT(has_function_cend<type>::value, true);               \
    TEST_EXPECT(boost::has_equal_to<type>::value, true);             \
    TEST_EXPECT(boost::has_not_equal_to<type>::value, true);         \
    TEST_EXPECT(has_function_swap<type>::value, true);               \
    TEST_EXPECT(has_function_size<type>::value, true);               \
    TEST_EXPECT(has_function_max_size<type>::value, true);           \
    TEST_EXPECT(has_function_empty<type>::value, true);              \
                                                                     \
    TEST_EXPECT(IsContainer<type>::value, true);                     \
  }


  TEST_TYPE(std::vector<int>);
  TEST_TYPE(std::list<std::string>);
  TEST_TYPE(std::list<std::vector<int>>);

  // std::queue satisfies the requirements of SequenceContainer.
  TEST_EXPECT(IsContainer<std::queue<int>>::value, false);

#undef TEST_TYPE
}

void
testIsReversibleContainer() {
  using namespace flutter_iot;

#define TEST_TYPE(type)                                                 \
  {                                                                     \
    TEST_EXPECT(IsContainer<type>::value, true);                        \
                                                                        \
    TEST_EXPECT(has_typedef_reverse_iterator<type>::value, true);       \
    TEST_EXPECT(has_typedef_const_reverse_iterator<type>::value, true); \
    TEST_EXPECT(has_function_rbegin<type>::value, true);                \
    TEST_EXPECT(has_function_rend<type>::value, true);                  \
    TEST_EXPECT(has_function_crbegin<type>::value, true);               \
    TEST_EXPECT(has_function_crend<type>::value, true);                 \
                                                                        \
    TEST_EXPECT(IsReversibleContainer<type>::value, true);              \
  }

  TEST_TYPE(std::vector<int>);
  TEST_TYPE(std::list<std::string>);
  TEST_TYPE(std::list<std::vector<int>>);

#undef TEST_TYPE
}

void
testIsAllocatorAwareContainer() {
  using namespace flutter_iot;

#define TEST_TYPE(type)                                         \
  {                                                             \
    TEST_EXPECT(IsContainer<type>::value, true);                \
                                                                \
    TEST_EXPECT(has_typedef_allocator_type<type>::value, true); \
    TEST_EXPECT(has_function_get_allocator<type>::value, true); \
                                                                \
    TEST_EXPECT(IsAllocatorAwareContainer<type>::value, true);  \
  }


  TEST_TYPE(std::vector<int>);
  TEST_TYPE(std::list<std::string>);
  TEST_TYPE(std::list<std::vector<int>>);

#undef TEST_TYPE
}
