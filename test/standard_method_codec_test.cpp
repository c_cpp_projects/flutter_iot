#include "tests.inc"

#define MAKE_PAIR(index, first, second) \
  auto p##index = std::make_pair(AnyOrNone(first), AnyOrNone(second));

bool
testEncodeCall();
bool
testEncodeAndDecodeCall();
bool
testEncodeSuccessEnvelope();
bool
testEncodeErrorEnvelope();
bool
testEncodeSuccessEnvelopeAndDecode();
bool
testEncodeErrorEnvelopeAndDecode();


int
main() {
  TEST_FOR_N(100, []() {
    TEST_FUNCTION(testEncodeCall);
    TEST_FUNCTION(testEncodeAndDecodeCall);
    TEST_FUNCTION(testEncodeSuccessEnvelope);
    TEST_FUNCTION(testEncodeErrorEnvelope);
    TEST_FUNCTION(testEncodeSuccessEnvelopeAndDecode);
    TEST_FUNCTION(testEncodeErrorEnvelopeAndDecode);

    return true;
  });

  return 0;
}



bool
testEncodeCall() {
  using namespace flutter_iot;

  StandardMethodCodec::MethodCallType call("myMethod",
                                           StandardMethodCodec::ArgsContainerType());
  StandardMethodCodec codec;

  auto data = codec.encodeMethodCall(call);

  return true;
}

bool
testEncodeAndDecodeCall() {
  using namespace flutter_iot;

  MAKE_PAIR(0, getRandom<std::string>() + '0', getRandom<int32_t>());
  MAKE_PAIR(1, getRandom<std::string>() + '1', getRandom<std::string>());
  MAKE_PAIR(2, getRandom<std::string>() + '2', getRandom<float64_t>());
  MAKE_PAIR(3, getRandom<std::string>() + '3', true);
  MAKE_PAIR(4, (float64_t) 2.199551145, /* null */);
  MAKE_PAIR(5, getRandom<std::string>() + '5', getRandomContainer<Uint8List>(50));
  MAKE_PAIR(6, getRandom<std::string>() + '6', getRandomContainer<Float64List>(40));
  MAKE_PAIR(7,
            getRandom<std::string>() + '7',
            StandardMessageCodec::ValueTypeLabel::List({
              AnyOrNone(getRandom<std::string>()),
              AnyOrNone(getRandom<int64_t>()),
              AnyOrNone(getRandom<float64_t>()),
              AnyOrNone(false),
              AnyOrNone(),
            }));
  MAKE_PAIR(8,
            getRandom<std::string>() + '8',
            StandardMessageCodec::ValueTypeLabel::Map({
              {AnyOrNone(getRandom<std::string>()), AnyOrNone(getRandom<std::string>())},
              {AnyOrNone(getRandom<float64_t>()), AnyOrNone(false)},
              {AnyOrNone(true), AnyOrNone(getRandom<int32_t>())},
            }));

  StandardMethodCodec::MethodCallType call(
    "myMethod",
    StandardMethodCodec::ArgsContainerType({p0, p1, p2, p3, p4, p5, p6, p7, p8}));
  StandardMethodCodec codec;

  auto data = codec.encodeMethodCall(call);
  auto decodedCall = codec.decodeMethodCall(data);

  FLUTTER_IOT_ASSERT(decodedCall.getMethod() == call.getMethod());
  FLUTTER_IOT_ASSERT(FLUTTER_IOT_IS_TYPE(
    call.getArguments(), typeid(const StandardMethodCodec::ArgsContainerType&)));
  FLUTTER_IOT_ASSERT(FLUTTER_IOT_IS_TYPE(
    decodedCall.getArguments(), typeid(const StandardMethodCodec::ArgsContainerType&)));

#define ASSERT_VALUE(pair)                                                    \
  FLUTTER_IOT_ASSERT(StandardMessageCodec::ValueTypeLabel::Equivalent::equal( \
    decodedCall.getArguments().at(pair.first), pair.second));

  ASSERT_VALUE(p0);
  ASSERT_VALUE(p1);
  ASSERT_VALUE(p2);
  ASSERT_VALUE(p3);
  ASSERT_VALUE(p4);
  ASSERT_VALUE(p5);
  ASSERT_VALUE(p6);
  ASSERT_VALUE(p7);
  ASSERT_VALUE(p8);

#undef ASSERT_VALUE

  return true;
}

bool
testEncodeSuccessEnvelope() {
  using namespace flutter_iot;

  StandardMethodCodec codec;
  auto envelope = codec.encodeSuccessEnvelope(AnyOrNone(getRandom<std::string>()));

  return true;
}

bool
testEncodeErrorEnvelope() {
  using namespace flutter_iot;

  StandardMethodCodec codec;
  auto envelope1 = codec.encodeErrorEnvelope("errorCode1");
  auto envelope2 = codec.encodeErrorEnvelope("errorCode2", ErrorDetails("myMessage2"));
  auto envelope3 = codec.encodeErrorEnvelope(
    "errorCode3", ErrorDetails("myMessage3"), ErrorDetails("the details3"));

  return true;
}

bool
testEncodeSuccessEnvelopeAndDecode() {
  using namespace flutter_iot;

  StandardMethodCodec codec;

#define CHECK_ENVELOPE(value)                                                 \
  {                                                                           \
    auto any = AnyOrNone(value);                                              \
    auto envelope = codec.encodeSuccessEnvelope(any);                         \
    auto decoded = codec.decodeEnvelope(envelope);                            \
    FLUTTER_IOT_ASSERT(                                                       \
      StandardMessageCodec::ValueTypeLabel::Equivalent::equal(any, decoded)); \
  }

  CHECK_ENVELOPE(getRandom<std::string>());
  CHECK_ENVELOPE(/* null */);
  CHECK_ENVELOPE(true);
  CHECK_ENVELOPE(getRandom<int32_t>());
  CHECK_ENVELOPE(getRandom<int64_t>());
  CHECK_ENVELOPE(getRandom<float64_t>());
  CHECK_ENVELOPE(getRandomContainer<Uint8List>(20));
  CHECK_ENVELOPE(getRandomContainer<Int32List>(20));
  CHECK_ENVELOPE(getRandomContainer<Int64List>(20));
  CHECK_ENVELOPE(getRandomContainer<Float32List>(20));
  CHECK_ENVELOPE(getRandomContainer<Float64List>(20));
  CHECK_ENVELOPE(StandardMessageCodec::ValueTypeLabel::List({
    AnyOrNone(getRandom<std::string>()),
    AnyOrNone(getRandom<int32_t>()),
    AnyOrNone(getRandom<float64_t>()),
    AnyOrNone(false),
    AnyOrNone(),
  }));
  CHECK_ENVELOPE(StandardMessageCodec::ValueTypeLabel::Map({
    {AnyOrNone(getRandom<std::string>()), AnyOrNone(getRandom<std::string>())},
    {AnyOrNone(getRandom<float64_t>()), AnyOrNone(false)},
    {AnyOrNone(true), AnyOrNone(getRandom<int32_t>())},
  }));

#undef CHECK_ENVELOPE

  return true;
}

bool
testEncodeErrorEnvelopeAndDecode() {
  using namespace flutter_iot;

  StandardMethodCodec codec;

#define EXPECT_PLATFORM_EXCEPTION(envelope)                                             \
  try {                                                                                 \
    auto decoded1 = codec.decodeEnvelope(envelope1);                                    \
  } catch (const PlatformException& e) { /* OK */                                       \
  } catch (const std::exception& e) {                                                   \
    throw StateError(std::string("Expected 'PlatformException' but got '") + e.what() + \
                     "'.");                                                             \
  }

  auto envelope1 = codec.encodeErrorEnvelope(getRandom<std::string>());
  auto envelope2 =
    codec.encodeErrorEnvelope(getRandom<std::string>(), getRandom<std::string>());
  auto envelope3 = codec.encodeErrorEnvelope(
    getRandom<std::string>(), getRandom<std::string>(), getRandom<std::string>());


  EXPECT_PLATFORM_EXCEPTION(envelope1);
  EXPECT_PLATFORM_EXCEPTION(envelope2);
  EXPECT_PLATFORM_EXCEPTION(envelope3);


#undef EXPECT_PLATFORM_EXCEPTION

  return true;
}
