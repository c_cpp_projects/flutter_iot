#include "tests.inc"



bool
testWriteReadUint8();
bool
testWriteReadUint16(flutter_iot::Endian endian);
bool
testWriteReadUint32(flutter_iot::Endian endian);
bool
testWriteReadInt32(flutter_iot::Endian endian);
bool
testWriteReadInt64(flutter_iot::Endian endian);
bool
testWriteReadFloat64(flutter_iot::Endian endian);
bool
testWriteReadUint8List();
bool
testWriteReadInt32List();
bool
testWriteReadInt64List();
bool
testWriteReadFloat32List();
bool
testWriteReadFloat64List();



int
main() {
  flutter_iot::Endian endian = flutter_iot::Endian::HOST;

  TEST_FOR_N(100, [endian]() {
    TEST_FUNCTION(testWriteReadUint8);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadUint16, endian);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadUint32, endian);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadInt32, endian);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadInt64, endian);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadFloat64, endian);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadUint8List);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadInt32List);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadInt64List);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadFloat32List);
    TEST_PUTS("\n");

    TEST_FUNCTION(testWriteReadFloat64List);
    TEST_PUTS("\n");

    return true;
  });

  return 0;
}



bool
testWriteReadUint8() {
  auto u8 = getRandom<uint8_t>();
  TEST_PRINTF("Writing %x\n", u8);

  flutter_iot::WriteBuffer wb;
  wb.putUint8(u8);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getUint8();

  bool isSame = (u8 == read);
  TEST_PRINTF("Read %x\nIs same: %s\n", read, BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadUint16(flutter_iot::Endian endian) {
  auto u16 = getRandom<uint16_t>();
  TEST_PRINTF("Writing %x\n", u16);

  flutter_iot::WriteBuffer wb;
  wb.putUint16(u16, endian);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getUint16(endian);

  bool isSame = (u16 == read);
  TEST_PRINTF("Read %x\nIs same: %s\n", read, BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadUint32(flutter_iot::Endian endian) {
  auto u32 = getRandom<uint32_t>();
  TEST_PRINTF("Writing %x\n", u32);

  flutter_iot::WriteBuffer wb;
  wb.putUint32(u32, endian);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getUint32(endian);

  bool isSame = (u32 == read);
  TEST_PRINTF("Read %x\nIs same: %s\n", read, BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadInt32(flutter_iot::Endian endian) {
  auto i32 = getRandom<int32_t>();
  TEST_PRINTF("Writing %x\n", i32);

  flutter_iot::WriteBuffer wb;
  wb.putInt32(i32, endian);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getInt32(endian);

  bool isSame = (i32 == read);
  TEST_PRINTF("Read %x\nIs same: %s\n", read, BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadInt64(flutter_iot::Endian endian) {
  auto i64 = getRandom<int64_t>();
  TEST_PRINTF("Writing %lx\n", i64);

  flutter_iot::WriteBuffer wb;
  wb.putInt64(i64, endian);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getInt64(endian);

  bool isSame = (i64 == read);
  TEST_PRINTF("Read %lx\nIs same: %s\n", read, BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadFloat64(flutter_iot::Endian endian) {
  auto u64 = getRandom<uint64_t>();
  flutter_iot::float64_t f64;
  std::memcpy(&f64, &u64, 8);

  TEST_PRINTF("Writing %lx\n", u64);

  flutter_iot::WriteBuffer wb;
  wb.putFloat64(f64, endian);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getFloat64(endian);

  bool isSame = (f64 == read);
  uint64_t bytes;
  std::memcpy(&bytes, &f64, 8);

  TEST_PRINTF("Read %lx\nIs same: %s\n", bytes, BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadUint8List() {
  using namespace flutter_iot;

  auto data = getRandomContainer<flutter_iot::Uint8List>(30);
  TEST_OUT("Writing data: " << data << '\n');

  flutter_iot::WriteBuffer wb;
  wb.putUint8List(data);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getUint8List(data.size());
  TEST_OUT("read: " << read << '\n');

  bool isSame = boost::equal(data, read);
  TEST_PRINTF("Is same: %s\n", BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadInt32List() {
  using namespace flutter_iot;

  auto data = getRandomContainer<flutter_iot::Int32List>(50);
  TEST_OUT("Writing data: " << data << '\n');

  flutter_iot::WriteBuffer wb;
  wb.putInt32List(data);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getInt32List(data.size());
  TEST_OUT("read: " << read << '\n');

  bool isSame = boost::equal(data, read);
  TEST_PRINTF("Is same: %s\n", BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadInt64List() {
  using namespace flutter_iot;

  auto data = getRandomContainer<flutter_iot::Int64List>(50);
  TEST_OUT("Writing data: " << data << '\n');

  flutter_iot::WriteBuffer wb;
  wb.putInt64List(data);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getInt64List(data.size());
  TEST_OUT("read: " << read << '\n');

  bool isSame = boost::equal(data, read);
  TEST_PRINTF("Is same: %s\n", BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadFloat32List() {
  using namespace flutter_iot;

  auto data = getRandomContainer<flutter_iot::Float32List>(50);
  TEST_OUT("Writing data: " << data << '\n');

  flutter_iot::WriteBuffer wb;
  wb.putFloat32List(data);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getFloat32List(data.size());
  TEST_OUT("read: " << read << '\n');

  bool isSame = boost::equal(data, read);
  TEST_PRINTF("Is same: %s\n", BOOL2STR(isSame));

  return isSame;
}

bool
testWriteReadFloat64List() {
  using namespace flutter_iot;

  auto data = getRandomContainer<flutter_iot::Float64List>(50);
  TEST_OUT("Writing data: " << data << '\n');

  flutter_iot::WriteBuffer wb;
  wb.putFloat64List(data);

  auto buffer = wb.done();
  TEST_OUT("Buffer: " << buffer << '\n');

  flutter_iot::ReadBuffer rb(buffer);
  auto read = rb.getFloat64List(data.size());
  TEST_OUT("read: " << read << '\n');

  bool isSame = boost::equal(data, read);
  TEST_PRINTF("Is same: %s\n", BOOL2STR(isSame));

  return isSame;
}
