#include "tests.inc"

#define IS_TYPE_ANY_OR_NONE(value) \
  FLUTTER_IOT_IS_TYPE(flutter_iot::AnyOrNone, typeid(value))

int
main() {
  using namespace flutter_iot;
  using namespace flutter_iot::literals;

  auto v1 = "nona"_any;
  TEST_EXPECT(IS_TYPE_ANY_OR_NONE(v1), true);

  auto v2 = 0xFF_any;
  TEST_EXPECT(IS_TYPE_ANY_OR_NONE(v2), true);

  auto v3 = 0xFFFF_any;
  TEST_EXPECT(IS_TYPE_ANY_OR_NONE(v3), true);

  auto v4 = 0xFFFFFFFF_any;
  TEST_EXPECT(IS_TYPE_ANY_OR_NONE(v4), true);

  auto v5 = 0xFFFFFFFFFFFFFFFF_any;
  TEST_EXPECT(IS_TYPE_ANY_OR_NONE(v5), true);

  auto v6 = 0.2_any;
  TEST_EXPECT(IS_TYPE_ANY_OR_NONE(v6), true);

  return 0;
}