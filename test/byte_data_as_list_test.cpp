#include "tests.inc"

#include <cinttypes>
#include <ctime>

#include <algorithm>
#include <limits>
#include <random>

#include <boost/type_traits.hpp>

bool
testAsFloat32List();
bool
testAsFloat64List();
bool
testAsInt8List();
bool
testAsInt16List();
bool
testAsInt32List();
bool
testAsInt64List();
bool
testAsUint8List();
bool
testAsUint16List();
bool
testAsUint32List();
bool
testAsUint64List();



int
main() {
  std::srand((uint) std::time(nullptr));

  std::cout << "optional<int>: "
            << flutter_iot::has_typedef_value_type<boost::optional<int>>::value << "\n";
  std::cout << "int: " << flutter_iot::has_typedef_value_type<int>::value << "\n";

  // TEST_FOR_N(1000, []() {
  //   TEST_FUNCTION(testAsFloat32List);
  //   TEST_FUNCTION(testAsFloat64List);
  //   TEST_FUNCTION(testAsInt8List);
  //   TEST_FUNCTION(testAsInt16List);
  //   TEST_FUNCTION(testAsInt32List);
  //   TEST_FUNCTION(testAsInt64List);
  //   TEST_FUNCTION(testAsUint8List);
  //   TEST_FUNCTION(testAsUint16List);
  //   TEST_FUNCTION(testAsUint32List);
  //   TEST_FUNCTION(testAsUint64List);

  //   return true;
  // });

  return 0;
}



bool
testAsFloat32List() {
  auto list = getRandomContainer<flutter_iot::Float32List>(20);
  flutter_iot::ByteData data(list.size() * 4);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setFloat32(i * 4, list[i]);
  }

  auto dataAsList = data.asFloat32List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %f\tdataAsList[%lu]: %f", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 4 == data.getLengthInBytes());

  return true;
}

bool
testAsFloat64List() {
  auto list = getRandomContainer<flutter_iot::Float64List>(20);
  flutter_iot::ByteData data(list.size() * 8);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setFloat64(i * 8, list[i]);
  }

  auto dataAsList = data.asFloat64List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %lf\tdataAsList[%lu]: %lf", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 8 == data.getLengthInBytes());

  return true;
}

bool
testAsInt8List() {
  auto list = getRandomContainer<flutter_iot::Int8List>(20);
  flutter_iot::ByteData data(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    data.setInt8(i, list[i]);
  }

  auto dataAsList = data.asInt8List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %i\tdataAsList[%lu]: %i", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() == data.getLengthInBytes());

  return true;
}

bool
testAsInt16List() {
  auto list = getRandomContainer<flutter_iot::Int16List>(20);
  flutter_iot::ByteData data(list.size() * 2);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setInt16(i * 2, list[i]);
  }

  auto dataAsList = data.asInt16List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %i\tdataAsList[%lu]: %i", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 2 == data.getLengthInBytes());

  return true;
}

bool
testAsInt32List() {
  auto list = getRandomContainer<flutter_iot::Int32List>(20);
  flutter_iot::ByteData data(list.size() * 4);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setInt32(i * 4, list[i]);
  }

  auto dataAsList = data.asInt32List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %i\tdataAsList[%lu]: %i", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 4 == data.getLengthInBytes());

  return true;
}

bool
testAsInt64List() {
  auto list = getRandomContainer<flutter_iot::Int64List>(20);
  flutter_iot::ByteData data(list.size() * 8);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setInt64(i * 8, list[i]);
  }

  auto dataAsList = data.asInt64List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %li\tdataAsList[%lu]: %li", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 8 == data.getLengthInBytes());

  return true;
}

bool
testAsUint8List() {
  auto list = getRandomContainer<flutter_iot::Uint8List>(20);
  flutter_iot::ByteData data(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    data.setUint8(i, list[i]);
  }

  auto dataAsList = data.asUint8List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %u\tdataAsList[%lu]: %u", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() == data.getLengthInBytes());

  return true;
}

bool
testAsUint16List() {
  auto list = getRandomContainer<flutter_iot::Uint16List>(20);
  flutter_iot::ByteData data(list.size() * 2);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setUint16(i * 2, list[i]);
  }

  auto dataAsList = data.asUint16List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %u\tdataAsList[%lu]: %u", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 2 == data.getLengthInBytes());

  return true;
}

bool
testAsUint32List() {
  auto list = getRandomContainer<flutter_iot::Uint32List>(20);
  flutter_iot::ByteData data(list.size() * 4);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setUint32(i * 4, list[i]);
  }

  auto dataAsList = data.asUint32List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %u\tdataAsList[%lu]: %u", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 4 == data.getLengthInBytes());

  return true;
}

bool
testAsUint64List() {
  auto list = getRandomContainer<flutter_iot::Uint64List>(20);
  flutter_iot::ByteData data(list.size() * 8);

  for (size_t i = 0; i < list.size(); ++i) {
    data.setUint64(i * 8, list[i]);
  }

  auto dataAsList = data.asUint64List(list.size());

  for (size_t i = 0; i < list.size(); ++i) {
    auto a = list[i];
    auto b = dataAsList[i];
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %lu\tdataAsList[%lu]: %lu", i, a, i, b);
  }

  FLUTTER_IOT_ASSERT(list.size() * 8 == data.getLengthInBytes());

  return true;
}
