#include "tests.inc"

#include <map>
#include <unordered_map>
#include <vector>

#define PAIR(first, second) std::make_pair(AnyOrNone(first), AnyOrNone(second))

#define MAKE_PAIR(index, first, second) auto p##index = PAIR(first, second)



bool
testHashUint8List();
bool
testHashUint16List();
bool
testHashUint32List();
bool
testHashUint64List();
bool
testHashInt8List();
bool
testHashInt16List();
bool
testHashInt32List();
bool
testHashInt64List();
bool
testHashFloat32List();
bool
testHashFloat64List();
bool
testHashMethodCall1();
bool
testHashString();



int
main() {
  TEST_FOR_N(10, []() {
    TEST_FUNCTION(testHashUint8List);
    TEST_FUNCTION(testHashUint16List);
    TEST_FUNCTION(testHashUint32List);
    TEST_FUNCTION(testHashUint64List);
    TEST_FUNCTION(testHashInt8List);
    TEST_FUNCTION(testHashInt16List);
    TEST_FUNCTION(testHashInt32List);
    TEST_FUNCTION(testHashInt64List);
    TEST_FUNCTION(testHashFloat32List);
    TEST_FUNCTION(testHashFloat64List);
    TEST_FUNCTION(testHashMethodCall1);
    TEST_FUNCTION(testHashString);

    return true;
  });

  return 0;
}



bool
testHashUint8List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 53038351);

  auto list = flutter_iot::Uint8List({5, 6, 99, 88, 71});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashUint16List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 274548095);

  auto list = flutter_iot::Uint16List({6887, 7392, 88, 9321, 60000});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashUint32List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 316920651);

  auto list = flutter_iot::Uint32List({98431213, 123516, 681234, 1268316324, 4218313254});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashUint64List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 2652132);

  auto list = flutter_iot::Uint64List({97818632790472344,
                                       876813240128937,
                                       674983124665497895,
                                       781324652483520,
                                       218379558413527});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashInt8List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 307490926);

  auto list = flutter_iot::Int8List({-99, 87, -120, 0, 42});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashInt16List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 46371336);

  auto list = flutter_iot::Int16List({-6887, -30392, 30190, -9321, 512});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashInt32List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 212180225);

  auto list = flutter_iot::Int32List({-2147483648, 879562, 9984132, 2147483647, -574});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashInt64List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 449614685);

  auto list = flutter_iot::Int64List(
    {9223372036854775807, 9789127839, -922337203685477580, -7983249, 0});
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashFloat32List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 71903496);

  auto list = flutter_iot::Float32List({
    8.06117e+37,
    1.76361e+38,
    2.47788e+38,
    1.9597e+38,
    6.94603e+37,
  });
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashFloat64List() {
  using namespace flutter_iot;
  FLUTTER_IOT_STATIC_CONSTEXPR(uint64_t, dartHash = 15234592);

  auto list = flutter_iot::Float64List({
    1.72755e+154,
    1.34878e+3,
    1.38452e+308,
    1.47782e+8,
    1.69855e+99,
  });
  auto hash = flutter_iot::hashList(list);
  TEST_OUT("\ndartHash: " << dartHash << "\nhash: " << hash << "\nlist: " << list
                          << "\n");

  return dartHash == hash;
}

bool
testHashMethodCall1() {
  using namespace flutter_iot;

  StandardMethodCodec::MessageCodecType::ValueTypeLabel::Map args({
    PAIR(std::string("0-null"), /* null */),  //
    PAIR(std::string("1-bool"), true),        //
    PAIR(std::string("2-float64_t"), 99.123),
    PAIR(std::string("3-int32_t"), (int32_t) 123456),
    PAIR(std::string("4 -987654"), -987654),
    PAIR(std::string("5-std::string"), std::string("Jo nona ned")),
    PAIR(std::string("6-Uint8List"), Uint8List({1, 2, 3, 4, 5})),
    PAIR(std::string("7-Int32List"), Int32List({-10, 20, -30, 40, -50})),
    PAIR(std::string("8-Int64List"), Int64List({-99, 888, -7777, 66666, -555555})),
    PAIR(std::string("9-Float32List"),
         Float32List({-10.111f, 20.222f, -30.333f, 40.444f, -50.555f})),
    PAIR(std::string("10-Float64List"),
         Float64List({-99.1415, 888.1415, -7777.1415, 66666.1415, -555555.1415})),
    PAIR(std::string("11-List"),
         StandardMessageCodec::ValueTypeLabel::List({
           AnyOrNone(std::string("List val1")),
           AnyOrNone(12345),
           AnyOrNone(-9.123),
           AnyOrNone(false),
           AnyOrNone(/* null */),
         })),
    PAIR(std::string("12-Map"),
         StandardMessageCodec::ValueTypeLabel::Map({
           PAIR(std::string("Key1"), std::string("value1")),
           PAIR(3.1415, false),
           PAIR(true, (int32_t) 9988),
         })),
  });
  std::string method("testHashMethodCall1_Method");

  StandardMethodCodec::MethodCallType call(method, args);

  StandardMethodCodec codec;
  auto hashedCall = hash(codec, call);

  for (auto& p : args) {
    TEST_OUT(
      "Key: " << StandardMessageCodec::ValueTypeLabel::toString(p.first) << "\nValue: "
              << StandardMessageCodec::ValueTypeLabel::toString(p.second) << "\n\n");
  }

  TEST_OUT("\nhashedCall: " << hashedCall << "\n");
  TEST_EXPECT(hashedCall, 122082098);

  return true;
}

bool
testHashString() {
  using namespace flutter_iot;

  std::string s("I like cookies");
  auto hashedString = hash(s);

  TEST_OUT("\nhashedString: " << hashedString << "\n");
  TEST_EXPECT(hashedString, 215871094);

  return true;
}
