#ifndef TEST_TESTS
#define TEST_TESTS

#include <cassert>
#include <cerrno>
#include <cstring>

#include <iostream>
#include <limits>
#include <ostream>
#include <random>
#include <sstream>

#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/range.hpp>

#include "flutter_iot.inc"

#ifndef FLUTTER_IOT_TEST_LOG_ENABLE
#  define FLUTTER_IOT_TEST_LOG_ENABLE 0
#endif  // FLUTTER_IOT_TEST_LOG_ENABLE

#if FLUTTER_IOT_TEST_LOG_ENABLE

#  define TEST_PUTS(message)       std::puts(message)
#  define TEST_PRINTF(format, ...) std::printf(format, ##__VA_ARGS__)
#  define TEST_OUT(message)        std::cout << message

#else  // FLUTTER_IOT_TEST_LOG_ENABLE

#  define TEST_PUTS(message)
#  define TEST_PRINTF(format, ...)
#  define TEST_OUT(message)

#endif  // FLUTTER_IOT_TEST_LOG_ENABLE

#define BOOL2STR(expr) (static_cast<bool>(expr) ? "true" : "false")

#define TEST_EXPECT(actual, matcher)                      \
  FLUTTER_IOT_FASSERT(actual == matcher,                  \
                      "Expected '" FLUTTER_IOT_STRINGIFY( \
                        matcher) "', but got '" FLUTTER_IOT_STRINGIFY(actual) "'.")

#define TEST_EXPECT_NOT(actual, no_matcher)               \
  FLUTTER_IOT_FASSERT(actual != no_matcher,               \
                      "Expected '" FLUTTER_IOT_STRINGIFY( \
                        matcher) "' to not equal '" FLUTTER_IOT_STRINGIFY(actual) "'.")

/**
 * @brief Check if the evaluated result of @c funtion is @b true ,
 *        if not throw an assertion error using @c TEST_FASSERT . 
 */
#define TEST_FUNCTION(function, ...)                                                \
  do {                                                                              \
    TEST_PRINTF("Testing " #function "...");                                        \
    FLUTTER_IOT_FASSERT(function(__VA_ARGS__), "Test for '" #function "' failed."); \
    TEST_PRINTF("\tSuccess!\n");                                                    \
  } while (0)

/**
 * @brief Run @c function for @c n times and see if @c function
 *        always returns @b true .
 *        
 * @param n The number of times you want to run @c function .    
 * @param function A callable that does not take any parameters and
 *                 returns a @c bool .
 */
#define TEST_FOR_N(n, function)                               \
  for (size_t i = 0; i < n; ++i) {                            \
    TEST_PRINTF("Run Nr.%lu\n", i);                           \
    FLUTTER_IOT_FASSERT(function(), "Run Nr.%lu failed.", i); \
    TEST_PUTS("");                                            \
  }


template<typename CharType, typename Traits>
inline std::basic_ostream<CharType, Traits>&
operator<<(std::basic_ostream<CharType, Traits>& stream,
           const flutter_iot::ByteData& data) noexcept {
  stream << "flutter_iot::ByteData[";

  if (0 == data.getLengthInBytes()) {
    stream << "]";
    return stream;
  }

  auto bytes = data.asUint8List(data.getLengthInBytes());
  size_t i;
  for (i = 0; i < bytes.size() - 1; ++i) {
    stream << boost::format("0x%02x") % static_cast<int>(bytes[i]) << ", ";
  }
  stream << boost::format("0x%02x") % static_cast<int>(bytes[i]);

  stream << "]";

  return stream;
}

/**
 * @brief Get a random floating point value in the range [ @c min , @c max )
 *        with equal probability throughout the range.
 * 
 * @param min Defaults to the numeric min limit of @c T .
 * @param max Defaults to the numeric max limit of @c T .
 */
template<typename T,
         boost::enable_if_t<                    ///
           boost::is_floating_point<T>::value,  //
           bool                                 //
           > = true                             ///
         >
T
getRandom(T min = std::numeric_limits<T>::min(),
          T max = std::numeric_limits<T>::max()) noexcept {
  static std::random_device device;
  static std::default_random_engine engine(device());

  std::uniform_real_distribution<T> distribution(min, max);

  return distribution(engine);
}

/**
 * @brief Get a random integer value in the range [ @c min , @c max ]
 *        with equal probability throughout the range.
 * 
 * @param min Defaults to the numeric min limit of @c T .
 * @param max Defaults to the numeric max limit of @c T .
 */
template<typename T,
         boost::enable_if_t<              ///
           boost::is_integral<T>::value,  //
           bool                           //
           > = true                       ///
         >
T
getRandom(T min = std::numeric_limits<T>::min(),
          T max = std::numeric_limits<T>::max()) noexcept {
  static std::random_device device;
  static std::default_random_engine engine(device());

  std::uniform_int_distribution<T> distribution(min, max);

  return distribution(engine);
}


template<typename T,
         boost::enable_if_t<                       ///
           boost::is_same<T, std::string>::value,  //
           bool                                    //
           > = true                                ///
         >
T
getRandom(size_t minSize = 30, size_t maxSize = 60) noexcept {
  FLUTTER_IOT_ASSERT(minSize <= maxSize);

  if (0 == (maxSize - minSize))
    return "";

  auto getLetter = [](bool upperCase) -> char {
    auto random = getRandom<uint8_t>('A', 'Z');
    auto offset = 'a' - 'A';
    return static_cast<char>(upperCase ? random : (random + offset));
  };
  auto getNumber = []() -> char {
    return static_cast<char>(getRandom<uint8_t>('0', '9'));
  };

  size_t size = getRandom<size_t>(minSize, maxSize);
  std::stringstream ss;

  for (size_t i = 0; i < size; ++i) {
    auto choice = getRandom<uint8_t>();

    if (choice >= 0 && choice < 255 * 1 / 3) {
      ss << flutter_iot::toString(getLetter(true));
    } else if (choice >= 255 * 1 / 3 && choice < 255 * 2 / 3) {
      ss << flutter_iot::toString(getNumber());
    } else {
      ss << flutter_iot::toString(getLetter(false));
    }
  }

  return ss.str();
}

/**
 * @brief Get a SequenceContainer with @c count random values in the specified range. 
 *        
 *        If @c SequenceContainer::value_type is a floating point type then
 *        the range is [ @c min , @c max ).
 * 
 *        If @c SequenceContainer::value_type is an integral type then
 *        the range is [ @c min , @c max ].
 * 
 * @param min Defaults to the numeric min limit of @c SequenceContainer::value_type .
 * @param max Defaults to the numeric max limit of @c SequenceContainer::value_type .
 */
template<typename SequenceContainer,
         boost::enable_if_t<                                                     ///
           boost::is_arithmetic<typename SequenceContainer::value_type>::value,  //
           bool                                                                  //
           > = true                                                              ///
         >
SequenceContainer
getRandomContainer(size_t count,
                   typename SequenceContainer::value_type min =
                     std::numeric_limits<typename SequenceContainer::value_type>::min(),
                   typename SequenceContainer::value_type max =
                     std::numeric_limits<typename SequenceContainer::value_type>::max()) {
  SequenceContainer list(count, 0);
  std::generate(list.begin(), list.end(), [min, max]() {
    return getRandom<typename SequenceContainer::value_type>(min, max);
  });
  return list;
}

#endif  // TEST_TESTS