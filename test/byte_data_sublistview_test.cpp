#include "tests.inc"

#include <ctime>

#include <random>

std::pair<uint64_t, uint64_t>
getRandomStartAndEnd(size_t size) {
  uint64_t start = std::rand() % (size / 2);
  uint64_t end = (size / 2) + (std::rand() % (size / 2));
  return std::make_pair(start, end);
}

bool
testSubListViewUint8List();
bool
testSubListViewInt32List();
bool
testSubListViewInt64List();
bool
testSubListViewFloat32List();
bool
testSubListViewFloat64List();

int
main() {
  std::srand((uint) std::time(nullptr));

  TEST_FOR_N(100, []() {
    TEST_FUNCTION(testSubListViewUint8List);
    TEST_FUNCTION(testSubListViewInt32List);
    TEST_FUNCTION(testSubListViewInt64List);
    TEST_FUNCTION(testSubListViewFloat32List);
    TEST_FUNCTION(testSubListViewFloat64List);

    return true;
  });

  return 0;
}

bool
testSubListViewUint8List() {
  using namespace flutter_iot;

  auto list = getRandomContainer<Uint8List>(50, 0x00, 0xFF);
  auto pair = getRandomStartAndEnd(list.size());
  auto start = pair.first;
  auto end = pair.second;

  RangeError::checkValidRange(
    FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, list.size(), start, end + 1);
  ByteData data = ByteData::sublistCopy(list, start, end);

  for (size_t i = start; i <= end; ++i) {
    auto a = list[i];
    auto b = data.getUint8(i - start);
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %u\tdata.getUint8(%lu): %u", i, a, i, b);
  }

  return true;
}

bool
testSubListViewInt32List() {
  using namespace flutter_iot;

  auto list = getRandomContainer<Int32List>(50, -0xFFFFFF, 0xFFFFFF);
  auto pair = getRandomStartAndEnd(list.size());
  auto start = pair.first;
  auto end = pair.second;

  RangeError::checkValidRange(
    FLUTTER_IOT_DEFAULT_ERROR_MESSAGE, list.size(), start, end + 1);
  ByteData data = ByteData::sublistCopy(list, start, end);

  for (size_t i = start; i <= end; ++i) {
    auto a = list[i];
    auto b = data.getInt32((i - start) * 4);
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %i\tdata.getInt32(%lu): %i", i, a, i, b);
  }

  return true;
}

bool
testSubListViewInt64List() {
  using namespace flutter_iot;

  auto list = getRandomContainer<Int64List>(50, -0xFFFFFFFFFF, 0xFFFFFFFFFF);
  auto pair = getRandomStartAndEnd(list.size());
  auto start = pair.first;
  auto end = pair.second;

  RangeError::checkValidRange(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,list.size(), start, end + 1);
  ByteData data = ByteData::sublistCopy(list, start, end);

  for (size_t i = start; i <= end; ++i) {
    auto a = list[i];
    auto b = data.getInt64((i - start) * 8);
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %li\tdata.getInt64(%lu): %li", i, a, i, b);
  }

  return true;
}

bool
testSubListViewFloat32List() {
  using namespace flutter_iot;

  auto list = getRandomContainer<Float32List>(50, -100000, 100000);
  auto pair = getRandomStartAndEnd(list.size());
  auto start = pair.first;
  auto end = pair.second;

  RangeError::checkValidRange(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,list.size(), start, end + 1);
  ByteData data = ByteData::sublistCopy(list, start, end);

  for (size_t i = start; i <= end; ++i) {
    auto a = list[i];
    auto b = data.getFloat32((i - start) * 4);
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %f\tdata.getFloat32(%lu): %f", i, a, i, b);
  }

  return true;
}

bool
testSubListViewFloat64List() {
  using namespace flutter_iot;

  auto list = getRandomContainer<Float64List>(50, -1000000, 1000000);
  auto pair = getRandomStartAndEnd(list.size());
  auto start = pair.first;
  auto end = pair.second;

  RangeError::checkValidRange(FLUTTER_IOT_DEFAULT_ERROR_MESSAGE,list.size(), start, end + 1);
  ByteData data = ByteData::sublistCopy(list, start, end);

  for (size_t i = start; i <= end; ++i) {
    auto a = list[i];
    auto b = data.getFloat64((i - start) * 8);
    FLUTTER_IOT_FASSERT(a == b, "list[%lu]: %lf\tdata.getFloat64(%lu): %lf", i, a, i, b);
  }

  return true;
}
