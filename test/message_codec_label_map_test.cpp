#include "tests.inc"

#define MAKE_PAIRS(index, first, second)         \
  auto p##index = std::make_pair(first, second); \
  auto rp##index = std::make_pair(second, first);

int
main() {
  using namespace flutter_iot;

  StandardMessageCodec::ValueTypeLabel::Map map;


  MAKE_PAIRS(0, AnyOrNone(std::string("0-null")), AnyOrNone());
  MAKE_PAIRS(1, AnyOrNone(std::string("1-bool")), AnyOrNone(true));
  MAKE_PAIRS(2, AnyOrNone(std::string("2-float64_t")), AnyOrNone((float64_t) 99.123));
  MAKE_PAIRS(3, AnyOrNone(std::string("3-int32_t")), AnyOrNone((int32_t) 123456));
  MAKE_PAIRS(4, AnyOrNone(std::string("4-int64_t")), AnyOrNone((int64_t) -987654));
  MAKE_PAIRS(
    5, AnyOrNone(std::string("5-std::string")), AnyOrNone(std::string("Jo nona ned")));
  MAKE_PAIRS(
    6, AnyOrNone(std::string("6-Uint8List")), AnyOrNone(Uint8List({1, 2, 3, 4, 5})));
  MAKE_PAIRS(7,
             AnyOrNone(std::string("7-Int32List")),
             AnyOrNone(Int32List({-10, 20, -30, 40, -50})));
  MAKE_PAIRS(8,
             AnyOrNone(std::string("8-Int64List")),
             AnyOrNone(Int64List({-99, 888, -7777, 66666, -555555})));
  MAKE_PAIRS(9,
             AnyOrNone(std::string("9-Float32List")),
             AnyOrNone(Float32List({-10.111f, 20.222f, -30.333f, 40.444f, -50.555f})));
  MAKE_PAIRS(
    10,
    AnyOrNone(std::string("10-Float64List")),
    AnyOrNone(Float64List({-99.1415, 888.1415, -7777.1415, 66666.1415, -555555.1415})));
  MAKE_PAIRS(11,
             AnyOrNone(std::string("11-List")),
             AnyOrNone(StandardMessageCodec::ValueTypeLabel::List({
               AnyOrNone(std::string("List val1")),
               AnyOrNone(12345),
               AnyOrNone((float64_t) -9.123f),
               AnyOrNone(false),
               AnyOrNone(),
             })));
  MAKE_PAIRS(11_2,
             AnyOrNone(std::string("112-List")),
             AnyOrNone(StandardMessageCodec::ValueTypeLabel::List({
               AnyOrNone(std::string("2List val1")),
               AnyOrNone(true),
             })));
  MAKE_PAIRS(12,
             AnyOrNone(std::string("12-Map")),
             AnyOrNone(StandardMessageCodec::ValueTypeLabel::Map({
               {AnyOrNone(std::string("Key1")), AnyOrNone(std::string("value1"))},
               {AnyOrNone((float64_t) 3.1415f), AnyOrNone(false)},
               {AnyOrNone(true), AnyOrNone((int32_t) 9988)},
             })));



  map.insert(p0);
  map.insert(p1);
  map.insert(p2);
  map.insert(p3);
  map.insert(p4);
  map.insert(p5);
  map.insert(p6);
  map.insert(p7);
  map.insert(p8);
  map.insert(p9);
  map.insert(p10);
  map.insert(p11);
  map.insert(p11_2);
  map.insert(p12);

  map.insert(rp0);
  map.insert(rp1);
  map.insert(rp2);
  map.insert(rp3);
  map.insert(rp4);
  map.insert(rp5);
  map.insert(rp6);
  map.insert(rp7);
  map.insert(rp8);
  map.insert(rp9);
  map.insert(rp10);
  map.insert(rp11);
  map.insert(rp11_2);
  map.insert(rp12);

  TEST_OUT(StandardMessageCodec::ValueTypeLabel::toString(AnyOrNone(map)) << "\n\n");

  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p0.first], p0.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p1.first], p1.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p2.first], p2.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p3.first], p3.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p4.first], p4.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p5.first], p5.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p6.first], p6.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p7.first], p7.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p8.first], p8.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p9.first], p9.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p10.first], p10.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p11.first], p11.second);
  TEST_FUNCTION(StandardMessageCodec::ValueTypeLabel::Equivalent::equal,
                map[p11_2.first],
                p11_2.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[p12.first], p12.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp0.first], rp0.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp1.first], rp1.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp2.first], rp2.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp3.first], rp3.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp4.first], rp4.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp5.first], rp5.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp6.first], rp6.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp7.first], rp7.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp8.first], rp8.second);
  TEST_FUNCTION(
    StandardMessageCodec::ValueTypeLabel::Equivalent::equal, map[rp9.first], rp9.second);
  TEST_FUNCTION(StandardMessageCodec::ValueTypeLabel::Equivalent::equal,
                map[rp10.first],
                rp10.second);
  TEST_FUNCTION(StandardMessageCodec::ValueTypeLabel::Equivalent::equal,
                map[rp11.first],
                rp11.second);
  TEST_FUNCTION(StandardMessageCodec::ValueTypeLabel::Equivalent::equal,
                map[rp11_2.first],
                rp11_2.second);
  TEST_FUNCTION(StandardMessageCodec::ValueTypeLabel::Equivalent::equal,
                map[rp12.first],
                rp12.second);

  return 0;
}
