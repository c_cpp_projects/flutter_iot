#include "tests.inc"

bool
testInitList();
bool
testAt();



int
main() {
  TEST_FUNCTION(testInitList);
  TEST_FUNCTION(testAt);

  return 0;
}



bool
testInitList() {
  using namespace flutter_iot;

  OrderedMap<std::string, int> map({{"nona", 1}, {"jo", -9}, {"fleck", 97}});

  TEST_OUT("\n");
  for (auto& p : map) {
    TEST_OUT("Key: " << p.first << "\nValue: " << p.second << "\n\n");
  }


  return true;
}

bool
testAt() {
  using namespace flutter_iot;

  OrderedMap<std::string, int> map({{"nona", 1}, {"jo", -9}, {"fleck", 97}});

  map["fetzn"];
  TEST_EXPECT(map["fetzn"], 0);

  try {
    map.at("no key");
  } catch (const std::out_of_range& e) {}


  return true;
}
