#include "tests.inc"

int main() {
  using namespace flutter_iot;

  StandardMessageCodec codec;
  WriteBuffer buffer;

  AnyOrNone method(std::string("myCppMethod"));
  AnyOrNone args(StandardMessageCodec::ValueTypeLabel::Map({
    {AnyOrNone(std::string("id")), AnyOrNone((int32_t) 99)},
    {AnyOrNone(std::string("name")), AnyOrNone(std::string("Hans Peter Friedrich"))},
    {AnyOrNone(std::string("grade")), AnyOrNone((float64_t) 2.145)},
  }));

  codec.writeValue(buffer, method);
  codec.writeValue(buffer, args);
  buffer.done();

  return 0;
}
