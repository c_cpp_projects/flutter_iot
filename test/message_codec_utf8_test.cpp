#include "tests.inc"

#include <sstream>
#include <string>

#include <boost/range.hpp>

#define PRINT_ITEM(item) \
  TEST_OUT(FLUTTER_IOT_STRINGIFY(item) ":\n" << item.toString() << "\n");

#define CHECK_ITEM(item, expectedBytes)                                             \
  do {                                                                              \
    auto encoded = item.getEncodedMessage();                                        \
    FLUTTER_IOT_ASSERT(encoded.has_value());                                        \
    FLUTTER_IOT_FASSERT(                                                            \
      boost::equal(encoded.value().asUint8List(encoded.value().getLengthInBytes()), \
                   expectedBytes),                                                  \
      "The encoded value of item(" FLUTTER_IOT_STRINGIFY(                           \
        item) ") with string '%s' is not equal to the expected result.",            \
      item.s.c_str());                                                              \
  } while (0)



struct Item {
  std::string s;

  flutter_iot::OptionalByteData
  getEncodedMessage() {
    static flutter_iot::StandardMessageCodec codec;
    return codec.encodeMessage(flutter_iot::makeAnyOrNone(s));
  }

  std::string
  toString() {
    auto encodedMessage = getEncodedMessage();
    std::stringstream ss;

    ss << "Item{\n";
    ss << "  s: " << s << ",\n";
    if (encodedMessage.has_value()) {
      ss << "  encodedMessage: " << encodedMessage.value();
    } else {
      ss << "  encodedMessage: null";
    }
    ss << "\n";
    ss << "}";

    return ss.str();
  }
};



bool
test1Byte();
bool
test2Byte();
bool
test3Byte();
bool
test4Byte();

int
main() {
  TEST_FUNCTION(test1Byte);
  TEST_FUNCTION(test2Byte);
  TEST_FUNCTION(test3Byte);
  TEST_FUNCTION(test4Byte);

  return 0;
}



bool
test1Byte() {
  TEST_OUT("\n");

  // item: a
  // encodeMessage: [0x07, 0x01, 0x61, 0, 0, 0, 0, 0]
  // utf8Encoded: [0x61]
  std::string s1 = "a";
  auto i1 = Item{s1};
  PRINT_ITEM(i1);
  CHECK_ITEM(i1, flutter_iot::Uint8List({0x07, 0x01, 0x61}));


  TEST_OUT("\n");
  return true;
}

bool
test2Byte() {
  TEST_OUT("\n");

  // item: Ѥ
  // encodeMessage: [0x07, 0x02, 0xd1, 0xa4, 0, 0, 0, 0]
  // utf8Encoded: [0xd1, 0xa4]
  std::string s1 = "Ѥ";
  auto i1 = Item{s1};
  PRINT_ITEM(i1);
  CHECK_ITEM(i1, flutter_iot::Uint8List({0x07, 0x02, 0xd1, 0xa4}));

  // item: ѵ
  // encodeMessage: [0x07, 0x02, 0xd1, 0xb5, 0, 0, 0, 0]
  // utf8Encoded: [0xd1, 0xb5]
  std::string s2 = "ѵ";
  auto i2 = Item{s2};
  PRINT_ITEM(i2);
  CHECK_ITEM(i2, flutter_iot::Uint8List({0x07, 0x02, 0xd1, 0xb5}));

  return true;
}

bool
test3Byte() {
  // item: ⼶
  // encodeMessage: [0x07, 0x03, 0xe2, 0xbc, 0xb6, 0, 0, 0]
  // utf8Encoded: [0xe2, 0xbc, 0xb6]
  std::string s1 = "⼶";
  auto i1 = Item{s1};
  PRINT_ITEM(i1);
  CHECK_ITEM(i1, flutter_iot::Uint8List({0x07, 0x03, 0xe2, 0xbc, 0xb6}));

  // item: 㨛
  // encodeMessage: [0x07, 0x03, 0xe3, 0xa8, 0x9b, 0, 0, 0]
  // utf8Encoded: [0xe3, 0xa8, 0x9b]
  std::string s2 = "㨛";
  auto i2 = Item{s2};
  PRINT_ITEM(i2);
  CHECK_ITEM(i2, flutter_iot::Uint8List({0x07, 0x03, 0xe3, 0xa8, 0x9b}));

  // item: 䞽
  // encodeMessage: [0x07, 0x03, 0xe4, 0x9e, 0xbd, 0, 0, 0]
  // utf8Encoded: [0xe4, 0x9e, 0xbd]
  std::string s3 = "䞽";
  auto i3 = Item{s3};
  PRINT_ITEM(i3);
  CHECK_ITEM(i3, flutter_iot::Uint8List({0x07, 0x03, 0xe4, 0x9e, 0xbd}));

  // item: 듆
  // encodeMessage: [0x07, 0x03, 0xeb, 0x93, 0x86, 0, 0, 0]
  // utf8Encoded: [0xeb, 0x93, 0x86]
  std::string s4 = "듆";
  auto i4 = Item{s4};
  PRINT_ITEM(i4);
  CHECK_ITEM(i4, flutter_iot::Uint8List({0x07, 0x03, 0xeb, 0x93, 0x86}));

  return true;
}

bool
test4Byte() {
  // item: 򨺚
  // encodeMessage: [0x07, 0x04, 0xf2, 0xa8, 0xba, 0x9a, 0, 0]
  // utf8Encoded: [0xf2, 0xa8, 0xba, 0x9a4]
  std::string s1 = "򨺚";
  auto i1 = Item{s1};
  PRINT_ITEM(i1);
  CHECK_ITEM(i1, flutter_iot::Uint8List({0x07, 0x04, 0xf2, 0xa8, 0xba, 0x9a}));

  return true;
}