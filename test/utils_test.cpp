#include "tests.inc"

bool
testToString();

int
main() {
  TEST_FOR_N(1, []() {
    testToString();
    return true;
  });

  return 0;
}

bool
testToString() {
  using namespace flutter_iot;

  TEST_EXPECT(toString(0), "0");
  TEST_EXPECT(toString(true), "true");
  TEST_EXPECT(toString(false), "false");
  TEST_EXPECT(toString(3.1f, 7), "3.0999999");
  TEST_EXPECT(toString(3.1, 7), "3.1000000");
  TEST_EXPECT(toString(nullptr), "(nil)");
  TEST_EXPECT(toString((void*) (0x1234)), "0x1234");

  return true;
}
